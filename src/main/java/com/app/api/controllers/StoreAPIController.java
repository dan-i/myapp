package com.app.api.controllers;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.dto.StoreDTO;
import com.app.model.Store;
import com.app.service.StoreService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping("/api/store")
public class StoreAPIController {
	
	@Autowired
	private StoreService storeService;
	
	@RequestMapping(value="/create", method=RequestMethod.POST)
	public @ResponseBody String addStoreAPI(@RequestBody String json) {
		String result = "0";
		try {
			StoreDTO storeDTO = convertJsonStringToStoreDTO(json);
			storeService.saveStore(storeDTO);
			result = "1";
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public @ResponseBody String updateStoreAPI(@RequestBody String json) {
		String result = "0";
		try {
			StoreDTO storeDTO = convertJsonStringToStoreDTO(json);
			storeService.updateStore(storeDTO);
			result = "1";
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	public @ResponseBody String deleteStoreAPI(@RequestBody String json) {
		String result = "0";
		try {
			Store store = convertJsonStringToObject(json);
			storeService.deleteStore(store.getId());
			result = "1";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	private Store convertJsonStringToObject(String json) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
				
		Store store = mapper.readValue(json, Store.class);
		
		return store;
	}
	
	private StoreDTO convertJsonStringToStoreDTO(String json) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
				
		StoreDTO storeDTO = mapper.readValue(json, StoreDTO.class);
		
		return storeDTO;
	}

}
