package com.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.app.service.StoreService;

@Controller
public class StoreController {
	
	@Autowired
	StoreService storeService;
	
	@RequestMapping(value="stores")
	public ModelAndView viewStoreLocator() {
		ModelAndView mv = new ModelAndView("/stores");
		mv.addObject("stores", storeService.getAllStores());
		return mv;
	}
}
