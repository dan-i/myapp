package com.app.admin.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.app.exceptions.BadRequestException;
import com.app.model.Store;
import com.app.service.StoreService;

@Controller
@RequestMapping(value="/admin/store")
public class StoreAdminController {
	
	@Autowired
	StoreService storeService;
	
	@RequestMapping(value= {"/view", "/"})
	public ModelAndView viewStores() {
		ModelAndView mv = new ModelAndView("/store/view");
		mv.addObject("stores", storeService.getAllStores());
		return mv;
	}
	
	@RequestMapping(value="/create")
	public ModelAndView createStore() {
		ModelAndView mv = new ModelAndView("/store/create");
		return mv;
	}
	
	@RequestMapping(value="/update/{storeId}")
	public ModelAndView updateStore(@PathVariable int storeId) {
		Store store = storeService.getStoreById(storeId);
		if(store == null)
			throw new BadRequestException();
		ModelAndView mv = new ModelAndView("/store/update");
		mv.addObject("store", store);
		return mv;
	}
	
	@RequestMapping(value="view/location/{storeId}")
	public ModelAndView viewLocationStore(@PathVariable int storeId) {
		Store store = storeService.getStoreById(storeId);
		if(store == null)
			throw new BadRequestException();
		ModelAndView mv = new ModelAndView("/store/viewLocation");
		mv.addObject("store", store);
		return mv;
	}
}
