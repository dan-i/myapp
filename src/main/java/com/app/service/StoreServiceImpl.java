package com.app.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dao.CountryDAO;
import com.app.dao.StoreDAO;
import com.app.dto.StoreDTO;
import com.app.model.Store;

@Service
@Transactional
public class StoreServiceImpl implements StoreService {
	@Autowired
	private StoreDAO storeDAO;
	
	@Autowired
	private CountryDAO countryDAO;
	
	public int saveStore(StoreDTO storeDto) {
		Store store = convertFromDto(storeDto);
		store.setCreated(new Date());
		return storeDAO.saveStore(store);
	}
	
	public int updateStore(StoreDTO storeDto) {
		Store store = convertFromDto(storeDto);
		return storeDAO.updateStore(store);
	}
	
	public int deleteStore(int storeId) {
		return storeDAO.deleteStore(storeId);
	}
	
	public Store getStoreById(int storeId) {
		return storeDAO.getStoreById(storeId);
	}
	
	public List<Store> getAllStores(){
		return storeDAO.getAllStores();
	}
	
	private Store convertFromDto(StoreDTO storeDto) {
		Store store = new Store();
		store.setId(storeDto.getId());
		store.setCode(storeDto.getCode());
		store.setName(storeDto.getName());
		store.setEmail(storeDto.getEmail());
		store.setContact(storeDto.getContact());
		store.setAddress1(storeDto.getAddress1());
		store.setAddress2(storeDto.getAddress2());
		store.setZipcode(storeDto.getZipcode());
		store.setCountry(countryDAO.getCountryById(storeDto.getCountryId()));
		store.setIp(storeDto.getIp());
		store.setLatitude(storeDto.getLatitude());
		store.setLongitude(storeDto.getLongitude());
		store.setModified(new Date());
		return store;
	}
}
