package com.app.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.app.dto.StoreDTO;
import com.app.model.Store;

@Service
public interface StoreService {
	public int saveStore(StoreDTO storeDto);
	
	public int updateStore(StoreDTO storeDto);
	
	public int deleteStore(int storeId);
	
	public Store getStoreById(int storeId);
	
	public List<Store> getAllStores();
}
