package com.app.dao;

import com.app.model.Country;

public interface CountryDAO {
	Country getCountryById(int id);
}
