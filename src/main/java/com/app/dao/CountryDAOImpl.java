package com.app.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.app.model.Country;

@Repository
public class CountryDAOImpl implements CountryDAO {

	@PersistenceContext
	EntityManager em;
	
	@Override
	public Country getCountryById(int id) {
		// TODO Auto-generated method stub
		return em.find(Country.class, id);
	}
	
}
