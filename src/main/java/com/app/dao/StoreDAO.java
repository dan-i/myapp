package com.app.dao;

import java.util.List;

import com.app.model.Store;

public interface StoreDAO {
	int saveStore(Store store);
	
	int updateStore(Store store);
	
	int deleteStore (int storeId);
	
	Store getStoreById(int storeId);
	
	List<Store> getAllStores();

}
