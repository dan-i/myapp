package com.app.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.app.model.Store;

@Repository
public class StoreDAOImpl implements StoreDAO {

	@PersistenceContext
	EntityManager em;
	
	@Override
	public int saveStore(Store store) {
		// TODO Auto-generated method stub
		em.persist(store);
		return 1;
	}

	@Override
	public int updateStore(Store store) {
		em.merge(store);
		return store.getId();
	}

	@Override
	public int deleteStore(int storeId) {
		Store store = em.find(Store.class, storeId);
		em.remove(store);
		return 1;
	}

	@Override
	public Store getStoreById(int storeId) {
		return em.find(Store.class, storeId);
	}

	@Override
	public List<Store> getAllStores() {
		String sql = "SELECT s FROM Store s";
		TypedQuery<Store> query = em.createQuery(sql, Store.class);
		return query.getResultList();
	}

}
