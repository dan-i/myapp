<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="For GUESS Philippines">
    <meta name="author" content="JETHER.NET">
    <meta name="keywords" content="">
	<title>
        GUESS Philippines
    </title>

    <meta name="keywords" content="">

 

    <!-- styles -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">

    <!-- theme stylesheet -->
    <link href="css/style.default.css" rel="stylesheet" id="theme-stylesheet">
	<!-- your stylesheet with modifications -->
    <link href="css/guess.css" rel="stylesheet">

    <script src="js/respond.min.js"></script>

    <link rel="shortcut icon" href="img/favicon.ico">



</head>

<body>


 
<!-- *** UMPISA NG NAVBAR ***  -->
	<div class="navbar navbar-default navbar-fixed-top yamm" role="navigation" id="navbar">
        <div class="row">
		
			<!-- umpisa ng navbar header -->
            <div class="navbar-header">

                <a class="navbar-brand home" href="index.html" data-animate-hover="none">
                    <img src="img/logo-JETHER.png" alt="GUESS logo" class="col-sm-11 hidden-xs">
                    <img src="img/logo-JETHER.png" alt="GUESS logo" class="visible-xs">
					<span class="sr-only">Go to homepage</span>
                </a>
                <div class="navbar-buttons">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-align-justify"></i>
                    </button>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#search">
                        <span class="sr-only">Toggle search</span>
                        <i class="fa fa-search"></i>
                    </button>
                    <a class="btn btn-default navbar-toggle" href="basket.html">
                        <i class="fa fa-shopping-bag"></i>  <span class="hidden-xs">3 items in cart</span>
                    </a>
					 
					 <a class="btn btn-default navbar-toggle" href="#">
					 <i class="fa fa-user red"></i> 
					</a>
                </div>
            </div>
            <!-- katapusan ng navbar header -->
		
		
<!-- umpisa para sa mga menu items -->			
            <div class="navbar-collapse collapse" id="navigation">

               
				
				<ul class="nav navbar-nav navbar-left">
				
				<!-- umpisa ng individual dropdown menu item -->
				<li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">NEW ARRIVALS <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										<h5>WOMEN</h5>
                                            <ul>
                                                <li><a href="category.html">View All</a></li>
                                                <li><a href="category.html">Dresses</a></li>
                                                <li><a href="category.html">Rompers & Jumpsuits</a></li>
                                                <li><a href="category.html">Denim</a></li>
                                                <li><a href="category.html">Jackets & Outerwear</a></li>
                                                <li><a href="category.html">Tops</a></li>
                                                <li><a href="category.html">Pants</a></li>
                                                <li><a href="category.html">Shorts</a></li>
                                                <li><a href="category.html">Skirts</a></li>
                                                <li><a href="category.html">Sweaters</a></li>
                                                <li><a href="category.html">Lingerie</a></li>
                                                <li><a href="category.html">Swimwear</a></li>
                                                <li><a href="category.html">Activewear</a></li>
                                                <li><a href="category.html">Handbags</a></li>
                                                <li><a href="category.html">Shoes</a></li>
											</ul>
                                        </div>
										
										<div class="col-sm-2">
										<h5>MEN</h5>
                                            <ul>
                                                <li><a href="category.html">View All</a></li>
                                                <li><a href="category.html">Denim</a></li>
                                                <li><a href="category.html">Tanks & T Shirts</a></li>
                                                <li><a href="category.html">Shirts</a></li>
                                                <li><a href="category.html">Shorts</a></li>
                                                <li><a href="category.html">Pants</a></li>
                                                <li><a href="category.html">Jackets & Outerwear</a></li>
                                                <li><a href="category.html">Activewear</a></li>
                                                <li><a href="category.html">Underwear</a></li>
                                                <li><a href="category.html">Swimwear</a></li>
                                                <li><a href="category.html">Shoes</a></li>
											</ul>
                                        </div>
										
										<div class="col-sm-2">
										<h5>KIDS</h5>
                                            <ul>
                                                <li><a href="category.html">View All</a></li>
                                                <li><a href="category.html">Baby</a></li>
                                                <li><a href="category.html">Girl</a></li>
                                                <li><a href="category.html">Boy</a></li>
											</ul>
                                        </div>
										
										<div class="col-sm-2">
										<h5>HANDBAGS</h5>
                                            <ul>
                                                <li><a href="category.html">View All</a></li>
											</ul>
                                        </div>
										
										
                                        <div class="col-sm-2">
										<h5>ACCESSORIES</h5>
                                            <ul>
                                                <li><a href="category.html">View All</a></li>
                                                <li><a href="category.html">Women</a></li>
                                                <li><a href="category.html">Men</a></li>
											</ul>
                                        </div>
										
										
										<div class="col-sm-2">
										<h5>SHOES</h5>
                                             <ul>
                                                <li><a href="category.html">View All</a></li>
                                                <li><a href="category.html">Women</a></li>
                                                <li><a href="category.html">Men</a></li>
											</ul>
                                        </div>
										
										
										 
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                </li>
				<!-- katapusan ng individual dropdown menu item -->
				
				<!-- umpisa ng individual dropdown menu item -->
				<li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">WOMEN <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										
                                            <ul>
                                                <li><a href="#">View All</a></li>
												<li><a href="#">Dresses</a></li>
												<li><a href="#">Denim</a></li>
												<li><a href="#">Tops</a></li>
												<li><a href="#">Rompers & Jumpsuits</a></li>
												<li><a href="#">Bottoms</a></li>
												<li><a href="#">Shorts</a></li>
												<li><a href="#">Skirts</a></li>
												<li><a href="#">Pants</a></li>
												<li><a href="#">Pants</a></li>
												<li><a href="#">Jackets & Outerwear</a></li>
												<li><a href="#">Swimwear</a></li>
												<li><a href="#">Lingerie</a></li>
												<li><a href="#">Activewear</a></li>
												<li><a href="#">Limited Pricing</a></li>
											</ul>
											
                                        </div>
										<div class="col-sm-2">
											<h5>FEATURES</h5>
                                            <ul>
												<li><a href="#">Best Sellers</a></li>
												<li><a href="#">Online Exclusives</a></li>
												<li><a href="#">Online Exclusives</a></li>
												<li><a href="#">The Logo Shop</a></li>
												<li><a href="#">Camilas Picks</a></li>
												<li><a href="#">Fall Essentials</a></li>
												<li><a href="#">Night Out</a></li>
												<li><a href="#">The Wedding Shop</a></li>
												<li><a href="#">Embroidered</a></li>
												<li><a href="#">Marciano</a></li>
												<li><a href="#">Gift Cards</a></li>
											</ul>
                                        </div>
                                        <div class="col-sm-4 text-center">
                                              <div class="banner">
                                                <a href="#"><img src="img/menu-women.jpg" alt="GUESS Women"></a><br><br>
												<a href="#">Shop New</a>
                                            </div>
                                        </div>
										<div class="col-sm-6"></div>
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                    </li>
                <!-- katapusan ng individual dropdown menu item -->
				
				<!-- umpisa ng individual dropdown menu item -->
				<li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">MEN <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										
                                            <ul>
                                                <li><a href="#">View All</a></li>
												<li><a href="#">Denim</a></li>
												<li><a href="#">Tanks & T Shirts</a></li>
												<li><a href="#">Shirts</a></li>
												<li><a href="#">Shorts & Pants</a></li>
												<li><a href="#">Shorts</a></li>
												<li><a href="#">Pants</a></li>
												<li><a href="#">Jackets & Outerwear</a></li>
												<li><a href="#">Activewear</a></li>
												<li><a href="#">Swimwear</a></li>
												<li><a href="#">Underwear</a></li>
												<li><a href="#">Suits</a></li>
												<li><a href="#">Shoes</a></li>
												<li><a href="#">Accessories</a></li>
												<li><a href="#">Limited Pricing</a></li>
											</ul>
											
                                        </div>
										<div class="col-sm-2">
											<h5>FEATURES</h5>
                                            <ul>
												<li><a href="#">Best Sellers</a></li>
												<li><a href="#">Online Exclusives</a></li>
												<li><a href="#">Online Exclusives</a></li>
												<li><a href="#">The Logo Shop</a></li>
												<li><a href="#">Moto Trend</a></li>
												<li><a href="#">Fall Essentials</a></li>
												<li><a href="#">Night Out</a></li>
												<li><a href="#">Casual Shirts</a></li>
												<li><a href="#">Gifts For Him</a></li>
												<li><a href="#">Gift Cards</a></li>
											</ul>
                                        </div>
                                        <div class="col-sm-4 text-center">
                                              <div class="banner">
                                                <a href="#"><img src="img/menu-men.jpg" alt="GUESS Men"></a><br><br>
												<a href="#">Shop New</a>
                                            </div>
                                        </div>
										 <div class="col-sm-6"></div>
										
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                    </li>
				<!-- katapusan ng individual dropdown menu item -->
				
				<!-- umpisa ng individual dropdown menu item -->
                <li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">DENIM <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										<h5>WOMEN</h5>
                                            <ul>
                                                <li><a href="#">View All</a></li>
												<li><a href="#">Skinny</a></li>
												<li><a href="#">High Rise</a></li>
												<li><a href="#">Mid & Low Rise</a></li>
												<li><a href="#">Bootcut & Flare</a></li>
												<li><a href="#">Straight & Relaxed Fit</a></li>
												<li><a href="#">Shorts & Skirts</a></li>
												<li><a href="#">Tops & Jackets</a></li>
											</ul>
											
                                        </div>
										<div class="col-sm-3">
											<h5>MEN</h5>
                                            <ul>
												<li><a href="#">View All</a></li>
												<li><a href="#">Skinny</a></li>
												<li><a href="#">Slim Tapered</a></li>
												<li><a href="#">Slim Straight & Regular Straight</a></li>
												<li><a href="#">Relaxed & Bootcut</a></li>
												<li><a href="#">Shorts</a></li>
												<li><a href="#">Shirts & Jackets</a></li>
												<li><a href="#">Moto & Destroyed</a></li>
											</ul>
											
                                        </div>
										 
                                        <div class="col-sm-4 text-center">
                                              <div class="banner">
                                                <a href="#"><img src="img/menu-denim.jpg" alt="GUESS Denim"></a><br><br>
												<a href="#">Shop Denim</a>
                                            </div>
                                        </div>
										 <div class="col-sm-5"></div>
										
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                </li>
				<!-- katapusan ng individual dropdown menu item -->
					
				<!-- umpisa ng individual dropdown menu item -->	
				<li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">HANDBAGS <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-3">
										
                                            <ul>
                                                <li><a href="#">View All</a></li>
                                                <li><a href="#">Wallets</a></li>
                                                <li><a href="#">Tote Bags</a></li>
                                                <li><a href="#">Satchels</a></li>
                                                <li><a href="#">Crossbody Bags</a></li>
                                                <li><a href="#">Crossbody Shoulder Bags</a></li>
                                                <li><a href="#">Backpacks</a></li>
                                                <li><a href="#">Mini Bags</a></li>
                                                <li><a href="#">Luggage & Travel Bags</a></li>
                                                <li><a href="#">Bag Accessories</a></li>
                                                <li><a href="#">Limited Pricing</a></li>
											</ul>
											
                                        </div>
										<div class="col-sm-3">
											<h5>FEATURES</h5>
                                            <ul>
												<li><a href="#">Best Sellers</a></li>
												<li><a href="#">Trending Now: Details</a></li>
												<li><a href="#">Night Out</a></li>
												<li><a href="#">Matching Sets</a></li>
												<li><a href="#">Logo</a></li>
												<li><a href="#">Online Exclusives</a></li>
												<li><a href="#">Gift Cards</a></li>
												<li><a href="#">Summer Whites</a></li>
											</ul>
										</div>
                                        <div class="col-sm-3 text-center">
                                            <div class="banner">
                                                <a href="#"><img src="img/menu-handbags.jpg" class="img img-responsive" alt="GUESS Handbags"></a><br>
												<a href="#">Shop Handbags</a>
                                            </div>
                                        </div>
										
										<div class="col-sm-3 text-center"></div>
										
										
										
                                    </div>
                                </div>
                               
                            </li>
                        </ul>
				<!-- katapusan ng individual dropdown menu item -->		
                 
				<!-- umpisa ng individual dropdown menu item -->	
				<li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">ACCESSORIES <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										<h5>WOMEN</h5>
                                            <ul>
                                                <li><a href="#">View All</a></li>
												<li><a href="#">Watches</a></li>
												<li><a href="#">Jewelry</a></li>
												<li><a href="#">Belts</a></li>
												<li><a href="#">Hats & Scarves</a></li>
												<li><a href="#">Sunglasses</a></li>
												<li><a href="#">Fragrance</a></li>
												<li><a href="#">Keychains</a></li>
												<li><a href="#">Phone Cases & Tech Accessories</a></li>
												<li><a href="#">Limited Pricing</a></li>
											</ul>
											
                                        </div>
										<div class="col-sm-2">
											<h5>MEN</h5>
                                            <ul>
												<li><a href="#">View All</a></li>
												<li><a href="#">Watches</a></li>
												<li><a href="#">Jewelry</a></li>
												<li><a href="#">Wallets & Bags</a></li>
												<li><a href="#">Belts</a></li>
												<li><a href="#">Hats</a></li>
												<li><a href="#">Sunglasses</a></li>
												<li><a href="#">Fragrance</a></li>
												<li><a href="#">Limited Pricing</a></li>
											</ul>
											
                                        </div>
										
                                        <div class="col-sm-4 text-center">
                                              <div class="banner">
                                                <a href="#"><img src="img/menu-accessories.jpg" alt="GUESS Accessories"></a><br><br>
												<a href="#">Shop Accessories</a>
                                            </div>
                                        </div>
										 <div class="col-sm-"></div>
										
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                    
					</li>
				<!-- katapusan ng individual dropdown menu item -->
				
				<!-- umpisa ng individual dropdown menu item -->	
				<li class="active dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">SALE <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										<h5>BABY</h5>
                                            <ul>
                                                <li><a href="#">View All</a></li>
                                                <li><a href="#">Baby Girl (0-24m)</a></li>
                                                <li><a href="#">Baby Boy (0-24M)</a></li>
											</ul>
                                        </div>
										<div class="col-sm-2">
										<h5>GIRL</h5>
                                            <ul>
                                                <li><a href="#">View All</a></li>
                                                <li><a href="#">Little Girl (2 -7)</a></li>
                                                <li><a href="#">Big Girl (7-16)</a></li>
											</ul>
                                        </div>
										<div class="col-sm-2">
                                        <h5>BOY</h5>
                                            <ul>
                                                <li><a href="#">View All</a></li>
                                                <li><a href="#">Little Boy (2 -7)</a></li>
                                                <li><a href="#">Big Boy (7-18)</a></li>
											</ul>
                                        </div>
										<div class="col-sm-2">
                                        <h5>FEATURES</h5>
                                            <ul>
                                                <li><a href="#">Best Sellers</a></li>
                                                <li><a href="#">Denim Shop</a></li>
                                                <li><a href="#">Back To School</a></li>
                                                <li><a href="#">Floral Shop</a></li>
                                                <li><a href="#">Gifts</a></li>
                                                <li><a href="#">Gift Cards</a></li>
											</ul>
                                        </div>
										<div class="col-sm-4 text-center">
                                              <div class="banner">
                                                <a href="#"><img src="img/menu-sale.jpg" alt="GUESS SALE"></a><br><br>
												<a href="#">Sale</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                    </li>
				<!-- katapusan ng individual dropdown menu item -->	
					
                </ul>

            </div>
<!-- katapusan para sa mga menu items -->			


            
			<div class="navbar-buttons">


                <div class="navbar-collapse collapse right"><!--id="search-not-mobile" -->
                  
					<a href="#" class="btn"   data-toggle="collapse" data-target="#search">
						 <span class="sr-only">Toggle search</span>
                        <i class="fa fa-search"></i>
					</a>
                       <a href="stores.html" class="btn"><i class="fa fa-map-marker"></i></a>
					   <a href="login.html" class="btn"><i class="fa fa-user"></i></a>
					   <a href="basket.html" class="btn"><i class="fa fa-shopping-bag"></i></a>
					   
				 
					
					
				</div>
				 
            </div>

           
			<!-- umpisa ng search toggle -->
		   <div class="collapse clearfix" id="search">

                <form class="navbar-form" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search">
                        <span class="input-group-btn">

			    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
				</span>
                    </div>
                </form>

            </div>
			<!-- katapusan ng search toggle --> 

			
        </div>
</div>  	
<!-- *** KATAPUSAN NG NAVBAR ***  -->




 <!-- umpisa ng maliit na promotional item 120px height. pwedeng hard coded or isang image lang -->
	<section id="ad-30">
	<div class="container">
	 
	<div class="col-sm-3 black text-center">ONLINE ONLY</div>
	<div class="col-sm-6 text-center">
	<span class="black-bold">50% OFF</span> <span class="black">SALE STYLES</span> + 
	<span class="red-bold">EXTRA 10%</span> <span class="red">AT CHECKOUT</span>
	</div>
	
	<div class="col-sm-3 text-center">
	<a href="#" class="black link">WOMEN</a><i class="fa fa-play"></i>
	<a href="#" class="black link">MEN</a> 
	<a href="#" class="black small">DETAILS</a>
	</div>
	
 
	</div>
	</section>
 <!-- katapusan ng maliit na promotional item 120px height. pwedeng hard coded or isang image lang -->

 

<!-- *** UMPISA PARA SA OVERALL WEBSITE CONTENTS -->  
<section id="pinakalaman">

 
 
 <!-- UMPISA NG MAIN ONTENTS --> 
 <div class="container">
			<h1>Terms & Conditions</h1>
			
			 
	
<p>Welcome to GUESS Philippines. Please review the following basic rules governing your use of and purchase of products from this site (collectively, "Agreement").</p>
<p>THESE TERMS AND CONDITIONS REQUIRE BINDING ARBITRATION ON AN INDIVIDUAL BASIS TO RESOLVE DISPUTES AND INCLUDE A CLASS ACTION WAIVER. IN THE EVENT OF A DISPUTE, YOU ARE WAIVING YOUR RIGHT TO SUE IN COURT. THESE TERMS INCLUDE A DISCLAIM OF WARRANTIES AND LIMITATION OF LIABILITY THAT MAY APPLY TO YOU IN THE EVENT OF A DISPUTE.</p>
<p><strong>Use Of This Site Constitutes Acceptance Of These Terms And Conditions</strong></p>
<p>GUESS Philippines may make changes to the information contained in this Internet site, or to the services described and contained therein at any time and without notice. GUESS Philippines may from time to time change the rules that govern your use of this Internet site. Your use of this Internet site following any such change constitutes your agreement to follow and be bound by the rules as changed. GUESS Philippines may change, move or delete portions of, or may add to this site from time to time.</p>
<h3>Use Restrictions</h3>
<p>All trademarks, service marks and trade names on this site, including without limitation, "GUESS" and "GUESS" alone and in combination with the inverted triangle design are property of GUESS, Inc. "GUESS". No use of these may be made without the prior written authorization of GUESS Philippines, except to identify the products or services of GUESS Philippines. The products, logos, designs, advertisements and images described and/or displayed on this site may be intellectual property with rights reserved by GUESS Philippines or by other third parties. No license is granted with respect to these intellectual property rights.</p>
<p>All materials, including images, illustrations, designs, icons, photographs, video clips, and written and other materials appearing on this site (collectively,"Content") are copyrights, trademarks, trade dress and/or other intellectual properties owned, controlled or licensed by GUESS Philippines collectively, and its subsidiaries and/or affiliates ("GUESS Philippines"). This Internet site as a whole is protected by copyright and trade dress, all worldwide rights, titles and interests in and to which are owned by GUESS Philippines and all other GUESS Philippines trademarks appearing on this site are trademarks of GUESS Philippines.</p>
<p>The Contents of this Internet site, and this site as a whole, are intended solely for personal, noncommercial use by the users of this site. You may download or copy the Contents and other downloadable materials displayed on this site for your personal, non-commercial use only, provided you do not modify, obscure, or delete any copyright or other propriety notices on this site. No right, title or interest in any downloaded materials or software is transferred to you as a result of any such downloading or copying. You may not reproduce (except as noted above), publish, transmit, distribute, display, modify, create derivative works from, sell or participate in any sale of, or exploit in any way, in whole or in part, any of the Contents, this site, or any related software.</p>
<h3>Communications and Submissions</h3>
<p>All communications, comments, feedback, postcards, suggestions, ideas, and other submissions disclosed, submitted or offered to GUESS Philippines on or by this site or otherwise disclosed, submitted or offered in connection with your use of this site ("Submissions") shall be and remain GUESS Philippines property. Such disclosure, submission or offer of any comments shall constitute an assignment to GUESS Philippines of all worldwide rights, titles and interests in all copyrights and other intellectual properties in the submissions. GUESS Philippines will own exclusively all such rights, titles and interests and shall not be limited in any way in its use, commercial or otherwise, of any submissions. GUESS Philippines shall be free to use the content of any such communications, including any ideas, inventions, concepts, techniques or knowledge disclosed therein, for any purpose, including the development, manufacturing and/or marketing of goods or services. By your participation in surveys, contests or promotions on this site, and/or the request of promotional information or product updates, you hereby agree that GUESS Philippines may use information about you for marketing, product development and promotional purposes. GUESS Philippines is under no obligation to maintain any submissions in confidence, to pay user any compensation for any submissions, or to respond to user submissions. You agree that no submissions by you to this site will violate any right of any third party, including copyright, trademark, privacy or other personal or proprietary right(s). In addition, you agree that no submissions by you to this site will be or contain libelous or otherwise unlawful, abusive or obscene material. You are and shall remain solely responsible for the content of any submissions you make.</p>
<h3>Products</h3>
<p>GUESS Philippines products are available in select GUESS stores, better department stores, and specialty stores. GUESS Philippines cannot guarantee that the products displayed on this site will be available at every authorized GUESS retailer; availability and selection are subject to supply. Product descriptions, specifications, colors, and pricing are subject to change by GUESS Philippines at any time without notice. Prices displayed at the site are quoted in U.S. Dollars and are valid and effective only in the United States.</p>
<h3>Links</h3>
<p>This site may provide links or references to other sites. However, GUESS Philippines assumes no responsibility for the content of such other sites and shall not be liable for any damages or injury arising from that content.</p>
<h3>Disclaimer</h3>
<p>THE INFORMATION AND MATERIALS CONTAINED IN THIS SITE ARE PROVIDED "AS IS" AND WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESSED OR IMPLIED. GUESS.COM DISCLAIMS ALL WARRANTIES, EXPRESSED OR IMPLIED, INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANIBILITY AND FITNESS FOR A PARTICULAR PURPOSE TO THE FULLEST EXTENT PERMISSIBLE UNDER THE APPLICABLE LAW. YOU ACKNOWLEDGE, BY YOUR USE OF THIS SITE, THAT YOUR USE OF THIS SITE IS AT YOUR SOLE RISK, THAT YOU ASSUME FULL NECESSARY SERVICING OR REPAIRS OF ANY EQUIPMENT YOU USE IN CONNECTION WITH YOUR USE OF THIS SITE, AND THAT GUESS SHALL NOT BE LIABLE FOR ANY DAMAGES OF ANY KIND RELATED TO YOUR USE OF THIS SITE.</p>
<p><strong>NJ Residents</strong>: the Disclaimer section of this Agreement is inapplicable to you.</p>
<h3>Limitation Of Liability</h3>
<p>You agree to defend, indemnify and hold GUESS Philippines harmless from and against any and all claims, damages, costs and expenses, including attorney's fees, arising from or related to your use of this site, GUESS Philippines shall not be liable for any damages or injury resulting from your access to, or inability to access this site, or from your reliance on any information contained in this site.</p>
<p><strong>NJ Residents</strong>:the Limitation Of Liability section of this Agreement is inapplicable to you.</p>
<p><strong>Dispute Resolution &ndash; BINDING ARBITRATION, JURY TRIAL WAIVER, AND CLASS ACTION WAIVER</strong></p>
<p><strong>Any dispute, claim, or controversy arising from or relating to this Agreement, your use of GUESS Philippines, purchases made through GUESS Philippines, or to any products or services sold, distributed, or manufactured by Guess?, Inc. (&ldquo;GUESS&rdquo;) will be resolved by binding arbitration, rather than in court (<em>i.e.</em>, you are waiving your right to sue in court)</strong>, except that you may assert claims in small claims court if your claims qualify. Disputes subject to arbitration include without limitation disputes arising out of or relating to interpretation or application of this dispute resolution provision. The Federal Arbitration Act and federal arbitration law apply to this dispute resolution agreement.</p>
<p><strong>There is no judge or jury in arbitration, and court review of an arbitration award is limited. However, an arbitrator can award on an individual basis the same damages and relief as a court (including injunctive and declaratory relief or statutory damages), and must follow the terms of this Agreement as a court would.</strong></p>
<p>The arbitration will be conducted by JAMS, Inc. under its rules by a single arbitrator experienced in commercial disputes. The JAMS&rsquo;s rules are available at <a href="http://www.jamsadr.">www.jamsadr.</a>org or by calling 1-800-352-5267. You may choose to have the arbitration conducted by telephone, based on written submissions, or in person in the county where you live or at another mutually agreed location. The arbitrator shall issue a written award setting forth the arbitrator&rsquo;s complete determination of the dispute and the factual findings and legal conclusions relevant to it. The arbitrator&rsquo;s award may be entered in any court of competent jurisdiction.</p>
<p>Payment of all filing, administration, and arbitrator fees will be governed by the JAMS&rsquo;s rules. However, unless the arbitrator determines that your claim(s) were frivolous or asserted in bad faith [a] we will reimburse those fees for claims totaling less than $10,000, and [b] in the event that you are able to demonstrate that the costs of arbitration will be prohibitive as compared to the costs of litigation, GUESS will pay as much of your filing and hearing fees in connection with the arbitration as the arbitrator deems necessary to prevent the arbitration from being cost-prohibitive as compared to the costs of litigation, regardless of the outcome of the arbitration. Likewise, GUESS will not seek attorneys&rsquo; fees and costs in arbitration unless permitted by law and the arbitrator determines the claim(s) asserted against GUESS are frivolous or brought in bad faith.</p>
<p>YOU AND GUESS EACH AGREE THAT ANY DISPUTE RESOLUTION PROCEEDINGSHALL BE CONDUCTED ON AN INDIVIDUAL BASIS AND NOT IN A CLASS, CONSOLIDATED, OR REPRESENTATIVE ACTION. Notwithstanding any provision in this Agreement to the contrary, if the class, consolidated, and representative action waiver in the prior sentence is deemed invalid or unenforceable, neither you nor GUESS are entitled to arbitration.</p>
<p>This Agreement is governed by the laws of the State of California without regard to conflict of law principles.</p>
<p>If the arbitration provision in this section is found unenforceable or to not apply for a given dispute, then the proceeding must be brought exclusively in a court of competent subject matter jurisdiction &ndash; whether federal or state &ndash; within the Central or Southern Districts of California or, if applicable, state courts for the counties within those two federal districts.</p>
<p>Notwithstanding any other provision in this Agreement to the contrary, to the extent GUESS makes a material change to this dispute resolution provision, such change will not apply to any dispute that you provided Guess notice of prior to the change in the dispute resolution provision.</p>
<p>To provide us notice of an arbitration demand, send the demand to our registered agent: Guess?, Inc., c/o CSC &ndash; Lawyers Incorporating Service, 2710 Gateway Oaks Drive, Suite 150N, Sacramento, CA 95833.</p>
<h3>Miscellaneous</h3>
<p>This site and the Contents therein are displayed solely for the purpose of promoting GUESS Philippines products and services. This site is controlled and operated by GUESS Philippines from its offices in Los Angeles, California. GUESS Philippines reserves the right to make changes to the information in this site, or to the services described therein, at any time without notice. GUESS Philippines makes no commitment to update this information.</p>
<h3>Duration</h3>
<p>This Agreement remains in effect unless and until terminated by either you or GUESS Philippines. You may terminate this Agreement at any time. GUESS Philippines may also terminate this Agreement at any time and may do so immediately without notice, and accordingly deny you access to the site if in GUESS Philippines's sole discretion you fail to comply with any term or provision of this Agreement. Upon any termination of this Agreement by either you or GUESS Philippines, you must promptly destroy all materials downloaded or otherwise obtained from this site, as well as all copies of such materials, whether made under the terms of this Agreement or otherwise.</p>
<h3>Sales and Promotions</h3>
<p>Unless otherwise stated, prices on sale and promotional items cannot be combined with any other offers. Sales and promotions may not include all sizes, colors or styles. Quantities are limited; no rain checks will be issued. Offers are not valid on previously purchased merchandise or purchases of gift cards. Sale and promotion start and end times are displayed in US Eastern time. GUESS?, Inc. reserves the right to extend, modify or discontinue any offer at any time without notice.</p>
<h3>Limits on Purchases</h3>
<p>In an effort to enhance your shopping experience and give as many customers as possible the opportunity to purchase our product, we may place limits on purchases and reject purchases or orders for commercial quantities of our products. Products are not offered for resale. We also may, among other things, restrict orders placed by or under the same customer account, the same credit card, loyalty program account and/or orders that use the same billing and/or shipping address. We may also reserve the right to cancel any portion of an order that cannot be fulfilled for any reason. Online orders are not deemed to be accepted by GUESS? Inc. until your products are shipped from our warehouse. We reserve the right to limit, cancel or prohibit orders that, in our judgment, appear to be placed in violation of this policy and the right to cease doing business with customers who violate this policy. This policy applies to all purchases of GUESS? INC. product, including, but not limited to, all purchases made at our retail stores, sample sales, warehouse sales, and through our websites. We may modify this policy at any time without prior notice.</p>
<br/><br/>
<h1 id="guessList">GUESS List Loyalty Program Terms &amp; Conditions</h1>
<p>THESE TERMS AND CONDITIONS REQUIRE BINDING ARBITRATION ON AN INDIVIDUAL BASIS TO RESOLVE DISPUTES AND INCLUDE A CLASS ACTION WAIVER. IN THE EVENT OF A DISPUTE, YOU ARE WAIVING YOUR RIGHT TO SUE IN COURT. THESE TERMS INCLUDE A DISCLAIMER OF WARRANTIES AND LIMITATION OF LIABILITY THAT MAY APPLY TO YOU IN THE EVENT OF A DISPUTE.</p>
<p>Use of the GUESS List Loyalty Card constitutes acceptance of the following terms and conditions and all future Program rules (this "Agreement") by you as a GUESS List Loyalty Program member ("Member"):</p>
<ol>
<li>BENEFITS.<ol>
<li>The GUESS List Loyalty Program (the "Program") provides GUESS, GUESS Factory, G by GUESS, GUESS KIDS and Marciano customers with the following benefits subject to the terms and conditions contained in this Agreement (collectively, the "Benefits"):</li>
<li>Effective September 30, 2012 all new Program enrollments will automatically receive 50 bonus Points towards their first Reward.</li>
<li>Shop at Participating Stores (as defined in Section 6 below), present your GUESS List Loyalty Card prior to checkout (or enter your Loyalty Card number where indicated for online purchases) and receive one (1) point for nearly every dollar you spend on Qualifying Purchases (each a "Point").</li>
<li>For every 200 Points you earn you will receive a $10 Reward applicable towards your next purchase at a Participating Store (each a "Reward"). Rewards issued will expire automatically after 60 days.</li>
<li>In addition, as you use your GUESS List Loyalty Card, you may be eligible to receive special discounts, earn double points, or participate in other exciting GUESS promotions including in-store events, fashion shows, exclusive parties and store openings.</li>
<li>GUESS List Noir. GUESS List Members who spend more than $500 in Qualifying Purchases in a rolling 12-month period will become eligible to upgrade automatically to GUESS List Noir status.You must continue to spend $500 in Qualifying Purchases every 12 months to continue to receive GUESS List Noir benefits. Members who no longer qualify for GUESS List Noir because they failed to make the minimum purchase requirement for GUESS List Noir status will be automatically returned to the standard level of benefits associated with GUESS List. As a GUESS List Noir member, you are eligible for additional benefits, including:</li>
<li>GUESS List Noir Members are eligible for increased Rewards. For every 200 points accumulated, GUESS List Noir Members receive a $15.00 Reward. Reward is valid for 60 days after issuance. The Reward can be redeemed at all Participating Stores.</li>
<li>To celebrate your birthday, GUESS List Noir Members are also eligible to receive double points all month long, plus a bonus $20.00 Reward valid for the entire month. Reward will expire on the last day of the month. The Reward can be redeemed at all Participating Stores.</li>
<li>Receive free shipping for all qualifying online purchases. Free shipping is valid as long as the Member maintains GUESS List Noir status.</li>
</ol></li>
<li>ENROLLMENT.<ol>
<li>No purchase is necessary to become a Member. To apply, you must either (1) visit a Participating Store and provide your full name, a valid e-mail address and your current zip code to receive your GUESS List Loyalty Card and then go online to www.GUESS Philippines or www.GUESS.ca to complete additional enrollment information or (2) simply apply online at www.GUESS Philippines or www.GUESS.ca, and provide the information above plus your complete mailing address so your GUESS List Loyalty Card can be mailed to you. You must provide a valid e-mail address to be eligible to receive any Benefits associated with the Program.</li>
<li>Enrollment begins upon activation in store or online but you can opt out of the Program at any time as more fully set forth in Section 8 below.</li>
<li>Member must be a legal resident of the United States, Puerto Rico or Canada and 13 years of age or older to participate in the Program. The sponsors of the Program are GUESS?, Inc. and GUESS? Canada Corporation (collectively, "GUESS"). Employees of GUESS, its subsidiaries and affiliates, and their immediate families, and others who are eligible for employee-based discounts, or Members of any corporate or wholesale account of GUESS are not eligible for Membership in the Program and Program Benefits may not be applied towards transactions by such individuals. GUESS reserves the right in its sole discretion to accept, deny, suspend or discontinue any Membership for the Program.</li>
</ol></li>
<li>GENERAL TERMS AND CONDITIONS.<ol>
<li>GUESS List Loyalty Membership is not transferable and may only be used by an individual Member for the Member's personal benefit. Member Benefits cannot be combined with Benefits accrued by another Member. GUESS may request proper identification with regard to Program transactions. Member Benefits will not be applied retroactively to any purchases. Other exclusions may apply.</li>
<li>GUESS is not responsible for lost or stolen Membership cards. GUESS may, in its sole discretion, permit replacement cards to be issued and any remaining points at the time GUESS issues the replacement card may be transferred. If GUESS agrees to issue a replacement card to the Member, GUESS assumes no responsibility for replacing any points or Rewards accrued prior to the date GUESS is notified in writing of the Member's loss.</li>
<li>Member agrees that the Program and any Benefits related thereto do not create any property rights whatsoever in favor of Member.</li>
<li>Member shall be responsible for maintaining accurate and updated personal information with the GUESS List Loyalty Membership Program Department. Members may update their personal information online at www.GUESS Philippines and www.GUESS.ca or at any Participating Store. GUESS is not responsible for Rewards or other Benefits that are not received as a result of loss or Member's failure to update Member's personal information.</li>
<li>GUESS, the GUESS logos, the GUESS List Loyalty Program and other logos and taglines are the intellectual property of GUESS (or their affiliates), licensed to others under controlled conditions.</li>
<li>Membership rules are void where and to the extent prohibited by law. Taxes may apply where required by law.</li>
<li>Resale Policy - Products offered by GUESS are not offered for resale; by signing up for the GUESS List Loyalty Program, you expressly acknowledge and agree that you are entering an agreement with GUESS that you will not offer for sale, sell, transship or otherwise engage in any commercial activity (or permit any third parties acting on your behalf to do so) in relation to any products purchased from us using your Loyalty account; if you are suspected of breaching this provision, in addition to immediately suspending your Membership and shopping privileges with GUESS, you agree that GUESS may pursue all legal and equitable remedies available against you or anyone acting on your behalf for such breach, including but not limited to, monetary damages, injunctions and/or other remedies. Additionally, we may place limits on purchases and reject purchases or orders suspected of consisting of commercial quantities of our products. We may, among other things, restrict orders placed by or under the same customer account, the same credit card, loyalty program account and/or orders that use the same billing and/or shipping address. We may also reserve the right to cancel any portion of an order that cannot be fulfilled for any reason. Online orders are not deemed to be accepted by GUESS until your products are shipped from our warehouse; however an acceptance of an order by GUESS does not constitute a waiver of our rights under our resale policy. We reserve the right to limit, cancel or prohibit orders that, in our judgment, appear to be placed in violation of this policy and the right to cease doing business with customers who violate this policy. This policy applies to all purchases of GUESS product, including, but not limited to, all purchases made at our retail stores, sample sales, warehouse sales, and through our websites. We may modify this policy at any time without prior notice.</li>
</ol></li>
<li>POINTS AND REWARDS ACCRUAL.<ol>
<li>Points accrual will be calculated based on Member's Qualifying Purchases. "Qualifying Purchases" are purchases made at Participating Locations, excluding purchases of gift cards and certain merchandise items that are excluded in particular promotions and shall be calculated based on the final sale price of merchandise after any applicable discounts and coupons are applied (including Rewards redeemed) and before sales tax and shipping and handling (if applicable) is added. Points shall be calculated based on the final sale price in the local currency. Other exclusions may apply. Earn additional Points through special offers and promotions.</li>
<li>Points will post to your account within 30 days of in-store purchase, date of on-line purchase or date of shipment. No adjustments to your account ("Account") will be made after the actual purchase date except as indicated below.</li>
<li>Points apply only to the first GUESS List Loyalty Card scanned in connection with any purchase.</li>
<li>Points earned from a Qualifying Purchase shall be deducted from Member's account if the Qualifying Purchase is returned or exchanged.</li>
<li>Points shall expire automatically 6 months from the date of your last purchase using your GUESS List Loyalty Card. Points have no cash value and may not be redeemed for partial Rewards, merchandise or any other value.</li>
<li>Once a GUESS List Member accrues 200 Points, those Points will be automatically deducted from Member's Account and a Reward in the amount of $10 shall be issued to Member's GUESS List Loyalty Card. Once a GUESS List Noir Member accrues 200 Points, those Points will be automatically deducted from Member&rsquo;s Account and a Reward in the amount of $15 shall be issued to Member&rsquo;s GUESS List Noir card. Rewards will be issued to your Loyalty Card approximately 72 hours after reaching the appropriate Points accrual level. All Rewards shall be issued in local currency, if you use your Reward in a foreign country, the applicable exchange rate value of your Reward shall apply to your purchase.</li>
<li>GUESS may adjust the number of Points required to be eligible for a Reward or the Reward amount to be issued, at any time, in its sole discretion, with or without notice.</li>
<li>Rewards are coupons for discounts off future purchases and may only be redeemed for a discount off purchase amounts equal to or greater than the amount of the sum of the Rewards presented for redemption at Participating Stores, excluding tax. Rewards shall not be refunded if you return or exchange merchandise purchased using a Reward coupon. Points earned towards such Reward shall not be reinstated to your account.</li>
<li>Any unused Points and Rewards shall be forfeited if Member's Membership in the Program is cancelled for any reason.</li>
</ol></li>
<li>REWARDS REDEMPTION.<ol>
<li>Rewards are redeemable at Participating Stores for future purchases of any eligible merchandise available at Participating Stores only and cannot be used towards the purchase of gift cards or certain other merchandise. You may be notified of additional ineligible merchandise. Rewards have no cash value, are non-transferable, are not applicable to prior purchases or exchange transactions, and no change will be given for Reward redemptions. Rewards may only be used on time only. Other restrictions may apply.</li>
<li>Rewards may be combined with other Rewards issued to Member&rsquo;s Loyalty Card and may be used in conjunction with most other discounts (excluding employee discounts), promotions or offers towards the purchase of merchandise not to exceed the purchase amount at Participating Stores only.</li>
<li>Unless otherwise indicated, Rewards will expire and are forfeited if not redeemed within 60 days from the date issued.</li>
</ol></li>
<li>PARTICIPATING STORES.<ol>
<li>The GUESS List Loyalty Card is accepted at most GUESS, GUESS Accessory, GUESS Factory, G by GUESS and Marciano stores located in the United States (including Puerto Rico) and Canada and for most products available online at www.GUESS Philippines, www.GUESS.ca and www.ShopMarciano.ca (&ldquo;Participating Stores&rdquo;). The GUESS List Loyalty Card is not accepted at the following locations: international locations; certain GUESS, GUESS Accessory and Marciano stores in the US and Canada as may be determined by GUESS from time to time; any third-party locations carrying GUESS products within the United States, Puerto Rico or Canada; for telephone orders at any Participating Stores; or through third-party online websites. The GUESS List program is not accepted in the Province of Manitoba. Certain restrictions apply.</li>
</ol></li>
<li>MEMBER PRIVACY.<ol>
<li>By becoming a Member of the Program, you agree to receive advertising, marketing materials and other communications from GUESS and its affiliates. The information we collect will be used to help us provide a more personalized shopping experience and personalized discounts applicable to purchases at Participating Stores. The personal information you provide as a Member of the Program will be handled and retained in accordance with the GUESS Privacy Policy located on www.GUESS Philippines or www.GUESS.ca.</li>
<li>By providing your mobile number for the GUESS List program, you are agreeing to subscribe to GUESS Mobile Alerts to receive up to 5 text messages per month. Standard text messaging rates apply. Not available on all handsets/carriers. To unsubscribe at any time, text STOP to GUESS. For more information on GUESS Mobile Alerts, please visit www.GUESS Philippines or www.GUESS.ca.</li>
</ol></li>
<li>MODIFICATION AND TERMINATION.<ol>
<li>GUESS may terminate or modify the Program and/or these Terms and Conditions from time to time without notice and without any further obligations to Members. Updates shall be effective on the date they are posted on the websites below; continued use of Member's Loyalty Card after such date constitutes acceptance of all such changes. The most recent terms and conditions of the Program can be found at www.GUESS Philippines or www.GUESS.ca.</li>
<li>GUESS may also terminate the Program at any time through written notice sent to Member via e-mail (Members without valid e-mails will be notified the next time their card is presented at a Participating Location). Upon termination of the Program, Member shall forfeit all unredeemed Points earned as of the termination date but any Rewards issued shall remain in full force through their individual expiration dates. Members may choose to opt out of the Program at any time by providing notice by mail or e-mail; however, all Rewards and Points shall be forfeited at such time. Cancellation may take from 6 to 8 weeks to finalize. GUESS reserves the right to terminate your Membership at any time in its sole discretion for any reason.</li>
<li>All correspondence for the GUESS List Loyalty Card should be addressed to:<br/> GUESS List Loyalty Program<br/> Attn: Customer Care<br/> 1444 S. Alameda Street<br/> Los Angeles CA, 90021</li>
</ol></li>
<li>GOVERNING LAW AND ARBITRATION - BINDING ARBITRATION, JURY TRIAL WAIVER, AND CLASS ACTION WAIVER<ol>
<li>Any dispute, claim, or controversy arising from or relating to this Agreement or Member's Membership will be resolved by binding arbitration, rather than in court (i.e., you are waiving your right to sue in court), except that you may assert claims in small claims court if your claims qualify. Disputes subject to arbitration include without limitation disputes arising out of or relating to interpretation or application of this dispute resolution provision. The Federal Arbitration Act and federal arbitration law apply to this dispute resolution agreement.</li>
<li>There is no judge or jury in arbitration, and court review of an arbitration award is limited. However, an arbitrator can award on an individual basis the same damages and relief as a court (including injunctive and declaratory relief or statutory damages), and must follow the terms of this Agreement as a court would.</li>
<li>The arbitration will be conducted by JAMS, Inc. under its rules by a single arbitrator experienced in commercial disputes. The JAMS&rsquo;s rules are available at www.jamsadr.org or by calling 1-800-352-5267. You may choose to have the arbitration conducted by telephone, based on written submissions, or in person in the county where you live or at another mutually agreed location. The arbitrator shall issue a written award setting forth the arbitrator&rsquo;s complete determination of the dispute and the factual findings and legal conclusions relevant to it. The arbitrator&rsquo;s award may be entered in any court of competent jurisdiction.</li>
<li>Payment of all filing, administration, and arbitrator fees will be governed by the JAMS&rsquo;s rules. However, unless the arbitrator determines that your claim(s) were frivolous or asserted in bad faith [a] we will reimburse those fees for claims totaling less than $10,000, and [b] in the event that you are able to demonstrate that the costs of arbitration will be prohibitive as compared to the costs of litigation, GUESS will pay as much of your filing and hearing fees in connection with the arbitration as the arbitrator deems necessary to prevent the arbitration from being cost-prohibitive as compared to the costs of litigation, regardless of the outcome of the arbitration. Likewise, GUESS will not seek attorneys&rsquo; fees and costs in arbitration unless permitted by law and the arbitrator determines the claim(s) asserted against GUESS are frivolous or brought in bad faith.</li>
<li>You and Guess each agree that any dispute resolution proceeding SHALL BE CONDUCTED ON AN INDIVIDUAL BASIS AND NOT IN A CLASS, CONSOLIDATED, OR REPRESENTATIVE ACTION. Notwithstanding any provision in this Agreement to the contrary, if the class, consolidated, and representative action waiver in the prior sentence is deemed invalid or unenforceable, neither you nor GUESS are entitled to arbitration.</li>
<li>This Agreement is governed by the laws of the State of California without regard to conflict of law principles. If the arbitration provision in this section is found unenforceable or to not apply for a given dispute, then the proceeding must be brought exclusively in a court of competent subject matter jurisdiction &ndash; whether federal or state &ndash; within the Central or Southern Districts of California or, if applicable, state courts for the counties within those two federal districts.</li>
<li>Notwithstanding any other provision in this Agreement to the contrary, to the extent GUESS makes a material change to this dispute resolution provision, such change will not apply to any dispute that you provided Guess notice of prior to the change in the dispute resolution provision.</li>
<li>To provide us notice of an arbitration demand, send the demand to our registered agent: Guess?, Inc., c/o CSC &ndash; Lawyers Incorporating Service, 2710 Gateway Oaks Drive, Suite 150N, Sacramento, CA 95833.</li>
</ol></li>
<li>NO WARRANTY.<ol>
<li>GUESS AND ITS AFFILIATE ENTITIES, AND THEIR RESPECTIVE OFFICERS, DIRECTORS, EMPLOYEES AND AGENTS MAKE NO WARRANTY, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE WITH RESPECT TO THE PROGRAM OR ANY PRODUCTS OR SERVICES RELATED THERETO AND EXPRESSLY DISCLAIMS ANY SUCH WARRANTIES.</li>
<li>NJ Residents: The No Warranty section of this Agreement is inapplicable to you.</li>
</ol></li>
<li>RELEASE OF LIABILITY.<ol>
<li>MEMBER HEREBY RELEASES GUESS AND ITS AFFILIATE ENTITIES AND THEIR RESPECTIVE OFFICERS, DIRECTORS, EMPLOYEES AND AGENTS FROM AND AGAINST ANY AND ALL LIABILITY ARISING FROM OR RELATING TO THE PROGRAM, MEMBER'S MEMBERSHIP IN THE PROGRAM OR THIS AGREEMENT.</li>
<li>NJ Residents: The Release of Liability section of this Agreement is inapplicable to you.</li>
</ol></li>
</ol>	
		
	 		 
			
</div>	
 
</section>
<!-- *** KATAPUSAN PARA SA OVERALL WEBSITE CONTENTS -->   


    

	
	
	
	

<!-- *** START FOOTER *** -->
<section id="footer" data-animate="fadeInUp">
	 
			<div class="container">
			<div class="col-sm-3"></div>
			
			<div class="col-sm-6 text-center">
			<p>Enjoy 15% OFF & Free Shipping on your first order</p>
			
						<form>
                            <div class="input-group">
							<input type="text" class="form-control">
								<span class="input-group-btn"><button class="btn btn-danger" type="button">SIGNUP NOW!</button></span>
							</div>
                            <!-- /input-group -->
						<p>
						<input type="checkbox"> By signing up I agree to receive emails about the latest trends and exclusive content.
						</p>
                        </form>
			</div>
			
			<div class="col-sm-3"></div>
			
			</div>
			
			<br><br>
		
			
            <div class="container">
			
			 <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <h4>MY ACCOUNT</h4>

                        <ul>
                            <li><a href="register.html">Register</a></li>
                            <li><a href="login.html">Login</a></li>
                            <li><a href="orders.html">Orders</a></li>
                            <li><a href="returns.html">Returns</a></li>
                         </ul>
 

                       

                        <hr class="hidden-md hidden-lg hidden-sm">

                    </div>
                    <!-- /.col-md-3 -->

                    <div class="col-md-3 col-sm-6">

                        <h4>ASSISTANCE</h4>
						<ul>
                            <li><a href="support.html">Customer Care</a></li>
                            <li><a href="stores.html">Store Locator</a></li>
                            <li><a href="contact.html">Contact Us</a></li>
                            <li><a href="gift.html">Gift Cards</a></li>
                        </ul>
 

                        <hr class="hidden-md hidden-lg">

                    </div>
                    <!-- /.col-md-3 -->

                    <div class="col-md-3 col-sm-6">

                        <h4>COMPANY</h4>
						<ul>
                            <li><a href="stores.html">Our Store</a></li>
                            <li><a href="careers.html">Corporate Careers</a></li>
                            <li><a href="careers-retail.html">Retail Careers</a></li>
                        </ul>
						
                        <p class="hidden">
						<strong>GUESS Philippines</strong>
                            <br>DII Bldg. 
                            <br>150 San Vicente Rd
                            <br>Brgy. San Vicente
                            <br>Laguna
                            <br>
                            <strong>Philippines</strong>
                        </p>
 
                        <hr class="hidden-md hidden-lg">

                    </div>
                    <!-- /.col-md-3 -->



                    <div class="col-md-3 col-sm-6">

                        <h4>FOLLOW US</h4>
 
                        <p>
                            <a href="https://www.facebook.com/GUESSPhil" class="facebook external" target="_blank" data-animate-hover="none"><i class="fa fa-facebook-square fa-2x"></i></a>
                            <a href="https://twitter.com/GUESS" class="twitter external" target="_blank" data-animate-hover="none"><i class="fa fa-twitter fa-2x"></i></a>
                            <a href="https://www.instagram.com/guess" class="instagram external" target="_blank" data-animate-hover="none"><i class="fa fa-instagram fa-2x"></i></a>
                            <a href="https://www.pinterest.com/guess" class="gplus external" target="_blank" data-animate-hover="none"><i class="fa fa-pinterest-p fa-2x"></i></a>
                            <a href="https://www.youtube.com/user/GUESSincTV" class="email external" target="_blank" data-animate-hover="none"><i class="fa fa-youtube-play fa-2x"></i></a>
                        </p> 

                    </div>
                    <!-- /.col-md-3 -->

					
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
			<br><br>
			<p class="small text-center">
			&copy; <script type="text/javascript">document.write(new Date().getFullYear());</script> GUESS Philippines. All rights reserved.
			<a href="terms.html">Terms & Conditions</a> <a href="privacy.html">Privacy Policy</a>
			</p>
			
			
			
			
			
        </section>
<!-- *** END FOOTER *** -->




<!-- *** START COPYRIGHT *** -->
<section id="copyright" class="hidden">
<div class="container ">
		 
			
		 
                <div class="col-md-6">
                    <p class="pull-left">© <script type="text/javascript">document.write(new Date().getFullYear());</script> GUESS Philippines. All rights reserved.</p>

                </div>
                <div class="col-md-6">
                    <p class="pull-right">
					<a href="terms.html">Terms & Conditions</a> <a href="privacy.html">Privacy Policy</a>
                        
                    </p>
                </div>
			
			
</div>
</section>
<!-- *** COPYRIGHT END *** -->


	
	
	 

    <!-- *** SCRIPTS TO INCLUDE ***
 _________________________________________________________ -->
    <script src="js/jquery-1.11.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.cookie.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/modernizr.js"></script>
    <script src="js/bootstrap-hover-dropdown.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/front.js"></script>
	 
</body>

</html>