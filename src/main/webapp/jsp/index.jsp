<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>    
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="For GUESS Philippines">
    <meta name="author" content="JETHER.NET">
    <meta name="keywords" content="">
	<title>
        GUESS Philippines
    </title>

    <meta name="keywords" content="">

 

    <!-- styles -->
    <link href="<c:url value="/resources/css/font-awesome.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/animate.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/owl.carousel.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/owl.theme.css" />" rel="stylesheet">

    <!-- theme stylesheet -->
    <link href="<c:url value="/resources/css/style.default.css" />" rel="stylesheet" id="theme-stylesheet">
	<!-- your stylesheet with modifications -->
    <link href="<c:url value="/resources/css/guess.css" />" rel="stylesheet">

    <script src="<c:url value="/resources/js/plugins/respond.min.js" />"></script>

    <link rel="shortcut icon" href="<c:url value="/resources/img/favicon.ico" />">



</head>

<body>

<!-- *** UMPISA NG NAVBAR ***  -->
	<div class="navbar navbar-default navbar-fixed-top yamm" role="navigation" id="navbar">
        <div class="row">
		
			<!-- umpisa ng navbar header -->
            <div class="navbar-header">

                <a class="navbar-brand home" href="index" data-animate-hover="none">
                    <img src="<c:url value="/resources/img/logo-JETHER.png" />" alt="GUESS logo" class="col-sm-11 hidden-xs">
                    <img src="<c:url value="/resources/img/logo-JETHER.png" />" alt="GUESS logo" class="visible-xs">
					<span class="sr-only">Go to homepage</span>
                </a>
                <div class="navbar-buttons">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-align-justify"></i>
                    </button>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#search">
                        <span class="sr-only">Toggle search</span>
                        <i class="fa fa-search"></i>
                    </button>
                    <a class="btn btn-default navbar-toggle" href="basket.html">
                        <i class="fa fa-shopping-bag"></i>  <span class="hidden-xs">3 items in cart</span>
                    </a>
					 
					 <a class="btn btn-default navbar-toggle" href="#">
					 <i class="fa fa-user red"></i> 
					</a>
                </div>
            </div>
            <!-- katapusan ng navbar header -->
		
		
<!-- umpisa para sa mga menu items -->			
            <div class="navbar-collapse collapse" id="navigation">

               
				
				<ul class="nav navbar-nav navbar-left">
				
				<!-- umpisa ng individual dropdown menu item -->
				<li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">NEW ARRIVALS <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										<h5>WOMEN</h5>
                                            <ul>
                                                <li><a href="category.html">View All</a></li>
                                                <li><a href="category.html">Dresses</a></li>
                                                <li><a href="category.html">Rompers & Jumpsuits</a></li>
                                                <li><a href="category.html">Denim</a></li>
                                                <li><a href="category.html">Jackets & Outerwear</a></li>
                                                <li><a href="category.html">Tops</a></li>
                                                <li><a href="category.html">Pants</a></li>
                                                <li><a href="category.html">Shorts</a></li>
                                                <li><a href="category.html">Skirts</a></li>
                                                <li><a href="category.html">Sweaters</a></li>
                                                <li><a href="category.html">Lingerie</a></li>
                                                <li><a href="category.html">Swimwear</a></li>
                                                <li><a href="category.html">Activewear</a></li>
                                                <li><a href="category.html">Handbags</a></li>
                                                <li><a href="category.html">Shoes</a></li>
											</ul>
                                        </div>
										
										<div class="col-sm-2">
										<h5>MEN</h5>
                                            <ul>
                                                <li><a href="category.html">View All</a></li>
                                                <li><a href="category.html">Denim</a></li>
                                                <li><a href="category.html">Tanks & T Shirts</a></li>
                                                <li><a href="category.html">Shirts</a></li>
                                                <li><a href="category.html">Shorts</a></li>
                                                <li><a href="category.html">Pants</a></li>
                                                <li><a href="category.html">Jackets & Outerwear</a></li>
                                                <li><a href="category.html">Activewear</a></li>
                                                <li><a href="category.html">Underwear</a></li>
                                                <li><a href="category.html">Swimwear</a></li>
                                                <li><a href="category.html">Shoes</a></li>
											</ul>
                                        </div>
										
										<div class="col-sm-2">
										<h5>KIDS</h5>
                                            <ul>
                                                <li><a href="category.html">View All</a></li>
                                                <li><a href="category.html">Baby</a></li>
                                                <li><a href="category.html">Girl</a></li>
                                                <li><a href="category.html">Boy</a></li>
											</ul>
                                        </div>
										
										<div class="col-sm-2">
										<h5>HANDBAGS</h5>
                                            <ul>
                                                <li><a href="category.html">View All</a></li>
											</ul>
                                        </div>
										
										
                                        <div class="col-sm-2">
										<h5>ACCESSORIES</h5>
                                            <ul>
                                                <li><a href="category.html">View All</a></li>
                                                <li><a href="category.html">Women</a></li>
                                                <li><a href="category.html">Men</a></li>
											</ul>
                                        </div>
										
										
										<div class="col-sm-2">
										<h5>SHOES</h5>
                                             <ul>
                                                <li><a href="category.html">View All</a></li>
                                                <li><a href="category.html">Women</a></li>
                                                <li><a href="category.html">Men</a></li>
											</ul>
                                        </div>
										
										
										 
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                </li>
				<!-- katapusan ng individual dropdown menu item -->
				
				<!-- umpisa ng individual dropdown menu item -->
				<li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">WOMEN <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										
                                            <ul>
                                                <li><a href="#">View All</a></li>
												<li><a href="#">Dresses</a></li>
												<li><a href="#">Denim</a></li>
												<li><a href="#">Tops</a></li>
												<li><a href="#">Rompers & Jumpsuits</a></li>
												<li><a href="#">Bottoms</a></li>
												<li><a href="#">Shorts</a></li>
												<li><a href="#">Skirts</a></li>
												<li><a href="#">Pants</a></li>
												<li><a href="#">Pants</a></li>
												<li><a href="#">Jackets & Outerwear</a></li>
												<li><a href="#">Swimwear</a></li>
												<li><a href="#">Lingerie</a></li>
												<li><a href="#">Activewear</a></li>
												<li><a href="#">Limited Pricing</a></li>
											</ul>
											
                                        </div>
										<div class="col-sm-2">
											<h5>FEATURES</h5>
                                            <ul>
												<li><a href="#">Best Sellers</a></li>
												<li><a href="#">Online Exclusives</a></li>
												<li><a href="#">Online Exclusives</a></li>
												<li><a href="#">The Logo Shop</a></li>
												<li><a href="#">Camilas Picks</a></li>
												<li><a href="#">Fall Essentials</a></li>
												<li><a href="#">Night Out</a></li>
												<li><a href="#">The Wedding Shop</a></li>
												<li><a href="#">Embroidered</a></li>
												<li><a href="#">Marciano</a></li>
												<li><a href="#">Gift Cards</a></li>
											</ul>
                                        </div>
                                        <div class="col-sm-4 text-center">
                                              <div class="banner">
                                                <a href="#"><img src="<c:url value="/resources/img/menu-women.jpg" />" alt="GUESS Women"></a><br><br>
												<a href="#">Shop New</a>
                                            </div>
                                        </div>
										<div class="col-sm-6"></div>
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                    </li>
                <!-- katapusan ng individual dropdown menu item -->
				
				<!-- umpisa ng individual dropdown menu item -->
				<li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">MEN <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										
                                            <ul>
                                                <li><a href="#">View All</a></li>
												<li><a href="#">Denim</a></li>
												<li><a href="#">Tanks & T Shirts</a></li>
												<li><a href="#">Shirts</a></li>
												<li><a href="#">Shorts & Pants</a></li>
												<li><a href="#">Shorts</a></li>
												<li><a href="#">Pants</a></li>
												<li><a href="#">Jackets & Outerwear</a></li>
												<li><a href="#">Activewear</a></li>
												<li><a href="#">Swimwear</a></li>
												<li><a href="#">Underwear</a></li>
												<li><a href="#">Suits</a></li>
												<li><a href="#">Shoes</a></li>
												<li><a href="#">Accessories</a></li>
												<li><a href="#">Limited Pricing</a></li>
											</ul>
											
                                        </div>
										<div class="col-sm-2">
											<h5>FEATURES</h5>
                                            <ul>
												<li><a href="#">Best Sellers</a></li>
												<li><a href="#">Online Exclusives</a></li>
												<li><a href="#">Online Exclusives</a></li>
												<li><a href="#">The Logo Shop</a></li>
												<li><a href="#">Moto Trend</a></li>
												<li><a href="#">Fall Essentials</a></li>
												<li><a href="#">Night Out</a></li>
												<li><a href="#">Casual Shirts</a></li>
												<li><a href="#">Gifts For Him</a></li>
												<li><a href="#">Gift Cards</a></li>
											</ul>
                                        </div>
                                        <div class="col-sm-4 text-center">
                                              <div class="banner">
                                                <a href="#"><img src="<c:url value="/resources/img/menu-men.jpg" />" alt="GUESS Men"></a><br><br>
												<a href="#">Shop New</a>
                                            </div>
                                        </div>
										 <div class="col-sm-6"></div>
										
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                    </li>
				<!-- katapusan ng individual dropdown menu item -->
				
				<!-- umpisa ng individual dropdown menu item -->
                <li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">DENIM <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										<h5>WOMEN</h5>
                                            <ul>
                                                <li><a href="#">View All</a></li>
												<li><a href="#">Skinny</a></li>
												<li><a href="#">High Rise</a></li>
												<li><a href="#">Mid & Low Rise</a></li>
												<li><a href="#">Bootcut & Flare</a></li>
												<li><a href="#">Straight & Relaxed Fit</a></li>
												<li><a href="#">Shorts & Skirts</a></li>
												<li><a href="#">Tops & Jackets</a></li>
											</ul>
											
                                        </div>
										<div class="col-sm-3">
											<h5>MEN</h5>
                                            <ul>
												<li><a href="#">View All</a></li>
												<li><a href="#">Skinny</a></li>
												<li><a href="#">Slim Tapered</a></li>
												<li><a href="#">Slim Straight & Regular Straight</a></li>
												<li><a href="#">Relaxed & Bootcut</a></li>
												<li><a href="#">Shorts</a></li>
												<li><a href="#">Shirts & Jackets</a></li>
												<li><a href="#">Moto & Destroyed</a></li>
											</ul>
											
                                        </div>
										 
                                        <div class="col-sm-4 text-center">
                                              <div class="banner">
                                                <a href="#"><img src="<c:url value="/resources/img/menu-denim.jpg" />" alt="GUESS Denim"></a><br><br>
												<a href="#">Shop Denim</a>
                                            </div>
                                        </div>
										 <div class="col-sm-5"></div>
										
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                </li>
				<!-- katapusan ng individual dropdown menu item -->
					
				<!-- umpisa ng individual dropdown menu item -->	
				<li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">HANDBAGS <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-3">
										
                                            <ul>
                                                <li><a href="#">View All</a></li>
                                                <li><a href="#">Wallets</a></li>
                                                <li><a href="#">Tote Bags</a></li>
                                                <li><a href="#">Satchels</a></li>
                                                <li><a href="#">Crossbody Bags</a></li>
                                                <li><a href="#">Crossbody Shoulder Bags</a></li>
                                                <li><a href="#">Backpacks</a></li>
                                                <li><a href="#">Mini Bags</a></li>
                                                <li><a href="#">Luggage & Travel Bags</a></li>
                                                <li><a href="#">Bag Accessories</a></li>
                                                <li><a href="#">Limited Pricing</a></li>
											</ul>
											
                                        </div>
										<div class="col-sm-3">
											<h5>FEATURES</h5>
                                            <ul>
												<li><a href="#">Best Sellers</a></li>
												<li><a href="#">Trending Now: Details</a></li>
												<li><a href="#">Night Out</a></li>
												<li><a href="#">Matching Sets</a></li>
												<li><a href="#">Logo</a></li>
												<li><a href="#">Online Exclusives</a></li>
												<li><a href="#">Gift Cards</a></li>
												<li><a href="#">Summer Whites</a></li>
											</ul>
										</div>
                                        <div class="col-sm-3 text-center">
                                            <div class="banner">
                                                <a href="#"><img src="<c:url value="/resources/img/menu-handbags.jpg" />" class="img img-responsive" alt="GUESS Handbags"></a><br>
												<a href="#">Shop Handbags</a>
                                            </div>
                                        </div>
										
										<div class="col-sm-3 text-center"></div>
										
										
										
                                    </div>
                                </div>
                               
                            </li>
                        </ul>
				<!-- katapusan ng individual dropdown menu item -->		
                 
				<!-- umpisa ng individual dropdown menu item -->	
				<li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">ACCESSORIES <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										<h5>WOMEN</h5>
                                            <ul>
                                                <li><a href="#">View All</a></li>
												<li><a href="#">Watches</a></li>
												<li><a href="#">Jewelry</a></li>
												<li><a href="#">Belts</a></li>
												<li><a href="#">Hats & Scarves</a></li>
												<li><a href="#">Sunglasses</a></li>
												<li><a href="#">Fragrance</a></li>
												<li><a href="#">Keychains</a></li>
												<li><a href="#">Phone Cases & Tech Accessories</a></li>
												<li><a href="#">Limited Pricing</a></li>
											</ul>
											
                                        </div>
										<div class="col-sm-2">
											<h5>MEN</h5>
                                            <ul>
												<li><a href="#">View All</a></li>
												<li><a href="#">Watches</a></li>
												<li><a href="#">Jewelry</a></li>
												<li><a href="#">Wallets & Bags</a></li>
												<li><a href="#">Belts</a></li>
												<li><a href="#">Hats</a></li>
												<li><a href="#">Sunglasses</a></li>
												<li><a href="#">Fragrance</a></li>
												<li><a href="#">Limited Pricing</a></li>
											</ul>
											
                                        </div>
										
                                        <div class="col-sm-4 text-center">
                                              <div class="banner">
                                                <a href="#"><img src="<c:url value="/resources/img/menu-accessories.jpg" />" alt="GUESS Accessories"></a><br><br>
												<a href="#">Shop Accessories</a>
                                            </div>
                                        </div>
										 <div class="col-sm-"></div>
										
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                    
					</li>
				<!-- katapusan ng individual dropdown menu item -->
				
				<!-- umpisa ng individual dropdown menu item -->	
				<li class="active dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">SALE <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										<h5>BABY</h5>
                                            <ul>
                                                <li><a href="#">View All</a></li>
                                                <li><a href="#">Baby Girl (0-24m)</a></li>
                                                <li><a href="#">Baby Boy (0-24M)</a></li>
											</ul>
                                        </div>
										<div class="col-sm-2">
										<h5>GIRL</h5>
                                            <ul>
                                                <li><a href="#">View All</a></li>
                                                <li><a href="#">Little Girl (2 -7)</a></li>
                                                <li><a href="#">Big Girl (7-16)</a></li>
											</ul>
                                        </div>
										<div class="col-sm-2">
                                        <h5>BOY</h5>
                                            <ul>
                                                <li><a href="#">View All</a></li>
                                                <li><a href="#">Little Boy (2 -7)</a></li>
                                                <li><a href="#">Big Boy (7-18)</a></li>
											</ul>
                                        </div>
										<div class="col-sm-2">
                                        <h5>FEATURES</h5>
                                            <ul>
                                                <li><a href="#">Best Sellers</a></li>
                                                <li><a href="#">Denim Shop</a></li>
                                                <li><a href="#">Back To School</a></li>
                                                <li><a href="#">Floral Shop</a></li>
                                                <li><a href="#">Gifts</a></li>
                                                <li><a href="#">Gift Cards</a></li>
											</ul>
                                        </div>
										<div class="col-sm-4 text-center">
                                              <div class="banner">
                                                <a href="#"><img src="<c:url value="/resources/img/menu-sale.jpg" />" alt="GUESS SALE"></a><br><br>
												<a href="#">Sale</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                    </li>
				<!-- katapusan ng individual dropdown menu item -->	
					
                </ul>

            </div>
<!-- katapusan para sa mga menu items -->			


            
			<div class="navbar-buttons">


                <div class="navbar-collapse collapse right"><!--id="search-not-mobile" -->
                  
					<a href="#" class="btn"   data-toggle="collapse" data-target="#search">
						 <span class="sr-only">Toggle search</span>
                        <i class="fa fa-search"></i>
					</a>
                       <a href="stores" class="btn"><i class="fa fa-map-marker"></i></a>
					   <a href="login.html" class="btn"><i class="fa fa-user"></i></a>
					   <a href="basket.html" class="btn"><i class="fa fa-shopping-bag"></i></a>
					   
				 
					
					
				</div>
				 
            </div>

           
			<!-- umpisa ng search toggle -->
		   <div class="collapse clearfix" id="search">

                <form class="navbar-form" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search">
                        <span class="input-group-btn">

			    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
				</span>
                    </div>
                </form>

            </div>
			<!-- katapusan ng search toggle --> 

			
        </div>
</div>  	
<!-- *** KATAPUSAN NG NAVBAR ***  -->








 
 <!-- *** UMPISA PARA SA OVERALL WEBSITE CONTENTS -->  
<section id="pinakalaman">
 <div id="content">
 
 
 <!-- umpisa ng maliit na promotional item 120px height. pwedeng hard coded or isang image lang -->
	<section id="ad-30">
	<div class="container">
	 
	<div class="col-sm-3 black text-center">ONLINE ONLY</div>
	<div class="col-sm-6 text-center">
	<span class="black-bold">50% OFF</span> <span class="black">SALE STYLES</span> + 
	<span class="red-bold">EXTRA 10%</span> <span class="red">AT CHECKOUT</span>
	</div>
	
	<div class="col-sm-3 text-center">
	<a href="#" class="black link">WOMEN</a><i class="fa fa-play"></i>
	<a href="#" class="black link">MEN</a> 
	<a href="#" class="black small">DETAILS</a>
	</div>
	
 
	</div>
	</section>
 <!-- katapusan ng maliit na promotional item 120px height. pwedeng hard coded or isang image lang -->

 

<!-- umpisa para sa main slider -->
    <div class="container">
                <div class="col-md-12">
					
					
                    <div id="main-slider">
                        <div class="item">
                            <img src="<c:url value="/resources/img/main-slider1.jpg" />" alt="" class="img-responsive">
                        </div>
                        <div class="item">
                            <img class="img-responsive" src="<c:url value="/resources/img/main-slider2.jpg" />" alt="">
                        </div>
                        <div class="item">
                            <img class="img-responsive" src="<c:url value="/resources/img/main-slider3.jpg" />" alt="">
                        </div>
                        <div class="item">
                            <img class="img-responsive" src="<c:url value="/resources/img/main-slider4.jpg" />" alt="">
                        </div>
                    </div>
                   
					
                </div>
            </div>
<!-- katapusan para sa main-slider -->		
			

           

<!-- *** UMPISA NG JUST RELEASED SLIDER *** -->
		<section id="guess-released">

			<div class="box">
                    <div class="container">
                        <div class="col-md-12">
                            <h2>JUST RELEASED</h2>
                        </div>
                    </div>
            </div>

                <div class="container-fluid"> 
                    <div class="product-slider">
					
					
					
                        <div class="item">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-jr-tees.jpg" />" alt="New Tees" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-jr-tees.jpg" />" alt="New Tees" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img data-src="<c:url value="/resources/img/front-jr-tees.jpg" />" alt="New Tees" class="img-responsive">
                                </a>
								 
							 
                                <!-- /.text -->
							</div>

                            <a href="product.html"><div class="text-center ghost">TEES <i class="fa fa-caret-right"></i></div></a>
								
                        </div>
						
						
						
						

                         <div class="item">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-jr-rainy.jpg" />" alt="Rainy Season Essentials" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-jr-rainy.jpg" />" alt="Rainy Season" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img data-src="<c:url value="/resources/img/front-jr-rainy.jpg" />" alt="Rainy Season" class="img-responsive">
                                </a>
								 
							 
                                <!-- /.text -->
							</div>

                            <a href="product.html"><div class="text-center ghost">RAINY SEASON <i class="fa fa-caret-right"></i></div></a>
								
                        </div>
						
						

                        
						 <div class="item">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-jr-active.jpg" />" alt="GUESS Active Inspired" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-jr-active.jpg" />" alt="GUESS Active Inspired" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img data-src="<c:url value="/resources/img/front-jr-active.jpg" />" alt="GUESS Active Inspired" class="img-responsive">
                                </a>
								 
							 
                                <!-- /.text -->
							</div>

                            <a href="product.html"><div class="text-center ghost">ACTIVE INSPIRED <i class="fa fa-caret-right"></i></div></a>
								
                        </div>
						
						

 						

                        
						 <div class="item">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-jr-watches.jpg" />" alt="GUESS Watches" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-jr-watches.jpg" />" alt="GUESS Watches" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img data-src="<c:url value="/resources/img/front-jr-watches.jpg" />" alt="GUESS Watches" class="img-responsive">
                                </a>
								 
							 
                                <!-- /.text -->
							</div>

                            <a href="product.html"><div class="text-center ghost">WATCHES <i class="fa fa-caret-right"></i></div></a>
								
                        </div>
						
						
						
						 <div class="item">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-jr-shoes.jpg" />" alt="GUESS Shoes" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-jr-shoes.jpg" />" alt="GUESS Shoes" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img data-src="<c:url value="/resources/img/front-jr-shoes.jpg" />" alt="GUESS Shoes" class="img-responsive">
                                </a>
								 
							 
                                <!-- /.text -->
							</div>

                            <a href="product.html"><div class="text-center ghost">ALL SHOES <i class="fa fa-caret-right"></i></div></a>
								
                        </div>
						
						
						
						 
						
						
						
						
						
						
						
						

                    </div>
                </div>

            
</section>            
<!-- *** END OF JUST RELEASED SLIDER *** -->
			


<!-- start Camila -->
<section id="guest-promo1">
<div class="container-fluid">

<div class="col-md-6">
<h1 class="text-center">CAMILA CABELLO</h1>
<h3 class="text-center">FOR GUESS SUMMER & RAINY SEASON 2017</h3>

<!-- umpisa ng 2 buttons -->
<div class="col-md-6">
<a href="product.html"><p><div class="text-center ghost">SHOP CAMILA'S PICKS <i class="fa fa-caret-right"></i></div></p></a>
</div>
<div class="col-md-6">
<a href="#" data-target="#CamilaVideo" data-toggle="modal"><p><div class="text-center ghost">WATCH CAMPAIGN VIDEO <i class="fa fa-caret-right"></i></div></p></a>


 <br>

 
<!-- Modal -->
<div class="modal fade" id="CamilaVideo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
             
		<div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         <br>
        </div>
          
			<div class="modal-body">
			<div class="embed-container">
			 
			<iframe class="img-responsive" style="width: 700px; height:300px;" src="https://www.youtube.com/embed/e8kA-uFTr58" frameborder="0" allowfullscreen></iframe>
			</div>
			</div>
            
        </div> 
		<!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal -->


</div>
<!-- katapusan ng dalawang buttons -->

</div>
<div class="col-md-6">
<img src="<c:url value="/resources/img/camila-cabello.png" />" class="img-responsive">
</div>



	</div>
</section>			
<!-- end Camila -->
			
			
			
 		
			
			
			
<!-- *** start GUESS LIST ***  -->
<section id="guesslist" class="hidden">
<div class="container text-center">
<p>
<h3>NEW & EXISTING GUESS LIST MEMBERS</h3>
<h5>
Sign up for your chance to win : A limited edition, autograph Camila Cabello Denim Jacket,<br> Php 50,000 prize card and styling session at your GUESS store
</h5>
 
 <a href="#"><div class="btn ghost text-center">LEARN MORE <i class="fa fa-caret-right"></i></div></a>
</p>

 </div>

</section>
<!-- *** end GUESS LIST ***  -->
 
 	 
			
			
<!-- START BEST SELLER -->
<section id="guess-released">

                <div class="box">
                    <div class="container">
                        <div class="col-md-12">
                            <h2>BEST SELLER</h2>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="product-slider">
                        
						 <div class="item text-center">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-best-seller1.jpg" />" alt="GUESS Shoes" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-best-seller1.jpg" />" alt="GUESS Shoes" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img data-src="<c:url value="/resources/img/front-best-seller1.jpg" />" alt="GUESS Shoes" class="img-responsive">
                                </a>
								 
							 
                                <!-- /.text -->
							</div>

                            <a href="product.html">OVERSIZE DENIM JACKET </a>
								
                        </div>
						
						
						

                        
						 <div class="item text-center">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-best-seller2.jpg" />" alt="GUESS Men Best Seller" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-best-seller2.jpg" />" alt="GUESS Men Best Seller" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img data-src="<c:url value="/resources/img/front-best-seller2.jpg" />" alt="GUESS Men Best Seller" class="img-responsive">
                                </a>
								 
							 
                                <!-- /.text -->
							</div>

                            <a href="product.html">STREAM COLOR-BLOCK TEE</a>
								
                        </div>
						
						
						

                        
						 <div class="item text-center">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-best-seller3.jpg" />" alt="GUESS Men Best Seller" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-best-seller3.jpg" />" alt="GUESS Men Best Seller" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img data-src="<c:url value="/resources/img/front-best-seller3.jpg" />" alt="GUESS Men Best Seller" class="img-responsive">
                                </a>
								 
							 
                                <!-- /.text -->
							</div>

                            <a href="product.html">COLOR MOTO CROW TEE</a>
								
                        </div>
						
						
						 
						 <div class="item text-center">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-best-seller4.jpg" />" alt="GUESS Men Best Seller" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-best-seller4.jpg" />" alt="GUESS Men Best Seller" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img data-src="<c:url value="/resources/img/front-best-seller4.jpg" />" alt="GUESS Men Best Seller" class="img-responsive">
                                </a>
								 
							 
                                <!-- /.text -->
							</div>

                            <a href="product.html">PIN UP GIRL SHIRT</a>
								
                        </div>
						
						
						
						 
						 <div class="item text-center">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-best-seller5.jpg" />" alt="GUESS Men Best Seller" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-best-seller5.jpg" />" alt="GUESS Men Best Seller" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img data-src="<c:url value="/resources/img/front-best-seller5.jpg" />" alt="GUESS Men Best Seller" class="img-responsive">
                                </a>
								 
							 
                                <!-- /.text -->
							</div>

                            <a href="product.html">LAYERING TANK</a>
								
                        </div>



						
						
						
						
						
						
						 <div class="item text-center">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-best-seller1.jpg" />" alt="GUESS Shoes" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-best-seller1.jpg" />" alt="GUESS Shoes" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img data-src="<c:url value="/resources/img/front-best-seller1.jpg" />" alt="GUESS Shoes" class="img-responsive">
                                </a>
								 
							 
                                <!-- /.text -->
							</div>

                            <a href="product.html">OVERSIZE DENIM JACKET </a>
								
                        </div>
						
						
						

                        
						 <div class="item text-center">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-best-seller2.jpg" />" alt="GUESS Men Best Seller" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-best-seller2.jpg" />" alt="GUESS Men Best Seller" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img data-src="<c:url value="/resources/img/front-best-seller2.jpg" />" alt="GUESS Men Best Seller" class="img-responsive">
                                </a>
								 
							 
                                <!-- /.text -->
							</div>

                            <a href="product.html">STREAM COLOR-BLOCK TEE</a>
								
                        </div>
						
						
						

                        
						 <div class="item text-center">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-best-seller3.jpg" />" alt="GUESS Men Best Seller" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-best-seller3.jpg" />" alt="GUESS Men Best Seller" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img data-src="<c:url value="/resources/img/front-best-seller3.jpg" />" alt="GUESS Men Best Seller" class="img-responsive">
                                </a>
								 
							 
                                <!-- /.text -->
							</div>

                            <a href="product.html">COLOR MOTO CROW TEE</a>
								
                        </div>
						
						
						 
						 <div class="item text-center">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-best-seller4.jpg" />" alt="GUESS Men Best Seller" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-best-seller4.jpg" />" alt="GUESS Men Best Seller" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img data-src="<c:url value="/resources/img/front-best-seller4.jpg" />" alt="GUESS Men Best Seller" class="img-responsive">
                                </a>
								 
							 
                                <!-- /.text -->
							</div>

                            <a href="product.html">PIN UP GIRL SHIRT</a>
								
                        </div>
						
						
						
						 
						 <div class="item text-center">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-best-seller5.jpg" />" alt="GUESS Men Best Seller" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img data-src="<c:url value="/resources/img/front-best-seller5.jpg" />" alt="GUESS Men Best Seller" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img data-src="<c:url value="/resources/img/front-best-seller5.jpg" />" alt="GUESS Men Best Seller" class="img-responsive">
                                </a>
								 
							 
                                <!-- /.text -->
							</div>

                            <a href="product.html">LAYERING TANK</a>
								
                        </div>



						
						
						
						
						
						
						
						

						
						
						
                    </div>
                    <!-- /.product-slider -->
                </div>
                <!-- /.container -->

            </section>
<!-- END BEST SELLER -->
 
 
	

	
	
	
	
	</div>
</section>
<!-- *** KATAPUSAN PARA SA OVERALL WEBSITE CONTENTS -->  



<!-- *** START FOOTER *** -->
<section id="footer" data-animate="fadeInUp">
	 
			<div class="container">
			<div class="col-sm-3"></div>
			
			<div class="col-sm-6 text-center">
			<p>Enjoy 15% OFF & Free Shipping on your first order</p>
			
						<form>
                            <div class="input-group">
							<input type="text" class="form-control">
								<span class="input-group-btn"><button class="btn btn-danger" type="button">SIGNUP NOW!</button></span>
							</div>
                            <!-- /input-group -->
						<p>
						<input type="checkbox"> By signing up I agree to receive emails about the latest trends and exclusive content.
						</p>
                        </form>
			</div>
			
			<div class="col-sm-3"></div>
			
			</div>
			
			<br><br>
		
			
            <div class="container">
			
			 <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <h4>MY ACCOUNT</h4>

                        <ul>
                            <li><a href="register.html">Register</a></li>
                            <li><a href="login.html">Login</a></li>
                            <li><a href="orders.html">Orders</a></li>
                            <li><a href="returns.html">Returns</a></li>
                         </ul>
 

                       

                        <hr class="hidden-md hidden-lg hidden-sm">

                    </div>
                    <!-- /.col-md-3 -->

                    <div class="col-md-3 col-sm-6">

                        <h4>ASSISTANCE</h4>
						<ul>
                            <li><a href="support.html">Customer Care</a></li>
                            <li><a href="stores">Store Locator</a></li>
                            <li><a href="contact.html">Contact Us</a></li>
                            <li><a href="gift.html">Gift Cards</a></li>
                        </ul>
 

                        <hr class="hidden-md hidden-lg">

                    </div>
                    <!-- /.col-md-3 -->

                    <div class="col-md-3 col-sm-6">

                        <h4>COMPANY</h4>
						<ul>
                            <li><a href="stores">Our Store</a></li>
                            <li><a href="careers.html">Corporate Careers</a></li>
                            <li><a href="careers-retail.html">Retail Careers</a></li>
                        </ul>
						
                        <p class="hidden">
						<strong>GUESS Philippines</strong>
                            <br>DII Bldg. 
                            <br>150 San Vicente Rd
                            <br>Brgy. San Vicente
                            <br>Laguna
                            <br>
                            <strong>Philippines</strong>
                        </p>
 
                        <hr class="hidden-md hidden-lg">

                    </div>
                    <!-- /.col-md-3 -->



                    <div class="col-md-3 col-sm-6">

                        <h4>FOLLOW US</h4>
 
                        <p>
                            <a href="https://www.facebook.com/GUESSPhil" class="facebook external" target="_blank" data-animate-hover="none"><i class="fa fa-facebook-square fa-2x"></i></a>
                            <a href="https://twitter.com/GUESS" class="twitter external" target="_blank" data-animate-hover="none"><i class="fa fa-twitter fa-2x"></i></a>
                            <a href="https://www.instagram.com/guess" class="instagram external" target="_blank" data-animate-hover="none"><i class="fa fa-instagram fa-2x"></i></a>
                            <a href="https://www.pinterest.com/guess" class="gplus external" target="_blank" data-animate-hover="none"><i class="fa fa-pinterest-p fa-2x"></i></a>
                            <a href="https://www.youtube.com/user/GUESSincTV" class="email external" target="_blank" data-animate-hover="none"><i class="fa fa-youtube-play fa-2x"></i></a>
                        </p> 

                    </div>
                    <!-- /.col-md-3 -->

					
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
			<br><br>
			<p class="small text-center">
			&copy; <script type="text/javascript">document.write(new Date().getFullYear());</script> GUESS Philippines. All rights reserved.
			<a href="terms.html">Terms & Conditions</a> <a href="privacy.html">Privacy Policy</a>
			</p>
			
			
			
			
			
        </section>
<!-- *** END FOOTER *** -->




<!-- *** START COPYRIGHT *** -->
<section id="copyright" class="hidden">
<div class="container ">
		 
			
		 
                <div class="col-md-6">
                    <p class="pull-left">© <script type="text/javascript">document.write(new Date().getFullYear());</script> GUESS Philippines. All rights reserved.</p>

                </div>
                <div class="col-md-6">
                    <p class="pull-right">
					<a href="terms.html" class="active">Terms & Conditions</a> <a href="privacy.html">Privacy Policy</a>
                        
                    </p>
                </div>
			
			
</div>
</section>
<!-- *** COPYRIGHT END *** -->

	
	
	
 
    

    <!-- *** SCRIPTS TO INCLUDE ***
 _________________________________________________________ -->
    <script src="<c:url value="/resources/js/plugins/jquery-1.11.0.min.js" />"></script>
    <script src="<c:url value="/resources/js/plugins/bootstrap.min.js" />"></script>
    <script src="<c:url value="/resources/js/plugins/jquery.cookie.js" />"></script>
    <script src="<c:url value="/resources/js/plugins/waypoints.min.js" />"></script>
    <script src="<c:url value="/resources/js/plugins/modernizr.js" />"></script>
    <script src="<c:url value="/resources/js/plugins/bootstrap-hover-dropdown.js" />"></script>
    <script src="<c:url value="/resources/js/plugins/owl.carousel.min.js" />"></script>
    <script src="<c:url value="/resources/js/front.js" />"></script>
	
<script src="<c:url value="/resources/js/plugins/jquery.lazy.js" />"></script>
	<script> 
	$(function() {
        $('.img-responsive').Lazy();
    });
	</script>
	
	
	

</body>

</html>