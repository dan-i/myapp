<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="For GUESS Philippines">
    <meta name="author" content="JETHER.NET">
    <meta name="keywords" content="">
	<title>
        GUESS Philippines
    </title>

    <meta name="keywords" content="">

   <!-- <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">-->

    <!-- styles -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">

    <!-- theme stylesheet -->
    <link href="css/style.default.css" rel="stylesheet" id="theme-stylesheet">
	<!-- your stylesheet with modifications -->
    <link href="css/guess.css" rel="stylesheet">
    <link href="css/guess-product.css" rel="stylesheet">

    <script src="js/respond.min.js"></script>

    <link rel="shortcut icon" href="img/favicon.ico">



</head>

<body>

   
<!-- *** UMPISA NG NAVBAR ***  -->
	<div class="navbar navbar-default navbar-fixed-top yamm" role="navigation" id="navbar">
        <div class="row">
		
			<!-- umpisa ng navbar header -->
            <div class="navbar-header">

                <a class="navbar-brand home" href="index.html" data-animate-hover="none">
                    <img src="img/logo-JETHER.png" alt="GUESS logo" class="col-sm-11 hidden-xs">
                    <img src="img/logo-JETHER.png" alt="GUESS logo" class="visible-xs">
					<span class="sr-only">Go to homepage</span>
                </a>
                <div class="navbar-buttons">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-align-justify"></i>
                    </button>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#search">
                        <span class="sr-only">Toggle search</span>
                        <i class="fa fa-search"></i>
                    </button>
                    <a class="btn btn-default navbar-toggle" href="basket.html">
                        <i class="fa fa-shopping-bag"></i>  <span class="hidden-xs">3 items in cart</span>
                    </a>
					 
					 <a class="btn btn-default navbar-toggle" href="#">
					 <i class="fa fa-user red"></i> 
					</a>
                </div>
            </div>
            <!-- katapusan ng navbar header -->
		
		
<!-- umpisa para sa mga menu items -->			
            <div class="navbar-collapse collapse" id="navigation">

               
				
				<ul class="nav navbar-nav navbar-left">
				
				<!-- umpisa ng individual dropdown menu item -->
				<li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">NEW ARRIVALS <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										<h5>WOMEN</h5>
                                            <ul>
                                                <li><a href="category.html">View All</a></li>
                                                <li><a href="category.html">Dresses</a></li>
                                                <li><a href="category.html">Rompers & Jumpsuits</a></li>
                                                <li><a href="category.html">Denim</a></li>
                                                <li><a href="category.html">Jackets & Outerwear</a></li>
                                                <li><a href="category.html">Tops</a></li>
                                                <li><a href="category.html">Pants</a></li>
                                                <li><a href="category.html">Shorts</a></li>
                                                <li><a href="category.html">Skirts</a></li>
                                                <li><a href="category.html">Sweaters</a></li>
                                                <li><a href="category.html">Lingerie</a></li>
                                                <li><a href="category.html">Swimwear</a></li>
                                                <li><a href="category.html">Activewear</a></li>
                                                <li><a href="category.html">Handbags</a></li>
                                                <li><a href="category.html">Shoes</a></li>
											</ul>
                                        </div>
										
										<div class="col-sm-2">
										<h5>MEN</h5>
                                            <ul>
                                                <li><a href="category.html">View All</a></li>
                                                <li><a href="category.html">Denim</a></li>
                                                <li><a href="category.html">Tanks & T Shirts</a></li>
                                                <li><a href="category.html">Shirts</a></li>
                                                <li><a href="category.html">Shorts</a></li>
                                                <li><a href="category.html">Pants</a></li>
                                                <li><a href="category.html">Jackets & Outerwear</a></li>
                                                <li><a href="category.html">Activewear</a></li>
                                                <li><a href="category.html">Underwear</a></li>
                                                <li><a href="category.html">Swimwear</a></li>
                                                <li><a href="category.html">Shoes</a></li>
											</ul>
                                        </div>
										
										<div class="col-sm-2">
										<h5>KIDS</h5>
                                            <ul>
                                                <li><a href="category.html">View All</a></li>
                                                <li><a href="category.html">Baby</a></li>
                                                <li><a href="category.html">Girl</a></li>
                                                <li><a href="category.html">Boy</a></li>
											</ul>
                                        </div>
										
										<div class="col-sm-2">
										<h5>HANDBAGS</h5>
                                            <ul>
                                                <li><a href="category.html">View All</a></li>
											</ul>
                                        </div>
										
										
                                        <div class="col-sm-2">
										<h5>ACCESSORIES</h5>
                                            <ul>
                                                <li><a href="category.html">View All</a></li>
                                                <li><a href="category.html">Women</a></li>
                                                <li><a href="category.html">Men</a></li>
											</ul>
                                        </div>
										
										
										<div class="col-sm-2">
										<h5>SHOES</h5>
                                             <ul>
                                                <li><a href="category.html">View All</a></li>
                                                <li><a href="category.html">Women</a></li>
                                                <li><a href="category.html">Men</a></li>
											</ul>
                                        </div>
										
										
										 
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                </li>
				<!-- katapusan ng individual dropdown menu item -->
				
				<!-- umpisa ng individual dropdown menu item -->
				<li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">WOMEN <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										
                                            <ul>
                                                <li><a href="#">View All</a></li>
												<li><a href="#">Dresses</a></li>
												<li><a href="#">Denim</a></li>
												<li><a href="#">Tops</a></li>
												<li><a href="#">Rompers & Jumpsuits</a></li>
												<li><a href="#">Bottoms</a></li>
												<li><a href="#">Shorts</a></li>
												<li><a href="#">Skirts</a></li>
												<li><a href="#">Pants</a></li>
												<li><a href="#">Pants</a></li>
												<li><a href="#">Jackets & Outerwear</a></li>
												<li><a href="#">Swimwear</a></li>
												<li><a href="#">Lingerie</a></li>
												<li><a href="#">Activewear</a></li>
												<li><a href="#">Limited Pricing</a></li>
											</ul>
											
                                        </div>
										<div class="col-sm-2">
											<h5>FEATURES</h5>
                                            <ul>
												<li><a href="#">Best Sellers</a></li>
												<li><a href="#">Online Exclusives</a></li>
												<li><a href="#">Online Exclusives</a></li>
												<li><a href="#">The Logo Shop</a></li>
												<li><a href="#">Camilas Picks</a></li>
												<li><a href="#">Fall Essentials</a></li>
												<li><a href="#">Night Out</a></li>
												<li><a href="#">The Wedding Shop</a></li>
												<li><a href="#">Embroidered</a></li>
												<li><a href="#">Marciano</a></li>
												<li><a href="#">Gift Cards</a></li>
											</ul>
                                        </div>
                                        <div class="col-sm-4 text-center">
                                              <div class="banner">
                                                <a href="#"><img src="img/menu-women.jpg" alt="GUESS Women"></a><br><br>
												<a href="#">Shop New</a>
                                            </div>
                                        </div>
										<div class="col-sm-6"></div>
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                    </li>
                <!-- katapusan ng individual dropdown menu item -->
				
				<!-- umpisa ng individual dropdown menu item -->
				<li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">MEN <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										
                                            <ul>
                                                <li><a href="#">View All</a></li>
												<li><a href="#">Denim</a></li>
												<li><a href="#">Tanks & T Shirts</a></li>
												<li><a href="#">Shirts</a></li>
												<li><a href="#">Shorts & Pants</a></li>
												<li><a href="#">Shorts</a></li>
												<li><a href="#">Pants</a></li>
												<li><a href="#">Jackets & Outerwear</a></li>
												<li><a href="#">Activewear</a></li>
												<li><a href="#">Swimwear</a></li>
												<li><a href="#">Underwear</a></li>
												<li><a href="#">Suits</a></li>
												<li><a href="#">Shoes</a></li>
												<li><a href="#">Accessories</a></li>
												<li><a href="#">Limited Pricing</a></li>
											</ul>
											
                                        </div>
										<div class="col-sm-2">
											<h5>FEATURES</h5>
                                            <ul>
												<li><a href="#">Best Sellers</a></li>
												<li><a href="#">Online Exclusives</a></li>
												<li><a href="#">Online Exclusives</a></li>
												<li><a href="#">The Logo Shop</a></li>
												<li><a href="#">Moto Trend</a></li>
												<li><a href="#">Fall Essentials</a></li>
												<li><a href="#">Night Out</a></li>
												<li><a href="#">Casual Shirts</a></li>
												<li><a href="#">Gifts For Him</a></li>
												<li><a href="#">Gift Cards</a></li>
											</ul>
                                        </div>
                                        <div class="col-sm-4 text-center">
                                              <div class="banner">
                                                <a href="#"><img src="img/menu-men.jpg" alt="GUESS Men"></a><br><br>
												<a href="#">Shop New</a>
                                            </div>
                                        </div>
										 <div class="col-sm-6"></div>
										
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                    </li>
				<!-- katapusan ng individual dropdown menu item -->
				
				<!-- umpisa ng individual dropdown menu item -->
                <li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">DENIM <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										<h5>WOMEN</h5>
                                            <ul>
                                                <li><a href="#">View All</a></li>
												<li><a href="#">Skinny</a></li>
												<li><a href="#">High Rise</a></li>
												<li><a href="#">Mid & Low Rise</a></li>
												<li><a href="#">Bootcut & Flare</a></li>
												<li><a href="#">Straight & Relaxed Fit</a></li>
												<li><a href="#">Shorts & Skirts</a></li>
												<li><a href="#">Tops & Jackets</a></li>
											</ul>
											
                                        </div>
										<div class="col-sm-3">
											<h5>MEN</h5>
                                            <ul>
												<li><a href="#">View All</a></li>
												<li><a href="#">Skinny</a></li>
												<li><a href="#">Slim Tapered</a></li>
												<li><a href="#">Slim Straight & Regular Straight</a></li>
												<li><a href="#">Relaxed & Bootcut</a></li>
												<li><a href="#">Shorts</a></li>
												<li><a href="#">Shirts & Jackets</a></li>
												<li><a href="#">Moto & Destroyed</a></li>
											</ul>
											
                                        </div>
										 
                                        <div class="col-sm-4 text-center">
                                              <div class="banner">
                                                <a href="#"><img src="img/menu-denim.jpg" alt="GUESS Denim"></a><br><br>
												<a href="#">Shop Denim</a>
                                            </div>
                                        </div>
										 <div class="col-sm-5"></div>
										
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                </li>
				<!-- katapusan ng individual dropdown menu item -->
					
				<!-- umpisa ng individual dropdown menu item -->	
				<li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">HANDBAGS <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-3">
										
                                            <ul>
                                                <li><a href="#">View All</a></li>
                                                <li><a href="#">Wallets</a></li>
                                                <li><a href="#">Tote Bags</a></li>
                                                <li><a href="#">Satchels</a></li>
                                                <li><a href="#">Crossbody Bags</a></li>
                                                <li><a href="#">Crossbody Shoulder Bags</a></li>
                                                <li><a href="#">Backpacks</a></li>
                                                <li><a href="#">Mini Bags</a></li>
                                                <li><a href="#">Luggage & Travel Bags</a></li>
                                                <li><a href="#">Bag Accessories</a></li>
                                                <li><a href="#">Limited Pricing</a></li>
											</ul>
											
                                        </div>
										<div class="col-sm-3">
											<h5>FEATURES</h5>
                                            <ul>
												<li><a href="#">Best Sellers</a></li>
												<li><a href="#">Trending Now: Details</a></li>
												<li><a href="#">Night Out</a></li>
												<li><a href="#">Matching Sets</a></li>
												<li><a href="#">Logo</a></li>
												<li><a href="#">Online Exclusives</a></li>
												<li><a href="#">Gift Cards</a></li>
												<li><a href="#">Summer Whites</a></li>
											</ul>
										</div>
                                        <div class="col-sm-3 text-center">
                                            <div class="banner">
                                                <a href="#"><img src="img/menu-handbags.jpg" class="img img-responsive" alt="GUESS Handbags"></a><br>
												<a href="#">Shop Handbags</a>
                                            </div>
                                        </div>
										
										<div class="col-sm-3 text-center"></div>
										
										
										
                                    </div>
                                </div>
                               
                            </li>
                        </ul>
				<!-- katapusan ng individual dropdown menu item -->		
                 
				<!-- umpisa ng individual dropdown menu item -->	
				<li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">ACCESSORIES <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										<h5>WOMEN</h5>
                                            <ul>
                                                <li><a href="#">View All</a></li>
												<li><a href="#">Watches</a></li>
												<li><a href="#">Jewelry</a></li>
												<li><a href="#">Belts</a></li>
												<li><a href="#">Hats & Scarves</a></li>
												<li><a href="#">Sunglasses</a></li>
												<li><a href="#">Fragrance</a></li>
												<li><a href="#">Keychains</a></li>
												<li><a href="#">Phone Cases & Tech Accessories</a></li>
												<li><a href="#">Limited Pricing</a></li>
											</ul>
											
                                        </div>
										<div class="col-sm-2">
											<h5>MEN</h5>
                                            <ul>
												<li><a href="#">View All</a></li>
												<li><a href="#">Watches</a></li>
												<li><a href="#">Jewelry</a></li>
												<li><a href="#">Wallets & Bags</a></li>
												<li><a href="#">Belts</a></li>
												<li><a href="#">Hats</a></li>
												<li><a href="#">Sunglasses</a></li>
												<li><a href="#">Fragrance</a></li>
												<li><a href="#">Limited Pricing</a></li>
											</ul>
											
                                        </div>
										
                                        <div class="col-sm-4 text-center">
                                              <div class="banner">
                                                <a href="#"><img src="img/menu-accessories.jpg" alt="GUESS Accessories"></a><br><br>
												<a href="#">Shop Accessories</a>
                                            </div>
                                        </div>
										 <div class="col-sm-"></div>
										
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                    
					</li>
				<!-- katapusan ng individual dropdown menu item -->
				
				<!-- umpisa ng individual dropdown menu item -->	
				<li class="active dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">SALE <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										<h5>BABY</h5>
                                            <ul>
                                                <li><a href="#">View All</a></li>
                                                <li><a href="#">Baby Girl (0-24m)</a></li>
                                                <li><a href="#">Baby Boy (0-24M)</a></li>
											</ul>
                                        </div>
										<div class="col-sm-2">
										<h5>GIRL</h5>
                                            <ul>
                                                <li><a href="#">View All</a></li>
                                                <li><a href="#">Little Girl (2 -7)</a></li>
                                                <li><a href="#">Big Girl (7-16)</a></li>
											</ul>
                                        </div>
										<div class="col-sm-2">
                                        <h5>BOY</h5>
                                            <ul>
                                                <li><a href="#">View All</a></li>
                                                <li><a href="#">Little Boy (2 -7)</a></li>
                                                <li><a href="#">Big Boy (7-18)</a></li>
											</ul>
                                        </div>
										<div class="col-sm-2">
                                        <h5>FEATURES</h5>
                                            <ul>
                                                <li><a href="#">Best Sellers</a></li>
                                                <li><a href="#">Denim Shop</a></li>
                                                <li><a href="#">Back To School</a></li>
                                                <li><a href="#">Floral Shop</a></li>
                                                <li><a href="#">Gifts</a></li>
                                                <li><a href="#">Gift Cards</a></li>
											</ul>
                                        </div>
										<div class="col-sm-4 text-center">
                                              <div class="banner">
                                                <a href="#"><img src="img/menu-sale.jpg" alt="GUESS SALE"></a><br><br>
												<a href="#">Sale</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                    </li>
				<!-- katapusan ng individual dropdown menu item -->	
					
                </ul>

            </div>
<!-- katapusan para sa mga menu items -->			


            
			<div class="navbar-buttons">


                <div class="navbar-collapse collapse right"><!--id="search-not-mobile" -->
                  
					<a href="#" class="btn"   data-toggle="collapse" data-target="#search">
						 <span class="sr-only">Toggle search</span>
                        <i class="fa fa-search"></i>
					</a>
                       <a href="stores.html" class="btn"><i class="fa fa-map-marker"></i></a>
					   <a href="login.html" class="btn"><i class="fa fa-user"></i></a>
					   <a href="basket.html" class="btn"><i class="fa fa-shopping-bag"></i></a>
					   
				 
					
					
				</div>
				 
            </div>

           
			<!-- umpisa ng search toggle -->
		   <div class="collapse clearfix" id="search">

                <form class="navbar-form" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search">
                        <span class="input-group-btn">

			    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
				</span>
                    </div>
                </form>

            </div>
			<!-- katapusan ng search toggle --> 

			
        </div>
</div>  	
<!-- *** KATAPUSAN NG NAVBAR ***  -->




	 
 

    <div id="all">

        <div id="content">
            <div class="container-fluid">

                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="#">Home</a>
                        </li>
                        <li>New Arrivals</li>
                        <li>Women</li>
                    </ul>
                </div>

				
				<!-- START OF THE LEFT COLUMN / SIDE PANEL -->
                <div class="col-md-3">
                    <div class="panel panel-default sidebar-menu">

                        <div class="panel-heading">
                            <h3 class="panel-title">Categories</h3>
                        </div>

                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked category-menu">
                               <li class="active"><a href="category.html">New Arrivals<span class="badge pull-right">37</span></a></li>
                               <li><a href="category.html">Women  <span class="badge pull-right">88</span></a></li>
                                <li><a href="category.html">Men  <span class="badge pull-right">56</span></a></li>
								<li><a href="category.html">Kids  <span class="badge pull-right">87</span></a></li>
								<li><a href="category.html">Handbags  <span class="badge pull-right">11</span></a></li>

                            </ul>

                        </div>
                    </div>

                     
					<!-- START OF COLORS -->
                    <div class="panel panel-default sidebar-menu hidden">

                        <div class="panel-heading">
                            <h3 class="panel-title">Colors</h3>
                        </div>

                        <div class="panel-body">

                            <form>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> <span class="colour white"></span> White (14)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> <span class="colour blue"></span> Blue (10)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> <span class="colour green"></span> Green (20)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> <span class="colour yellow"></span> Yellow (13)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> <span class="colour red"></span> Red (10)
                                        </label>
                                    </div>
                                </div>

								<p class="text-center">
                                <button class="btn btn-default btn-sm btn-primary">APPLY</button>
                                <button class="btn btn-xs btn-danger">CLEAR</button>
								</p>


                            </form>

                        </div>
                    </div>
					<!-- END OF COLORS -->


                    <!-- *** MENUS AND FILTERS END *** -->

                   
				   
                </div>
				<!-- END OF THE LEFT COLUMN / SIDE PANEL -->
				
				
				
                <div class="col-md-9" id="main">
                    <div class="box">
                        <h1>New Arrivals</h1>
                        
                    </div>

                    <div class="box info-bar">
                        <div class="row">
                            <div class="col-sm-12 col-md-4 products-showing">
                                Showing <strong>12</strong> of <strong>25</strong> products
                            </div>

                            <div class="col-sm-12 col-md-8  products-number-sort">
                                <div class="row">
                                    <form class="form-inline">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="products-number">
                                                <strong>Show</strong>  <a href="#" class="btn btn-default btn-sm btn-primary">12</a>  <a href="#" class="btn btn-default btn-sm">24</a>  <a href="#" class="btn btn-default btn-sm">All</a> products
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="products-sort-by">
                                                <strong>Sort by</strong>
                                                <select name="sort-by" class="form-control">
                                                    <option>Price</option>
                                                    <option>Name</option>
                                                    <option>Sales first</option>
                                                </select>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row products">

                        <div class="col-md-4 col-sm-6">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img src="img/product1.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img src="img/product1.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img src="img/product1.jpg" alt="" class="img-responsive">
                                </a>
                                <div class="text">
                                    <h3><a href="product.html">Linda Denim Jacket</a></h3>
                                    <p class="price">‎₱ 6,000</p>
                                    <p class="buttons">
                                        <a href="product.html" class="btn ghost">View detail</a>
                                        <a href="basket.html" class="btn ghost-inverted"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                    </p>
                                </div>
                                <!-- /.text -->
								 

								
								
                            </div>
                            <!-- /.product -->
                        </div>

                        <div class="col-md-4 col-sm-6">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img src="img/product2.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img src="img/product2.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img src="img/product2.jpg" alt="" class="img-responsive">
                                </a>
                                <div class="text">
                                    <h3><a href="product.html">Longline Tee Logo</a></h3>
                                    <p class="price"><del>‎₱ 9,000</del> ‎₱ 6,000</p>
                                    <p class="buttons">
                                        <a href="product.html" class="btn ghost">View detail</a>
                                        <a href="basket.html" class="btn ghost-inverted"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                    </p>
                                </div>
                                <!-- /.text -->

                                <div class="ribbon sale">
                                    <div class="theribbon">SALE</div>
                                    <div class="ribbon-background"></div>
                                </div>
                                <!-- /.ribbon -->
 
                            </div>
                            <!-- /.product -->
                        </div>

                       
                         <div class="col-md-4 col-sm-6">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img src="img/product3.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img src="img/product3.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img src="img/product3.jpg" alt="" class="img-responsive">
                                </a>
                                <div class="text">
                                    <h3><a href="product.html">Black Longline Tee Logo</a></h3>
                                    <p class="price">‎₱ 6,000</p>
                                    <p class="buttons">
                                        <a href="product.html" class="btn ghost">View detail</a>
                                        <a href="basket.html" class="btn ghost-inverted"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                    </p>
                                </div>
                                <!-- /.text -->

                                
                            </div>
                            <!-- /.product -->
                        </div>

						
						
                           <div class="col-md-4 col-sm-6">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img src="img/product4.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img src="img/product4.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img src="img/product4.jpg" alt="" class="img-responsive">
                                </a>
                                <div class="text">
                                    <h3><a href="product.html">Longline Tee Logo</a></h3>
                                    <p class="price">‎₱ 6,000</p>
                                    <p class="buttons">
                                        <a href="product.html" class="btn ghost">View detail</a>
                                        <a href="basket.html" class="btn ghost-inverted"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                    </p>
                                </div>
                                <!-- /.text -->
 
                            </div>
                            <!-- /.product -->
                        </div>

                           <div class="col-md-4 col-sm-6">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img src="img/product5.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img src="img/product5.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img src="img/product5.jpg" alt="" class="img-responsive">
                                </a>
                                <div class="text">
                                    <h3><a href="product.html">Longline Tee Logo</a></h3>
                                    <p class="price">‎₱ 6,000</p>
                                    <p class="buttons">
                                        <a href="product.html" class="btn ghost">View detail</a>
                                        <a href="basket.html" class="btn ghost-inverted"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                    </p>
                                </div>
                                <!-- /.text -->
 
                            </div>
                            <!-- /.product -->
                        </div>
						
						
						
                            <div class="col-md-4 col-sm-6">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img src="img/product6.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img src="img/product6.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img src="img/product6.jpg" alt="" class="img-responsive">
                                </a>
                                <div class="text">
                                    <h3><a href="product.html">White Logo Tee</a></h3>
                                    <p class="price">‎₱ 6,000</p>
                                    <p class="buttons">
                                        <a href="product.html" class="btn ghost">View detail</a>
                                        <a href="basket.html" class="btn ghost-inverted"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                    </p>
                                </div>
                                <!-- /.text -->
 
                            </div>
                            <!-- /.product -->
                        </div>
						
						
						
						
						    <div class="col-md-4 col-sm-6">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img src="img/product1.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img src="img/product1.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img src="img/product1.jpg" alt="" class="img-responsive">
                                </a>
                                <div class="text">
                                    <h3><a href="product.html">Linda Denim Jacket</a></h3>
                                    <p class="price">‎₱ 6,000</p>
                                    <p class="buttons">
                                        <a href="product.html" class="btn ghost">View detail</a>
                                        <a href="basket.html" class="btn ghost-inverted"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                    </p>
                                </div>
                                <!-- /.text -->
								 

								
								
                            </div>
                            <!-- /.product -->
                        </div>

                        <div class="col-md-4 col-sm-6">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img src="img/product2.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img src="img/product2.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img src="img/product2.jpg" alt="" class="img-responsive">
                                </a>
                                <div class="text">
                                    <h3><a href="product.html">Longline Tee Logo</a></h3>
                                    <p class="price"><del>‎₱ 9,000</del> ‎₱ 6,000</p>
                                    <p class="buttons">
                                        <a href="product.html" class="btn ghost">View detail</a>
                                        <a href="basket.html" class="btn ghost-inverted"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                    </p>
                                </div>
                                <!-- /.text -->

                                <div class="ribbon sale">
                                    <div class="theribbon">SALE</div>
                                    <div class="ribbon-background"></div>
                                </div>
                                <!-- /.ribbon -->
 
                            </div>
                            <!-- /.product -->
                        </div>

                       
                         <div class="col-md-4 col-sm-6">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img src="img/product3.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img src="img/product3.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img src="img/product3.jpg" alt="" class="img-responsive">
                                </a>
                                <div class="text">
                                    <h3><a href="product.html">Black Longline Tee Logo</a></h3>
                                    <p class="price">‎₱ 6,000</p>
                                    <p class="buttons">
                                        <a href="product.html" class="btn ghost">View detail</a>
                                        <a href="basket.html" class="btn ghost-inverted"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                    </p>
                                </div>
                                <!-- /.text -->

                                
                            </div>
                            <!-- /.product -->
                        </div>

						
						
                           <div class="col-md-4 col-sm-6">
                            <div class="product">
							<div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img class="img-responsive" src="img/product4.jpg" alt="" />
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img class="img-responsive" src="img/product4.jpg" alt="" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img class="img-responsive" src="img/product4.jpg" alt="" />
                                </a>
                                <div class="text">
                                    <h3><a href="product.html">Longline Tee Logo</a></h3>
                                    <p class="price">‎₱ 6,000</p>
                                    <p class="buttons">
                                        <a href="product.html" class="btn ghost">View detail</a>
                                        <a href="basket.html" class="btn ghost-inverted"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                    </p>
                                </div>
                                <!-- /.text -->
 
                            </div>
                            <!-- /.product -->
                        </div>

                           <div class="col-md-4 col-sm-6">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img src="img/product5.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img src="img/product5.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img src="img/product5.jpg" alt="" class="img-responsive">
                                </a>
                                <div class="text">
                                    <h3><a href="product.html">Longline Tee Logo</a></h3>
                                    <p class="price">‎₱ 6,000</p>
                                    <p class="buttons">
                                        <a href="product.html" class="btn ghost">View detail</a>
                                        <a href="basket.html" class="btn ghost-inverted"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                    </p>
                                </div>
                                <!-- /.text -->
 
                            </div>
                            <!-- /.product -->
                        </div>
						
						
						
                            <div class="col-md-4 col-sm-6">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img src="img/product6.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img src="img/product6.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img src="img/product6.jpg" alt="" class="img-responsive">
                                </a>
                                <div class="text">
                                    <h3><a href="product.html">White Logo Tee</a></h3>
                                    <p class="price">‎₱ 6,000</p>
                                    <p class="buttons">
                                        <a href="product.html" class="btn ghost">View detail</a>
                                        <a href="basket.html" class="btn ghost-inverted"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                    </p>
                                </div>
                                <!-- /.text -->
 
                            </div>
                            <!-- /.product -->
                        </div>
						
						
						
						

						
 
                    <div class="pages">

                        <p class="loadMore">
                            <a href="#" class="btn ghost-inverted"><i class="fa fa-chevron-down"></i> Load more</a>
                        </p>
						<br>
                        <ul class="pagination">
                            <li><a href="#">&laquo;</a>
                            </li>
                            <li class="active"><a href="#">1</a>
                            </li>
                            <li><a href="#">2</a>
                            </li>
                            <li><a href="#">3</a>
                            </li>
                            <li><a href="#">4</a>
                            </li>
                            <li><a href="#">5</a>
                            </li>
                            <li><a href="#">&raquo;</a>
                            </li>
                        </ul>
						<br>
                    </div>


                </div>
                <!-- /.col-md-9 -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->


		
		
		
	
	
	
	

<!-- *** START FOOTER *** -->
<section id="footer" data-animate="fadeInUp">
	 
			<div class="container">
			<div class="col-sm-3"></div>
			
			<div class="col-sm-6 text-center">
			<p>Enjoy 15% OFF & Free Shipping on your first order</p>
			
						<form>
                            <div class="input-group">
							<input type="text" class="form-control">
								<span class="input-group-btn"><button class="btn btn-danger" type="button">SIGNUP NOW!</button></span>
							</div>
                            <!-- /input-group -->
						<p>
						<input type="checkbox"> By signing up I agree to receive emails about the latest trends and exclusive content.
						</p>
                        </form>
			</div>
			
			<div class="col-sm-3"></div>
			
			</div>
			
			<br><br>
		
			
            <div class="container">
			
			 <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <h4>MY ACCOUNT</h4>

                        <ul>
                            <li><a href="register.html">Register</a></li>
                            <li><a href="login.html">Login</a></li>
                            <li><a href="orders.html">Orders</a></li>
                            <li><a href="returns.html">Returns</a></li>
                         </ul>
 

                       

                        <hr class="hidden-md hidden-lg hidden-sm">

                    </div>
                    <!-- /.col-md-3 -->

                    <div class="col-md-3 col-sm-6">

                        <h4>ASSISTANCE</h4>
						<ul>
                            <li><a href="support.html">Customer Care</a></li>
                            <li><a href="stores.html">Store Locator</a></li>
                            <li><a href="contact.html">Contact Us</a></li>
                            <li><a href="gift.html">Gift Cards</a></li>
                        </ul>
 

                        <hr class="hidden-md hidden-lg">

                    </div>
                    <!-- /.col-md-3 -->

                    <div class="col-md-3 col-sm-6">

                        <h4>COMPANY</h4>
						<ul>
                            <li><a href="stores.html">Our Store</a></li>
                            <li><a href="careers.html">Corporate Careers</a></li>
                            <li><a href="careers-retail.html">Retail Careers</a></li>
                        </ul>
						
                        <p class="hidden">
						<strong>GUESS Philippines</strong>
                            <br>DII Bldg. 
                            <br>150 San Vicente Rd
                            <br>Brgy. San Vicente
                            <br>Laguna
                            <br>
                            <strong>Philippines</strong>
                        </p>
 
                        <hr class="hidden-md hidden-lg">

                    </div>
                    <!-- /.col-md-3 -->



                    <div class="col-md-3 col-sm-6">

                        <h4>FOLLOW US</h4>
 
                        <p>
                            <a href="https://www.facebook.com/GUESSPhil" class="facebook external" target="_blank" data-animate-hover="none"><i class="fa fa-facebook-square fa-2x"></i></a>
                            <a href="https://twitter.com/GUESS" class="twitter external" target="_blank" data-animate-hover="none"><i class="fa fa-twitter fa-2x"></i></a>
                            <a href="https://www.instagram.com/guess" class="instagram external" target="_blank" data-animate-hover="none"><i class="fa fa-instagram fa-2x"></i></a>
                            <a href="https://www.pinterest.com/guess" class="gplus external" target="_blank" data-animate-hover="none"><i class="fa fa-pinterest-p fa-2x"></i></a>
                            <a href="https://www.youtube.com/user/GUESSincTV" class="email external" target="_blank" data-animate-hover="none"><i class="fa fa-youtube-play fa-2x"></i></a>
                        </p> 

                    </div>
                    <!-- /.col-md-3 -->

					
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
			<br><br>
			<p class="small text-center">
			&copy; <script type="text/javascript">document.write(new Date().getFullYear());</script> GUESS Philippines. All rights reserved.
			<a href="terms.html">Terms & Conditions</a> <a href="privacy.html">Privacy Policy</a>
			</p>
			
			
			
			
			
        </section>
<!-- *** END FOOTER *** -->




<!-- *** START COPYRIGHT *** -->
<section id="copyright" class="hidden">
<div class="container ">
		 
			
		 
                <div class="col-md-6">
                    <p class="pull-left">© <script type="text/javascript">document.write(new Date().getFullYear());</script> GUESS Philippines. All rights reserved.</p>

                </div>
                <div class="col-md-6">
                    <p class="pull-right">
					<a href="terms.html" class="active">Terms & Conditions</a> <a href="privacy.html">Privacy Policy</a>
                        
                    </p>
                </div>
			
			
</div>
</section>
<!-- *** COPYRIGHT END *** -->

	
	
	
	
	</div>
</section>
<!-- *** KATAPUSAN PARA SA OVERALL WEBSITE CONTENTS -->  
     

    <!-- *** SCRIPTS TO INCLUDE ***
 _________________________________________________________ -->
    <script src="js/jquery-1.11.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.cookie.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/modernizr.js"></script>
    <script src="js/bootstrap-hover-dropdown.js"></script>
    <script src="js/owl.carousel.min.js"></script>
	<script src="js/front.js"></script>
	
<script src="js/jquery.lazy.js"></script>
	<script> 
	$(function() {
        $('.img-responsive').Lazy();
    });
	</script>
	
    

</body>

</html>