<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="For GUESS Philippines">
    <meta name="author" content="JETHER.NET">
    <meta name="keywords" content="">
	<title>
        GUESS Philippines
    </title>

    <meta name="keywords" content="">

   <!-- <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">-->

    <!-- styles -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">

    <!-- theme stylesheet -->
    <link href="css/style.default.css" rel="stylesheet" id="theme-stylesheet">
	<!-- your stylesheet with modifications -->
    <link href="css/guess.css" rel="stylesheet">
    <link href="css/guess-product.css" rel="stylesheet">

    <script src="js/respond.min.js"></script>

	<link rel="shortcut icon" href="img/favicon.ico">
	<link rel="stylesheet" href="css/swiper-min.css">
   <script src="js/jquery-1.11.0.min.js"></script>


</head> 
<body>
<style>

body {
 padding: 0px;
 margin: 0px;
}

:focus, :active {
  border:none;
  outline: none;
  -webkit-appearance: none;
  -moz-appearance: none;
}

/*for IE10*/
select::-ms-expand {
    display: none;
}

::-webkit-scrollbar {
  display: none;
}

#sizeChart {
	font-family: "GuessFont",Arial,Helvetica,sans-serif;
  font-size: 12px;
	color:#000;
  padding-top: 2%;
  background-color: #fff;
  max-width: 600px;
  overflow: auto;
  margin: 0 auto;
}

#sizeChart .newNav {
  width: 100%;
  margin: 0 auto;
  text-align: center;
}

#sizeChart ul li.active {
  color:#ec1c2d;
  border-bottom: 1px solid #ec1c2d !important;
  height: 30px !important;
}

#sizeChart .newNav ul li {
  display: inline-block;
  overflow: hidden;
  border-bottom: 1px solid transparent;
  height: 30px;
}

#sizeChart .newNav ul li:hover { 
  color: #ec1c2d;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  border-bottom: 1px solid #ec1c2d;
  transition: 0.5s ease;
  height: 30px;
}


#sizeChart #gender ul li {
  display: inline-block;
  overflow: hidden;
  padding: 0%;
  margin: 0 auto;
  margin-bottom: -1px;
}

#sizeChart #gender ul li:hover {
  font-weight: normal;
  color:#ec1c2d;
}

#sizeChart .newNav .Men ul li, #sizeChart .newNav .Kids ul li {
    margin: 0 2%;
}

#sizeChart .newNav .Women ul li {
  padding: 0%;
  margin: 0 2.5%;
}

#sizeChart .col-lg-12, #sizeChart .col-md-12, #sizeChart .col-sm-12, #sizeChart .col-xs-12 {
  padding-left: 0;
  padding-right: 0;
  margin: 0 auto;
}

#sizeChart #gender {
  width: 88%;
  margin: 0 6%;
  padding-bottom: 4%;
  font-size: 1.3em;
}

#sizeChart li#Women, #sizeChart li#Men, #sizeChart li#Kids {
    width: 34%;
    border-right: 1px solid #ebebeb;
}



#sizeChart .Women, #sizeChart .Men, #sizeChart .Kids {
  margin-top: -2%;
  height: 40px;
}

#sizeChart #womenSwiper #topCat {
  margin: 0 2%;
}

#sizeChart #gender ul {
	margin: 0px;
	min-height: 36px;
  background-color: #fff;
  /*padding: 10px 15px; */
  line-height: 8px;
  border:1px solid #ebebeb;
  text-align: center;
  font-family: "GuessFont",Arial,Helvetica,sans-serif;
	color:#000;
	text-transform: uppercase;
	font-weight: 400;
	letter-spacing: 0.2em;
	font-size: 1em;
	text-align-last:center;
	-webkit-appearance: none;
  -moz-appearance: none;
  appearance:none;
  cursor:pointer;
	-webkit-border-radius:0;
	-moz-border-radius:0px;
	border-radius:0px;
	-webkit-box-shadow: 0px;
	-moz-box-shadow: 0px;
	box-shadow: 0px;
  overflow:hidden;
  white-space: nowrap;
}


#sizeChart .newNav ul {
  margin: 0px;
  min-height: 36px;
  line-height: 8px;
	/*border-bottom:1px solid #ebebeb;*/
  text-align: center;
  font-family: "GuessFont",Arial,Helvetica,sans-serif;
  color:#000;
  text-transform: uppercase;
  font-weight: 400;
  letter-spacing: 0.2em;
  font-size: 1em;
  text-align-last:center;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance:none;
  cursor:pointer;
  -webkit-border-radius:0;
  -moz-border-radius:0px;
  border-radius:0px;
  -webkit-box-shadow: 0px;
  -moz-box-shadow: 0px;
  box-shadow: 0px;
  overflow: hidden;
  white-space: nowrap;
	}

#sizeChart ul > li{
	-webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
	text-align: center;
	font-size: 0.917em;
	text-align-last:center;
	line-height: 35px !important;
	padding: 0px;
	color:#000;
	-webkit-border-radius:0;
	-moz-border-radius:0px;
	border-radius:0px;
	-webkit-box-shadow: 0px;
	-moz-box-shadow: 0px;
	box-shadow: 0px;
	list-style: none;
}

#sizeChart .tab {
  font-weight: normal;
  background-color: none;
  color:#ec1c2d;
}


#sizeChart .col-lg-12#conv, .col-md-12#conv, .col-sm-12#conv, .col-xs-12#conv{
  margin: 0 30%;
  text-align: center;
  text-transform: uppercase;
  font-size: 0.8em;

}
#sizeChart #conv div {
  display: inline-block;
  letter-spacing: 1px;
  padding: 0 4%;
}


#chartContents section {
  padding-top: 5%;
  display: none;
}

#chartContents section .table-responsive {
  border:0px !important;
}

#chartContents section table {
  line-height: 24px;
  font-family: "GuessFont",Arial,Helvetica,sans-serif;
  color:#000;
  text-transform: uppercase;
  font-weight: 400;
  letter-spacing: 0.2em;
  font-size: 1em;
  margin: 0 auto;
  width: 100%;
}
#chartContents section .table>tbody>tr>td {
    line-height: 43px !important;
  }


#chartContents section#kdGirls,
#chartContents section#kdBoys,
#chartContents section#kdBaby {
  padding-top: 3%;
}

#chartContents section#kdGirls tbody,
#chartContents section#kdBoys tbody,
#chartContents section#kdBaby tbody {
  text-transform: none;
}

#chartContents section em {
  color: #707070;
  font-style: normal;
}

#chartContents section .table>thead>tr>th {
  vertical-align: bottom;
  line-height: 16px;
  text-align: center;
  font-size: 0.917em;
  letter-spacing: 0.2em;
  font-weight: bold;
  padding: 11px;
  padding-bottom: 2%;
}

#chartContents .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
  border-top: 0;
  border-bottom: 0;
}

#chartContents section .table>tbody>tr>td {
   padding: 8px;
   line-height: 35px;
   vertical-align: middle;
   text-align: center;
}

#chartContents section .table-fixed tbody {
   height: 230px;
   overflow-y: scroll;
   width: 100%;
 }

#sizeChart a {
  text-decoration: none;
}


/*Swiper styling*/

#sizeChart .newNav #womenSwiper.swiper-container ul {
  padding: 0 2%;
  margin: 0 -14%;
}

#sizeChart .newNav #womenSwiper.swiper-container #shoeCat ul {
    margin-right: 11%;
}

#womenSwiper.swiper-container {
  width: 100%;
  height: 100%;
}

#womenSwiper.swiper-slide {
  text-align: center;
  font-size: 1.1em;
  background: #fff;

  /* Center slide text vertically */
  display: -webkit-box;
  display: -ms-flexbox;
  display: -webkit-flex;
  display: flex;
  -webkit-box-pack: center;
  -ms-flex-pack: center;
  -webkit-justify-content: center;
  justify-content: center;
  -webkit-box-align: center;
  -ms-flex-align: center;
  -webkit-align-items: center;
  align-items: center;
}

@media(max-width: 1025px){
  #sizeChart .newNav .Men ul, #sizeChart .newNav .Kids ul {
    display: inline-block;
    padding: 0 5%;
  }
}

@media (max-width: 768px){
  #sizeChart #gender {
    width: 100%;
    margin:0 auto;
    padding-bottom: 4%;
    font-size: 1.3em;
  }

#sizeChartModal iframe {
  border: 0;
  width: 100% !important;
  margin: 0 auto;
  text-align: center;
  float: none;
  }

#sizeChartModal {
    z-index: 9999;
    left: 0;
    top: 0;
    background: #fff;
    border-radius: 0;
    /* padding: 0 1% 2% 1%; */
    width: 100%;
    height: 100%;
    position:fixed !important;
    -webkit-overflow-scrolling: touch;
    overflow-y: scroll;
  }
}

@media screen and (max-width: 767px){
  #sizeChart .table-responsive.col-xs-8 {
    width: 66.66666666666666%;
  }
}



@media(max-width: 414px){


  #sizeChart .newNav ul li {
    font-size: 0.9em;
  }

  #sizeChart {
    max-width: 404px;
    z-index: 9999;
    left: 0;
    top: 0;
    background: #fff;
    border-radius: 0;
    /* padding: 0 1% 2% 1%; */
    width: 100%;
    height: 100%;
    position: fixed !important;
    padding: 3% 3% 0% 3%;
  }

  #sizeChartModal {
    overflow-y: scroll;
    -webkit-overflow-scrolling: touch;
  }
}

@media(max-width: 375px){
  #sizeChart .newNav .Men ul {
    padding: 0 3%;
  }

  #sizeChart {
    max-width: 360px;
  }

#sizeChart .newNav #womenSwiper.swiper-container #topCat ul {
  margin: 0 -8%;
}

}


@media (max-width: 320px){
  #sizeChart {
    max-width: 310px;
  }
  #sizeChart .newNav #womenSwiper.swiper-container #topCat ul {
    margin: 0 -15%;
  }
}

</style>

<div id="sizeChart">
	<div class="row col-lg-12">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="col-xs-12" id="gender">
			  <ul>
          <li id="Women">Women</li><li id="Men">
          Men</li><li id="Kids">
          Kids</li>
        </ul>
			</div>
		  </div>


      <div class="col-lg-12 col-md-12 col-sm-12 hidden-xs newNav">
        <div class="Women" style="display:none;">
            <ul>
              <li class="dressCat">Dresses</li>
              <li class="topCat">Tops & Jackets</li>
              <li class="bottomCat">Bottoms</li>
              <li class="denimCat">Denim</li>
                <li class="swimCat">Swim</li>
              <li class="shoeCat">Shoes</li>
            </ul>
        </div>
            <div class="Men" style="display:none;">
              <ul>
                <li class="mShirtCat">Shirts & Jackets</li>
                <li class="mBottomCat">Bottoms</li>
                <li class="mShoeCat">Shoes</li>
            </ul>
          </div>
          <div class="Kids" style="display:none;">
            <ul>
              <li class="kGirlCat">Girl</li>
              <li class="kBoyCat">Boy</li>
              <li class="kBabyCat">Baby</li>
          </ul>
        </div>
        </div>

<!-- mobile -->
        <div class="col-xs-12 hidden-sm hidden-lg hidden-md newNav">
          <div class="Women" style="">

                <div class="swiper-container" id="womenSwiper">
                  <div class="swiper-wrapper">
                    <div class="swiper-slide">
                      <ul>
                        <li class="dressCat">Dresses</li>
                      </ul>
                    </div>
                    <div class="swiper-slide">
                      <ul>
                        <li class="bottomCat">Bottoms</li>
                      </ul>
                    </div>
                    <div class="swiper-slide">
                      <ul>
                        <li class="denimCat">Denim</li>
                      </ul>
                    </div>
                    <div class="swiper-slide" id="topCat">
                      <ul>
                        <li class="topCat">Tops & Jackets</li>
                      </ul>
                    </div>
                    <div class="swiper-slide">
                      <ul>
                        <li class="swimCat">Swim</li>
                      </ul>
                    </div>
                    <div class="swiper-slide" id="shoeCat">
                      <ul>
                        <li class="shoeCat">Shoes</li>
                      </ul>
                    </div>
                </div><!-- end swiper wrapper -->
             </div><!-- end swiper container -->
          </div><!-- end women container -->

          <div class="Men" style="display:none;">
            <div class="swiper-container" id="menSwiper">
              <div class="swiper-wrapper">
                <div class="swiper-slide">
                  <ul>
                    <li class="mShirtCat">Shirts & Jackets</li>
                  </ul>
                  <ul>
                    <li class="mBottomCat">Bottoms</li>
                  </ul>
                  <ul>
                    <li class="mShoeCat">Shoes</li>
                </ul>
              </div>
          </div><!-- end swiper wrapper -->
        </div><!-- end swiper container -->
      </div><!-- end men container -->

      <div class="Kids" style="display:none;">
        <div class="swiper-container" id="kidsSwiper">
          <div class="swiper-wrapper">
            <div class="swiper-slide">
              <ul>
                <li class="kGirlCat">Girl</li>
              </ul>
              <ul>
                <li class="kBoyCat">Boy</li>
              </ul>
              <ul>
                <li class="kBabyCat">Baby</li>
            </ul>
          </div>
      </div><!-- end swiper wrapper -->
    </div><!-- end swiper container -->
  </div><!-- end men container -->
    </div><!-- end mobile nav -->



    <div id="chartContents" class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <section id="ycDresses" style="display:none">
            <div class="table-responsive">
            <table class="table table-striped">
                <thead>
              <tr>
                <th class="col-lg-3" colspan="2">Size</th>
                <th class="col-lg-2">Bust</th>
                <th class="col-lg-2">Waist</th>
                <th class="col-lg-2">Hip</th>
            </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="col-lg-2">XXS</td>
                        <td class="col-lg-2">-</td>
                        <td class="col-lg-2">32.5"</td>
                        <td class="col-lg-2">24"</td>
                        <td class="col-lg-2">34.5"</td>
                    </tr>
                    <tr>
                        <td class="col-lg-2">XS</td>
                        <td class="col-lg-2">0 - 2</td>
                        <td class="col-lg-2">33.5 - 34.5"</td>
                        <td class="col-lg-2">25 - 26"</td>
                        <td class="col-lg-2">35.5 - 36.5"</td>
                    </tr>
                    <tr>
                        <td class="col-lg-2">S</td>
                        <td class="col-lg-2">4 - 6</td>
                        <td class="col-lg-2">35.5 - 36.5"</td>
                        <td class="col-lg-2">27 - 28"</td>
                        <td class="col-lg-2">37.5 - 38.5"</td>
                    </tr>
                    <tr>
                        <td class="col-lg-2">M</td>
                        <td class="col-lg-2">8</td>
                        <td class="col-lg-2">37.5 - 38"</td>
                        <td class="col-lg-2">29 - 29.5"</td>
                        <td class="col-lg-2">39.5 - 40"</td>
                    </tr>
                    <tr>
                        <td class="col-lg-2">L</td>
                        <td class="col-lg-2">10</td>
                        <td class="col-lg-2">39.5 - 40"</td>
                        <td class="col-lg-2">31 - 31.5"</td>
                        <td class="col-lg-2">41.5 - 42"</td>
                    </tr>
                    <tr>
                        <td class="col-lg-2">XL</td>
                        <td class="col-lg-2">-</td>
                        <td class="col-lg-2">42"</td>
                        <td class="col-lg-2">34"</td>
                        <td class="col-lg-2">44.5"</td>
                    </tr>

                </tbody>
            </table>
        </div>
        </section>

        <section id="ycTopsJackets" style="display:none">
            <div class="table-responsive">
            <table class="table table-striped">
                <thead><tr>
                <th class="col-lg-6" colspan="2">Size</th>
                 <th class="col-lg-3">Bust</th>
                <th class="col-lg-3">Waist</th>


            </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="col-lg-3">XXS</td>
                        <td class="col-lg-3">-</td>
                        <td class="col-lg-3">32.5"</td>
                        <td class="col-lg-3">24"</td>
                    </tr>
                    <tr>
                        <td class="col-lg-3">XS</td>
                        <td class="col-lg-3">0</td>
                        <td class="col-lg-3">33.5"</td>
                        <td class="col-lg-3">25"</td>
                    </tr>
                    <tr>
                        <td class="col-lg-3">S</td>
                        <td class="col-lg-3">2 - 4</td>
                        <td class="col-lg-3">34.5 - 35.5"</td>
                        <td class="col-lg-3">26 - 27"</td>
                    </tr>
                    <tr>
                        <td class="col-lg-3">M</td>
                        <td class="col-lg-3">6 - 8</td>
                        <td class="col-lg-3">36.5 - 38"</td>
                        <td class="col-lg-3">28 - 29.5"</td>
                    </tr>
                    <tr>
                        <td class="col-lg-3">L</td>
                        <td class="col-lg-3">10</td>
                        <td class="col-lg-3">39.5 - 40"</td>
                        <td class="col-lg-3">31 - 31.5"</td>
                    </tr>
                    <tr>
                        <td class="col-lg-3">XL</td>
                        <td class="col-lg-3">-</td>
                        <td class="col-lg-3">42"</td>
                        <td class="col-lg-3">34"</td>
                    </tr>
                </tbody>
            </table>
        </div>
        </section>
        <section id="ycBottoms" style="display:none">
            <div class="table-responsive">
            <table class="table table-striped">
                <thead><tr>
                <th class="col-lg-3" colspan="2">Size</th>
                <th class="col-lg-2">Waist</th>
                <th class="col-lg-2">Hip</th>
                <!-- <th class="col-lg-2">Thigh</th> -->
            </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="col-lg-2">XS</td>
                        <td class="col-lg-2">0</td>
                        <td class="col-lg-2">24.5 - 25"</td>
                        <td class="col-lg-2">35 - 35.5"</td>
                        <!-- <td class="col-lg-2">20 - 20.25</td> -->
                    </tr>
                    <tr>
                        <td class="col-lg-2">S</td>
                        <td class="col-lg-2">2 - 4</td>
                        <td class="col-lg-2">26 - 27"</td>
                        <td class="col-lg-2">36.5 - 37.5"</td>
                        <!-- <td class="col-lg-2">20.75 - 21.5</td> -->
                    </tr>
                    <tr>
                        <td class="col-lg-2">M</td>
                        <td class="col-lg-2">6</td>
                        <td class="col-lg-2">27.5 - 28"</td>
                        <td class="col-lg-2">38.5 - 39"</td>
                        <!-- <td class="col-lg-2">22.25</td> -->
                    </tr>
                    <tr>
                        <td class="col-lg-2">L</td>
                        <td class="col-lg-2">8</td>
                        <td class="col-lg-2">29.5 - 30"</td>
                        <td class="col-lg-2">40 - 41.5"</td>
                        <!-- <td class="col-lg-2">23.25 - 23.5</td> -->
                    </tr>
                    <tr>
                        <td class="col-lg-2">XL</td>
                        <td class="col-lg-2">10</td>
                        <td class="col-lg-2">31 - 32.5"</td>
                        <td class="col-lg-2">41.5 - 44"</td>
                        <!-- <td class="col-lg-2">24.25 - 24.75</td> -->
                    </tr>
                </tbody>
            </table>
        </div>
        </section>
        <section id="ycDenim" style="display:none">
            <div class="table-responsive">
            <table class="table table-striped">
                <thead><tr>
                <th class="col-lg-3">Size</th>
                <th class="col-lg-3">Waist</th>
                 <th class="col-lg-3">Hip</th>
                  <!-- <th class="col-lg-3">Thigh</th> -->

            </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="col-lg-3">23</td>
                        <td class="col-lg-3">23"</td>
                        <td class="col-lg-3">33.5"</td>
                        <!-- <td class="col-lg-3">19</td> -->
                    </tr>
                    <tr>
                        <td class="col-lg-3">24</td>
                        <td class="col-lg-3">24"</td>
                        <td class="col-lg-3">34.5"</td>
                        <!-- <td class="col-lg-3">19.625</td> -->
                    </tr>
                    <tr>
                        <td class="col-lg-3">25</td>
                        <td class="col-lg-3">25"</td>
                        <td class="col-lg-3">35.5"</td>
                        <!-- <td class="col-lg-3">20.25</td> -->
                    </tr>
                    <tr>
                        <td class="col-lg-3">26</td>
                        <td class="col-lg-3">26"</td>
                        <td class="col-lg-3">36.5"</td>
                        <!-- <td class="col-lg-3">21.25</td> -->
                    </tr>
                    <tr>
                        <td class="col-lg-3">27</td>
                        <td class="col-lg-3">27"</td>
                        <td class="col-lg-3">37.5"</td>
                        <!-- <td class="col-lg-3">20.875</td> -->
                    </tr>
                    <tr>
                        <td class="col-lg-3">28</td>
                        <td class="col-lg-3">28"</td>
                        <td class="col-lg-3">38.5"</td>
                        <!-- <td class="col-lg-3">21.5</td> -->
                    </tr>
                    <tr>
                        <td class="col-lg-3">29</td>
                        <td class="col-lg-3">29"</td>
                        <td class="col-lg-3">39.5"</td>
                        <!-- <td class="col-lg-3">22.125</td> -->
                    </tr>
                    <tr>
                        <td class="col-lg-3">30</td>
                        <td class="col-lg-3">30"</td>
                        <td class="col-lg-3">40.5"</td>
                        <!-- <td class="col-lg-3">23.375</td> -->
                    </tr>
                    <tr>
                        <td class="col-lg-3">31</td>
                        <td class="col-lg-3">31.5"</td>
                        <td class="col-lg-3">42"</td>
                        <!-- <td class="col-lg-3">24.25</td> -->
                    </tr>
                    <tr>
                        <td class="col-lg-3">32</td>
                        <td class="col-lg-3">32"</td>
                        <td class="col-lg-3">43.5"</td>
                        <!-- <td class="col-lg-3">25.125</td> -->
                    </tr>

                </tbody>
            </table>
        </div>
        </section>
        <section id="ycSwim" style="display:none">
            <div class="table-responsive">
            <table class="table table-striped">
                <thead><tr>
                <th class="col-lg-3" colspan="2">Size</th>
                  <th class="col-lg-2">EU</th>
                <th class="col-lg-2">Bust</th>
                <th class="col-lg-2">Waist</th>
                <th class="col-lg-2">Hip</th>
            </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="col-lg-2">xs</td>
                        <td class="col-lg-2">1 - 3</td>
                        <td class="col-lg-2">38</td>
                        <td class="col-lg-2">31 - 33"</td>
                        <td class="col-lg-2">23 - 24"</td>
                        <td class="col-lg-2">33 - 34"</td>
                    </tr>
                    <tr>
                        <td class="col-lg-2">s</td>
                        <td class="col-lg-2">5</td>
                        <td class="col-lg-2">40</td>
                        <td class="col-lg-2">33 - 34"</td>
                        <td class="col-lg-2">25 - 26"</td>
                        <td class="col-lg-2">35 - 36"</td>
                    </tr>
                    <tr>
                        <td class="col-lg-2">m</td>
                        <td class="col-lg-2">7</td>
                        <td class="col-lg-2">42</td>
                        <td class="col-lg-2">35 - 36"</td>
                        <td class="col-lg-2">27 - 28"</td>
                        <td class="col-lg-2">37 - 38"</td>

                    </tr>
                    <tr>
                        <td class="col-lg-2">l</td>
                        <td class="col-lg-2">9</td>
                        <td class="col-lg-2">44</td>
                        <td class="col-lg-2">37 - 39"</td>
                        <td class="col-lg-2">29 - 31"</td>
                        <td class="col-lg-2">39 - 41"</td>

                    </tr>
                    <tr>
                        <td class="col-lg-2">xl</td>
                        <td class="col-lg-2">11 - 13</td>
                          <td class="col-lg-2">46</td>
                        <td class="col-lg-2">40 - 42"</td>
                        <td class="col-lg-2">32 - 34"</td>
                        <td class="col-lg-2">42 - 44"</td>

                    </tr>

                </tbody>
            </table>
        </div>
        </section>
         <section id="ycShoes" style="display:none">
            <div class="table-responsive col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <table class="table table-striped">
                <thead><tr>
                <th class="col-lg-6 col-md-6 col-sm-6 col-xs-6">US</th>
                <th class="col-lg-6 col-md-6 col-sm-6 col-xs-6">Euro</th>
            </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="col-lg-6">5</td>
                        <td class="col-lg-6">35</td>
                    </tr>
                    <tr>
                        <td class="col-lg-6">5.5</td>
                        <td class="col-lg-6">36</td>
                    </tr>
                    <tr>
                        <td class="col-lg-6">6</td>
                        <td class="col-lg-6">36.5</td>
                    </tr>
                    <tr>
                        <td class="col-lg-6">6.5</td>
                        <td class="col-lg-6">37</td>
                    </tr>
                    <tr>
                        <td class="col-lg-6">7</td>
                        <td class="col-lg-6">37.5</td>
                    </tr>
                    <tr>
                        <td class="col-lg-6">7.5</td>
                        <td class="col-lg-6">38</td>
                    </tr>
                    <tr>
                        <td class="col-lg-6">8</td>
                        <td class="col-lg-6">38.5</td>
                    </tr>
                    <tr>
                        <td class="col-lg-6">8.5</td>
                        <td class="col-lg-6">39</td>
                    </tr>
                    <tr>
                        <td class="col-lg-6">9</td>
                        <td class="col-lg-6">39.5</td>
                    </tr>
                    <tr>
                        <td class="col-lg-6">9.5</td>
                        <td class="col-lg-6">40</td>
                    </tr>
                    <tr>
                        <td class="col-lg-6">10</td>
                        <td class="col-lg-6">41</td>
                    </tr>
                </tbody>
            </table>
        </div>
        </section>
        <section id="mnShirtsJackets" style="display:none">
            <div class="table-responsive">
            <table class="table table-striped">
                <thead><tr>
                <th class="col-lg-4">Size</th>
                <th class="col-lg-2">Chest</th>
                <th class="col-lg-2">Neck</th>
                <th class="col-lg-2">Shoulder</th>
                <th class="col-lg-2">Sleeve</th>
            </tr>
                </thead>
                <tbody>
                    <tr>

                        <td class="col-lg-2">XS</td>
                        <td class="col-lg-2">35 - 36"</td>
                        <td class="col-lg-2">13 - 13.5"</td>
                        <td class="col-lg-2">16.5"</td>
                        <td class="col-lg-2">31.5"</td>

                    </tr>
                    <tr>

                        <td class="col-lg-2">S</td>
                        <td class="col-lg-2">36 - 37"</td>
                        <td class="col-lg-2">14 - 14.5"</td>
                        <td class="col-lg-2">17"</td>
                        <td class="col-lg-2">32.5"</td>

                    </tr>
                    <tr>

                        <td class="col-lg-2">m</td>
                        <td class="col-lg-2">38 - 40"</td>
                        <td class="col-lg-2">15 - 15.5"</td>
                        <td class="col-lg-2">17.5"</td>
                        <td class="col-lg-2">33.5"</td>

                    </tr>
                    <tr>

                        <td class="col-lg-2">l</td>
                        <td class="col-lg-2">41 - 43"</td>
                        <td class="col-lg-2">16 - 16.5"</td>
                        <td class="col-lg-2">18"</td>
                        <td class="col-lg-2">34.5"</td>

                    </tr>
                    <tr>

                        <td class="col-lg-2">Xl</td>
                        <td class="col-lg-2">44 - 46"</td>
                        <td class="col-lg-2">17 - 17.5"</td>
                        <td class="col-lg-2">18.75"</td>
                        <td class="col-lg-2">35.5"</td>

                    </tr>
                    <tr>

                        <td class="col-lg-2">XXl</td>
                        <td class="col-lg-2">47 - 49"</td>
                        <td class="col-lg-2">18 - 18.5"</td>
                        <td class="col-lg-2">19.75"</td>
                        <td class="col-lg-2">36"</td>

                    </tr>
                     <tr>

                        <td class="col-lg-2">XXXl</td>
                        <td class="col-lg-2">50 - 51"</td>
                        <td class="col-lg-2">19"</td>
                        <td class="col-lg-2">20.25"</td>
                        <td class="col-lg-2">36.5"</td>

                    </tr>

                </tbody>
            </table>
        </div>
        </section>
        <section id="mnBottoms" style="display:none">
            <div class="table-responsive">
            <table class="table table-striped">
                <thead><tr>
                <th class="col-lg-3" colspan="2">Size</th>
                <th class="col-lg-2">Waist</th>
                <th class="col-lg-2">Hip</th>
                <!-- <th class="col-lg-2">Thigh</th> -->
            </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="col-lg-2">28</td>
                        <td class="col-lg-2">XS</td>
                        <td class="col-lg-2">28.5"</td>
                        <td class="col-lg-2">35"</td>
                        <!-- <td class="col-lg-2">20 1/2</td> -->
                    </tr>
                    <tr>
                        <td class="col-lg-2">29</td>
                        <td class="col-lg-2">xs</td>
                        <td class="col-lg-2">29.5"</td>
                        <td class="col-lg-2">36"</td>
                        <!-- <td class="col-lg-2">21</td> -->
                    </tr>
                    <tr>
                        <td class="col-lg-2">30</td>
                        <td class="col-lg-2">s</td>
                        <td class="col-lg-2">30.5"</td>
                        <td class="col-lg-2">37"</td>
                        <!-- <td class="col-lg-2">21 1/2</td> -->

                    </tr>
                    <tr>
                        <td class="col-lg-2">31</td>
                        <td class="col-lg-2">s</td>
                        <td class="col-lg-2">31.5"</td>
                        <td class="col-lg-2">38"</td>
                        <!-- <td class="col-lg-2">22</td> -->

                    </tr>
                    <tr>
                        <td class="col-lg-2">32</td>
                        <td class="col-lg-2">m</td>
                        <td class="col-lg-2">32.5"</td>
                        <td class="col-lg-2">39"</td>
                        <!-- <td class="col-lg-2">22 1/2</td> -->

                    </tr>
                    <tr>
                        <td class="col-lg-2">33</td>
                        <td class="col-lg-2">m</td>
                        <td class="col-lg-2">33.5"</td>
                        <td class="col-lg-2">40"</td>
                        <!-- <td class="col-lg-2">23</td> -->
                    </tr>
                     <tr>
                        <td class="col-lg-2">34</td>
                        <td class="col-lg-2">l</td>
                        <td class="col-lg-2">34.5"</td>
                        <td class="col-lg-2">41"</td>
                        <!-- <td class="col-lg-2">23 1/2</td> -->
                    </tr>
                    <tr>
                        <td class="col-lg-2">36</td>
                        <td class="col-lg-2">l</td>
                        <td class="col-lg-2">35 - 36"</td>
                        <td class="col-lg-2">41.5 - 42.5"</td>
                        <!-- <td class="col-lg-2">24 1/2</td> -->
                    </tr>
                    <tr>
                        <td class="col-lg-2">38</td>
                        <td class="col-lg-2">xl</td>
                        <td class="col-lg-2">37 - 38"</td>
                        <td class="col-lg-2">43.5 - 44.5"</td>
                        <!-- <td class="col-lg-2">25 1/2</td> -->
                    </tr>
                    <tr>
                        <td class="col-lg-2">40</td>
                        <td class="col-lg-2">Xl</td>
                         <td class="col-lg-2">39 - 40"</td>
                        <td class="col-lg-2">45.5 - 46.5"</td>
                        <!-- <td class="col-lg-2">26 1/4</td> -->

                    </tr>
                    <tr>
                        <td class="col-lg-2">42</td>
                        <td class="col-lg-2">XXl</td>
                        <td class="col-lg-2">41 - 42"</td>
                        <td class="col-lg-2">47.5 - 48.5"</td>
                        <!-- <td class="col-lg-2">27 1/2</td> -->
                    </tr>

                </tbody>
            </table>
        </div>
        </section>
        <section id="mnShoes" style="display:none">
            <div class="table-responsive col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <table class="table table-striped">
                <thead><tr>
                <th class="col-lg-6 col-md-6 col-sm-6 col-xs-6">US</th>
                <th class="col-lg-6 col-md-6 col-sm-6 col-xs-6">Euro</th>
            </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="col-lg-6">7</td>
                        <td class="col-lg-6">40</td>
                    </tr>
                    <tr>
                        <td class="col-lg-6">7.5</td>
                        <td class="col-lg-6">40</td>
                    </tr>
                    <tr>
                        <td class="col-lg-6">8</td>
                        <td class="col-lg-6">41</td>
                    </tr>
                    <tr>
                        <td class="col-lg-6">8.5</td>
                        <td class="col-lg-6">41.5</td>
                    </tr>
                    <tr>
                        <td class="col-lg-6">9</td>
                        <td class="col-lg-6">42</td>
                    </tr>
                    <tr>
                        <td class="col-lg-6">9.5</td>
                        <td class="col-lg-6">42.5</td>
                    </tr>
                    <tr>
                        <td class="col-lg-6">10</td>
                        <td class="col-lg-6">43</td>
                    </tr>
                    <tr>
                        <td class="col-lg-6">10.5</td>
                        <td class="col-lg-6">43.5</td>
                    </tr>
                    <tr>
                        <td class="col-lg-6">11</td>
                        <td class="col-lg-6">44</td>
                    </tr>
                    <tr>
                        <td class="col-lg-6">11.5</td>
                        <td class="col-lg-6">44.5</td>
                    </tr>
                    <tr>
                        <td class="col-lg-6">12</td>
                        <td class="col-lg-6">45</td>
                    </tr>
                     <tr>
                        <td class="col-lg-6">13</td>
                        <td class="col-lg-6">46</td>
                    </tr>
                </tbody>
            </table>
        </div>
        </section>
        <section id="kdGirls" style="display:none">
            <div class="table-responsive">
         <h6>LITTLE GIRL</h6>
            <table class="table table-striped">
                <thead><tr>
                <th class="col-lg-2">Size US</th>
                <th class="col-lg-2">Size EU</th>
                <th class="col-lg-2">Age</th>
                <th class="col-lg-2">Height</th>
                <th class="col-lg-2">Weight</th>
            </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="col-lg-2">2T</td>
                        <td class="col-lg-2">2Y</td>
                        <td class="col-lg-2">2 yrs</td>
                        <td class="col-lg-2">33-35.5"</td>
                        <td class="col-lg-2">25-28 lbs</td>
                    </tr>
                    <tr>
                        <td class="col-lg-2">3T</td>
                        <td class="col-lg-2">3Y</td>
                        <td class="col-lg-2">3 yrs</td>
                        <td class="col-lg-2">36-38.5"</td>
                        <td class="col-lg-2">29-32 lbs</td>
                    </tr>
                    <tr>
                        <td class="col-lg-2">4 (S)</td>
                        <td class="col-lg-2">4Y</td>
                        <td class="col-lg-2">4 yrs</td>
                        <td class="col-lg-2">39-41.5"</td>
                        <td class="col-lg-2">33-36 lbs</td>
                    </tr>
                    <tr>
                        <td class="col-lg-2">5 (M)</td>
                        <td class="col-lg-2">5Y</td>
                        <td class="col-lg-2">5 yrs</td>
                        <td class="col-lg-2">42-44.5"</td>
                        <td class="col-lg-2">37-42 lbs</td>
                    </tr>
                    <tr>
                        <td class="col-lg-2">6 (M)</td>
                        <td class="col-lg-2">5XY</td>
                        <td class="col-lg-2">5-6 yrs</td>
                        <td class="col-lg-2">45-46.5"</td>
                        <td class="col-lg-2">43-48 lbs</td>
                    </tr>
                    <tr>
                        <td class="col-lg-2">6X (L)</td>
                        <td class="col-lg-2">6Y</td>
                        <td class="col-lg-2">6-7 yrs</td>
                        <td class="col-lg-2">47-48.5"</td>
                        <td class="col-lg-2">49-54 lbs</td>
                    </tr>

                </tbody>
            </table>
             <h6>BIG GIRL</h6>
            <table class="table table-striped">
                <thead><tr>
                <th class="col-lg-2">Size US</th>
                <th class="col-lg-2">Size EU</th>
                <th class="col-lg-2">Age</th>
                <th class="col-lg-2">Height</th>
                <th class="col-lg-2">Weight</th>
                <!-- <th class="col-lg-2">Inseam</th> -->
            </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="col-lg-2">7(S)</td>
                        <td class="col-lg-2">6Y</td>
                        <td class="col-lg-2">7-8 yrs</td>
                        <td class="col-lg-2">49-51"</td>
                        <td class="col-lg-2">55-60 lbs</td>
                        <!-- <td class="col-lg-2">23"</td> -->
                    </tr>
                    <tr>
                        <td class="col-lg-2">8(S)</td>
                        <td class="col-lg-2">8Y</td>
                        <td class="col-lg-2">8 - 9 yrs</td>
                        <td class="col-lg-2">51.5 53"</td>
                        <td class="col-lg-2">61-66 lbs</td>
                        <!-- <td class="col-lg-2">24.5"</td> -->
                    </tr>
                    <tr>
                        <td class="col-lg-2">10(M)</td>
                        <td class="col-lg-2">10Y</td>
                        <td class="col-lg-2">9 - 10 yrs</td>
                        <td class="col-lg-2">53.5-55"</td>
                        <td class="col-lg-2">67-74 lbs</td>
                        <!-- <td class="col-lg-2">25.25"</td> -->
                    </tr>
                    <tr>
                        <td class="col-lg-2">12(M)</td>
                        <td class="col-lg-2">10Y</td>
                        <td class="col-lg-2">10-11 yrs</td>
                        <td class="col-lg-2">55.5 - 57.5"</td>
                        <td class="col-lg-2">75-84 lbs</td>
                        <!-- <td class="col-lg-2">25.25"</td> -->
                    </tr>
                    <tr>
                        <td class="col-lg-2">14(L)</td>
                        <td class="col-lg-2">12Y</td>
                        <td class="col-lg-2">11-12 yrs</td>
                        <td class="col-lg-2">58 - 60"</td>
                        <td class="col-lg-2">85-96 lbs</td>
                        <!-- <td class="col-lg-2">25.25"</td> -->
                    </tr>
                    <tr>
                        <td class="col-lg-2">16(XL)</td>
                        <td class="col-lg-2">14Y</td>
                        <td class="col-lg-2">12-13 yrs</td>
                        <td class="col-lg-2">60.5 - 63.5"</td>
                        <td class="col-lg-2">97-110 lbs</td>
                        <!-- <td class="col-lg-2">25.25"</td> -->
                    </tr>
                    <tr>
                        <td class="col-lg-2">-</td>
                        <td class="col-lg-2">16Y</td>
                        <td class="col-lg-2">13+ yrs</td>
                        <td class="col-lg-2">63" and up</td>
                        <td class="col-lg-2">111-115 lbs</td>
                        <!-- <td class="col-lg-2">25.25"</td> -->
                    </tr>
                </tbody>
            </table>
        </div>
        </section>
        <section id="kdBoys" style="display:none">
           <div class="table-responsive">
        <h6>LITTLE BOY</h6>
           <table class="table table-striped">
               <thead><tr>
               <th class="col-lg-2">Size US</th>
               <th class="col-lg-2">Size EU</th>
               <th class="col-lg-2">Age</th>
               <th class="col-lg-2">Height</th>
               <th class="col-lg-2">Weight</th>
           </tr>
               </thead>
               <tbody>
                   <tr>
                       <td class="col-lg-2">2T</td>
                       <td class="col-lg-2">2Y</td>
                       <td class="col-lg-2">2 yrs</td>
                       <td class="col-lg-2">33-35.5"</td>
                       <td class="col-lg-2">25-28 lbs</td>
                   </tr>
                   <tr>
                       <td class="col-lg-2">3T</td>
                       <td class="col-lg-2">3Y</td>
                       <td class="col-lg-2">3 yrs</td>
                       <td class="col-lg-2">36-38.5"</td>
                       <td class="col-lg-2">29-32 lbs</td>
                   </tr>
                   <tr>
                       <td class="col-lg-2">4 <em>(S)</em></td>
                       <td class="col-lg-2">4Y</td>
                       <td class="col-lg-2">4 yrs</td>
                       <td class="col-lg-2">39-41.5"</td>
                       <td class="col-lg-2">33-36 lbs</td>
                   </tr>
                   <tr>
                       <td class="col-lg-2">5 <em>(M)</em></td>
                       <td class="col-lg-2">5Y</td>
                       <td class="col-lg-2">5 yrs</td>
                       <td class="col-lg-2">42-44.5"</td>
                       <td class="col-lg-2">37-42 lbs</td>
                   </tr>
                   <tr>
                       <td class="col-lg-2">6 <em>(M)</em></td>
                       <td class="col-lg-2">5XY</td>
                       <td class="col-lg-2">5-6 yrs</td>
                       <td class="col-lg-2">45-46.5"</td>
                       <td class="col-lg-2">43-48 lbs</td>
                   </tr>
                   <tr>
                       <td class="col-lg-2">6X <em>(L)</em></td>
                       <td class="col-lg-2">6Y</td>
                       <td class="col-lg-2">6-7 yrs</td>
                       <td class="col-lg-2">47-48.5"</td>
                       <td class="col-lg-2">49-55 lbs</td>
                   </tr>

               </tbody>
           </table>
            <h6>BIG BOY</h6>
           <table class="table table-striped">
               <thead><tr>
               <th class="col-lg-2">Size US</th>
               <th class="col-lg-2">Size EU</th>
               <th class="col-lg-2">Age</th>
               <th class="col-lg-2">Height</th>
               <th class="col-lg-2">Weight</th>

           </tr>
               </thead>
               <tbody>
                   <tr>
                       <td class="col-lg-2">7</td>
                       <td class="col-lg-2">6Y</td>
                       <td class="col-lg-2">6-7 yrs</td>
                       <td class="col-lg-2">47-48.5"</td>
                       <td class="col-lg-2">49-55 lbs</td>

                   </tr>
                   <tr>
                       <td class="col-lg-2">8 <em>(S)</em></td>
                       <td class="col-lg-2">8Y</td>
                       <td class="col-lg-2">7-8 yrs</td>
                       <td class="col-lg-2">49-51"</td>
                       <td class="col-lg-2">55-59 lbs</td>

                   </tr>
                   <tr>
                       <td class="col-lg-2">10 <em>(S)</em></td>
                       <td class="col-lg-2">10Y</td>
                       <td class="col-lg-2">8-9 yrs</td>
                       <td class="col-lg-2">51.5-53""</td>
                       <td class="col-lg-2">60-73 lbs</td>

                   </tr>
                   <tr>
                       <td class="col-lg-2">12 <em>(M)</em></td>
                       <td class="col-lg-2">12Y</td>
                       <td class="col-lg-2">9-10 yrs</td>
                       <td class="col-lg-2">53.5-55"</td>
                       <td class="col-lg-2">74-87 lbs</td>

                   </tr>
                   <tr>
                       <td class="col-lg-2">14 <em>(M)</em></td>
                       <td class="col-lg-2">12Y</td>
                       <td class="col-lg-2">10-11 yrs</td>
                       <td class="col-lg-2">55.5-57.5"</td>
                       <td class="col-lg-2">88-100 lbs</td>

                   </tr>
                    <tr>
                       <td class="col-lg-2">16 <em>(L)</em></td>
                       <td class="col-lg-2">14Y</td>
                       <td class="col-lg-2">11-12 yrs</td>
                       <td class="col-lg-2">58-60"</td>
                       <td class="col-lg-2">101-115 lbs</td>

                   </tr>
                   <tr>
                       <td class="col-lg-2">18 <em>(L)</em></td>
                       <td class="col-lg-2">14Y</td>
                       <td class="col-lg-2">12-13 yrs</td>
                       <td class="col-lg-2">60.5-62.5"</td>
                       <td class="col-lg-2">116-126 lbs</td>

                   </tr>
                   <tr>
                       <td class="col-lg-2">20 <em>(XL)</em></td>
                       <td class="col-lg-2">16Y</td>
                       <td class="col-lg-2">13+ yrs</td>
                       <td class="col-lg-2">63" and up</td>
                       <td class="col-lg-2">127-138 lbs</td>

                   </tr>
               </tbody>
           </table>
       </div>
       </section>
       <section id="kdBaby" style="display:none">
           <div class="table-responsive">
           <table class="table table-striped">
               <thead><tr>
               <th class="col-lg-2">Size</th>
               <th class="col-lg-2">Age</th>
               <th class="col-lg-2">Height</th>
               <th class="col-lg-2">Weight</th>
           </tr>
               </thead>
               <tbody>
                   <tr>
                       <td class="col-lg-2">0/3M</td>
                       <td class="col-lg-2">0-3 mo</td>
                       <td class="col-lg-2">up to 23.5"</td>
                       <td class="col-lg-2">up to 12 lbs</td>
                   </tr>
                   <tr>
                       <td class="col-lg-2">3/6M</td>
                       <td class="col-lg-2">3-6 mo</td>
                       <td class="col-lg-2">24-26.5"</td>
                       <td class="col-lg-2">12.4-16.4 lbs</td>
                   </tr>
                   <tr>
                       <td class="col-lg-2">6/9M</td>
                       <td class="col-lg-2">6-9 mo</td>
                       <td class="col-lg-2">27-28.5"</td>
                       <td class="col-lg-2">17-20 lbs</td>
                   </tr>
                   <tr>
                       <td class="col-lg-2">12M</td>
                       <td class="col-lg-2">9-12 mo</td>
                       <td class="col-lg-2">29-30.5"</td>
                       <td class="col-lg-2">20.5-24 lbs</td>
                   </tr>
                   <tr>
                       <td class="col-lg-2">6M</td>
                       <td class="col-lg-2">6-9 mo</td>
                       <td class="col-lg-2">27-28.5"</td>
                       <td class="col-lg-2">17-20 lbs</td>
                   </tr>
                    <tr>
                       <td class="col-lg-2">12M</td>
                       <td class="col-lg-2">9-12 mo</td>
                       <td class="col-lg-2">29-30.5"</td>
                       <td class="col-lg-2">20.5-24 lbs</td>
                   </tr>
                    <tr>
                       <td class="col-lg-2">18M</td>
                       <td class="col-lg-2">12-18 mo</td>
                       <td class="col-lg-2">21-32.5"</td>
                       <td class="col-lg-2">24.5-27.5 lbs</td>
                   </tr>
                    <tr>
                       <td class="col-lg-2">24M</td>
                       <td class="col-lg-2">18-24 mo</td>
                       <td class="col-lg-2">33-34.5"</td>
                       <td class="col-lg-2">28-30 lbs</td>
                   </tr>

               </tbody>
           </table>
       </div>
       </section>
    </div>
  </div><!-- end row -->
</div><!-- end sizeChart -->

  <script src="js/swiper.js"></script>
  <script src="js/swiper.min.js"></script>
  <script src="js/swiper.jquery.js"></script>
  <script src="js/swiper.jquery.min.js"></script>

<script>

//Swiper
var mySwiper = new Swiper('#menSwiper.swiper-container', {
    slidesPerView: 1,
    freeMode: true,
    breakpoints: {
        360: {
          slidesPerView: 1
        },
        375: {
          slidesPerView: 1
        }
      }
    });

var swiper = new Swiper('#womenSwiper.swiper-container', {
    slidesPerView: 3.5,
    freeMode: true,
    breakpoints: {
      320: {
        slidesPerView: 2.5
      },
      414: {
        slidesPerView: 3.5
      },
      600: {
        slidesPerView: 6
      }
    }
});
</script>

<script>

$(function(){


  $('.newNav .Women').show();
  $('#chartContents #ycDresses').show();
  $('#gender ul li#Women').addClass('tab');
  $('.newNav .Women .dressCat').addClass('active');

  $('#gender ul li').click(function(e){
    if (this.id == "Women") {
      $('.newNav .Women').fadeIn(1000);
      swiper.update();
      $('.newNav .Men').hide();
      $('.newNav .Kids').hide();
      $('#chartContents #ycDresses').fadeIn(1000);
      $('.newNav ul li').removeClass('active');
      $('.newNav .Women .dressCat').addClass('active');
      $('#chartContents section').siblings().not('#ycDresses').hide();
    }
    else if (this.id == "Men") {
      $('.newNav .Men').fadeIn(1000);
      mySwiper.update();
      $('.newNav .Women').hide();
      $('.newNav .Kids').hide();
      $('#chartContents #mnShirtsJackets').fadeIn(1000);
      $('.newNav ul li').removeClass('active');
      $('.newNav .Men .mShirtCat').addClass('active');
      $('#chartContents section').siblings().not('#mnShirtsJackets').hide();
    }
    else if (this.id == "Kids") {
      $('.newNav .Kids').fadeIn(1000);
      mySwiper.update();
      $('.newNav .Women').hide();
      $('.newNav .Men').hide();
      $('#chartContents #kdGirls').fadeIn(1000);
      $('.newNav ul li').removeClass('active');
      $('.newNav .Kids .kGirlCat').addClass('active');
      $('#chartContents section').siblings().not('#kdGirls').hide();
    }
  })

$('.newNav ul li').click(function(e){
  $('.newNav ul li').removeClass('active');
  $(this).addClass('active');
})


$('#gender ul li').click(function(e){
  $('#gender ul li').removeClass('tab');
  $(this).addClass('tab');
})

// Select Women categories
$('.newNav .Women ul li').click(function(e){
  if ($(this).hasClass('bottomCat')) {
    $('#chartContents section').siblings().not('#ycBottoms').hide();
    $('#chartContents #ycBottoms').fadeIn(1000);
  }
  else if ($(this).hasClass('dressCat')) {
    $('#chartContents section').siblings().not('#ycDresses').hide();
    $('#chartContents #ycDresses').fadeIn(1000);
  }
  else if ($(this).hasClass('topCat')) {
    $('#chartContents section').siblings().not('#ycTopsJackets').hide();
    $('#chartContents #ycTopsJackets').fadeIn(1000);
  }
  else if ($(this).hasClass('denimCat')) {
    $('#chartContents section').siblings().not('#ycDenim').hide();
    $('#chartContents #ycDenim').fadeIn(1000);
  }
  else if ($(this).hasClass('shoeCat')) {
    $('#chartContents section').siblings().not('#ycShoes').hide();
    $('#chartContents #ycShoes').fadeIn(1000);
  }
  else if ($(this).hasClass('swimCat')) {
    $('#chartContents section').siblings().not('#ycSwim').hide();
    $('#chartContents #ycSwim').fadeIn(1000);
  }
});

// Select Men Categories
$('.newNav .Men ul li').click(function(e){
  if ($(this).hasClass('mShirtCat')) {
    $('#chartContents section').siblings().not('#mnShirtsJackets').hide();
    $('#chartContents #mnShirtsJackets').fadeIn(1000);
  }
  else if ($(this).hasClass('mBottomCat')){
    $('#chartContents section').siblings().not('#mnBottoms').hide();
    $('#chartContents #mnBottoms').fadeIn(1000);
  }
  else if ($(this).hasClass('mTopCat')) {
    $('#chartContents section').siblings().not('#mnShoes').hide();
    $('#chartContents #mnShoes').fadeIn(1000);
  }
  else if ($(this).hasClass('mShoeCat')) {
    $('#chartContents section').siblings().not('#ycShoes').hide();
    $('#chartContents #ycShoes').fadeIn(1000);
    }
  });
});

// Select Men Categories
$('.newNav .Kids ul li').click(function(e){
  if ($(this).hasClass('kGirlCat')) {
    $('#chartContents section').siblings().not('#kdGirls').hide();
    $('#chartContents #kdGirls').fadeIn(1000);
  }
  else if ($(this).hasClass('kBoyCat')){
    $('#chartContents section').siblings().not('#kdBoys').hide();
    $('#chartContents #kdBoys').fadeIn(1000);
  }
  else if ($(this).hasClass('kBabyCat')) {
    $('#chartContents section').siblings().not('#kdBaby').hide();
    $('#chartContents #kdBaby').fadeIn(1000);
  }
  });

</script>


<script type="text/javascript">
$(function(){
  if (window.devicePixelRatio > 1) {
    $('#sizeChart').css('font-size', '10px');
    $('#sizeChart.container-fluid').css('max-width', '600px');
    }
});
</script>


</body>
</html>
