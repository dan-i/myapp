<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="For GUESS Philippines">
    <meta name="author" content="JETHER.NET">
    <meta name="keywords" content="">
	<title>
        GUESS Philippines
    </title>

    <meta name="keywords" content="">

   <!-- <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">-->

    <!-- styles -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">

    <!-- theme stylesheet -->
    <link href="css/style.default.css" rel="stylesheet" id="theme-stylesheet">
	<!-- your stylesheet with modifications -->
    <link href="css/guess.css" rel="stylesheet">
    <link href="css/guess-product.css" rel="stylesheet">

    <script src="js/respond.min.js"></script>

    <link rel="shortcut icon" href="img/favicon.ico">



</head>

<body>


 
<!-- *** UMPISA NG NAVBAR ***  -->
	<div class="navbar navbar-default navbar-fixed-top yamm" role="navigation" id="navbar">
        <div class="row">
		
			<!-- umpisa ng navbar header -->
            <div class="navbar-header">

                <a class="navbar-brand home" href="index.html" data-animate-hover="none">
                    <img src="img/logo-JETHER.png" alt="GUESS logo" class="col-sm-11 hidden-xs">
                    <img src="img/logo-JETHER.png" alt="GUESS logo" class="visible-xs">
					<span class="sr-only">Go to homepage</span>
                </a>
                <div class="navbar-buttons">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-align-justify"></i>
                    </button>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#search">
                        <span class="sr-only">Toggle search</span>
                        <i class="fa fa-search"></i>
                    </button>
                    <a class="btn btn-default navbar-toggle" href="basket.html">
                        <i class="fa fa-shopping-bag"></i>  <span class="hidden-xs">3 items in cart</span>
                    </a>
					 
					 <a class="btn btn-default navbar-toggle" href="#">
					 <i class="fa fa-user red"></i> 
					</a>
                </div>
            </div>
            <!-- katapusan ng navbar header -->
		
		
<!-- umpisa para sa mga menu items -->			
            <div class="navbar-collapse collapse" id="navigation">

               
				
				<ul class="nav navbar-nav navbar-left">
				
				<!-- umpisa ng individual dropdown menu item -->
				<li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">NEW ARRIVALS <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										<h5>WOMEN</h5>
                                            <ul>
                                                <li><a href="category.html">View All</a></li>
                                                <li><a href="category.html">Dresses</a></li>
                                                <li><a href="category.html">Rompers & Jumpsuits</a></li>
                                                <li><a href="category.html">Denim</a></li>
                                                <li><a href="category.html">Jackets & Outerwear</a></li>
                                                <li><a href="category.html">Tops</a></li>
                                                <li><a href="category.html">Pants</a></li>
                                                <li><a href="category.html">Shorts</a></li>
                                                <li><a href="category.html">Skirts</a></li>
                                                <li><a href="category.html">Sweaters</a></li>
                                                <li><a href="category.html">Lingerie</a></li>
                                                <li><a href="category.html">Swimwear</a></li>
                                                <li><a href="category.html">Activewear</a></li>
                                                <li><a href="category.html">Handbags</a></li>
                                                <li><a href="category.html">Shoes</a></li>
											</ul>
                                        </div>
										
										<div class="col-sm-2">
										<h5>MEN</h5>
                                            <ul>
                                                <li><a href="category.html">View All</a></li>
                                                <li><a href="category.html">Denim</a></li>
                                                <li><a href="category.html">Tanks & T Shirts</a></li>
                                                <li><a href="category.html">Shirts</a></li>
                                                <li><a href="category.html">Shorts</a></li>
                                                <li><a href="category.html">Pants</a></li>
                                                <li><a href="category.html">Jackets & Outerwear</a></li>
                                                <li><a href="category.html">Activewear</a></li>
                                                <li><a href="category.html">Underwear</a></li>
                                                <li><a href="category.html">Swimwear</a></li>
                                                <li><a href="category.html">Shoes</a></li>
											</ul>
                                        </div>
										
										<div class="col-sm-2">
										<h5>KIDS</h5>
                                            <ul>
                                                <li><a href="category.html">View All</a></li>
                                                <li><a href="category.html">Baby</a></li>
                                                <li><a href="category.html">Girl</a></li>
                                                <li><a href="category.html">Boy</a></li>
											</ul>
                                        </div>
										
										<div class="col-sm-2">
										<h5>HANDBAGS</h5>
                                            <ul>
                                                <li><a href="category.html">View All</a></li>
											</ul>
                                        </div>
										
										
                                        <div class="col-sm-2">
										<h5>ACCESSORIES</h5>
                                            <ul>
                                                <li><a href="category.html">View All</a></li>
                                                <li><a href="category.html">Women</a></li>
                                                <li><a href="category.html">Men</a></li>
											</ul>
                                        </div>
										
										
										<div class="col-sm-2">
										<h5>SHOES</h5>
                                             <ul>
                                                <li><a href="category.html">View All</a></li>
                                                <li><a href="category.html">Women</a></li>
                                                <li><a href="category.html">Men</a></li>
											</ul>
                                        </div>
										
										
										 
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                </li>
				<!-- katapusan ng individual dropdown menu item -->
				
				<!-- umpisa ng individual dropdown menu item -->
				<li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">WOMEN <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										
                                            <ul>
                                                <li><a href="#">View All</a></li>
												<li><a href="#">Dresses</a></li>
												<li><a href="#">Denim</a></li>
												<li><a href="#">Tops</a></li>
												<li><a href="#">Rompers & Jumpsuits</a></li>
												<li><a href="#">Bottoms</a></li>
												<li><a href="#">Shorts</a></li>
												<li><a href="#">Skirts</a></li>
												<li><a href="#">Pants</a></li>
												<li><a href="#">Pants</a></li>
												<li><a href="#">Jackets & Outerwear</a></li>
												<li><a href="#">Swimwear</a></li>
												<li><a href="#">Lingerie</a></li>
												<li><a href="#">Activewear</a></li>
												<li><a href="#">Limited Pricing</a></li>
											</ul>
											
                                        </div>
										<div class="col-sm-2">
											<h5>FEATURES</h5>
                                            <ul>
												<li><a href="#">Best Sellers</a></li>
												<li><a href="#">Online Exclusives</a></li>
												<li><a href="#">Online Exclusives</a></li>
												<li><a href="#">The Logo Shop</a></li>
												<li><a href="#">Camilas Picks</a></li>
												<li><a href="#">Fall Essentials</a></li>
												<li><a href="#">Night Out</a></li>
												<li><a href="#">The Wedding Shop</a></li>
												<li><a href="#">Embroidered</a></li>
												<li><a href="#">Marciano</a></li>
												<li><a href="#">Gift Cards</a></li>
											</ul>
                                        </div>
                                        <div class="col-sm-4 text-center">
                                              <div class="banner">
                                                <a href="#"><img src="img/menu-women.jpg" alt="GUESS Women"></a><br><br>
												<a href="#">Shop New</a>
                                            </div>
                                        </div>
										<div class="col-sm-6"></div>
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                    </li>
                <!-- katapusan ng individual dropdown menu item -->
				
				<!-- umpisa ng individual dropdown menu item -->
				<li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">MEN <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										
                                            <ul>
                                                <li><a href="#">View All</a></li>
												<li><a href="#">Denim</a></li>
												<li><a href="#">Tanks & T Shirts</a></li>
												<li><a href="#">Shirts</a></li>
												<li><a href="#">Shorts & Pants</a></li>
												<li><a href="#">Shorts</a></li>
												<li><a href="#">Pants</a></li>
												<li><a href="#">Jackets & Outerwear</a></li>
												<li><a href="#">Activewear</a></li>
												<li><a href="#">Swimwear</a></li>
												<li><a href="#">Underwear</a></li>
												<li><a href="#">Suits</a></li>
												<li><a href="#">Shoes</a></li>
												<li><a href="#">Accessories</a></li>
												<li><a href="#">Limited Pricing</a></li>
											</ul>
											
                                        </div>
										<div class="col-sm-2">
											<h5>FEATURES</h5>
                                            <ul>
												<li><a href="#">Best Sellers</a></li>
												<li><a href="#">Online Exclusives</a></li>
												<li><a href="#">Online Exclusives</a></li>
												<li><a href="#">The Logo Shop</a></li>
												<li><a href="#">Moto Trend</a></li>
												<li><a href="#">Fall Essentials</a></li>
												<li><a href="#">Night Out</a></li>
												<li><a href="#">Casual Shirts</a></li>
												<li><a href="#">Gifts For Him</a></li>
												<li><a href="#">Gift Cards</a></li>
											</ul>
                                        </div>
                                        <div class="col-sm-4 text-center">
                                              <div class="banner">
                                                <a href="#"><img src="img/menu-men.jpg" alt="GUESS Men"></a><br><br>
												<a href="#">Shop New</a>
                                            </div>
                                        </div>
										 <div class="col-sm-6"></div>
										
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                    </li>
				<!-- katapusan ng individual dropdown menu item -->
				
				<!-- umpisa ng individual dropdown menu item -->
                <li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">DENIM <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										<h5>WOMEN</h5>
                                            <ul>
                                                <li><a href="#">View All</a></li>
												<li><a href="#">Skinny</a></li>
												<li><a href="#">High Rise</a></li>
												<li><a href="#">Mid & Low Rise</a></li>
												<li><a href="#">Bootcut & Flare</a></li>
												<li><a href="#">Straight & Relaxed Fit</a></li>
												<li><a href="#">Shorts & Skirts</a></li>
												<li><a href="#">Tops & Jackets</a></li>
											</ul>
											
                                        </div>
										<div class="col-sm-3">
											<h5>MEN</h5>
                                            <ul>
												<li><a href="#">View All</a></li>
												<li><a href="#">Skinny</a></li>
												<li><a href="#">Slim Tapered</a></li>
												<li><a href="#">Slim Straight & Regular Straight</a></li>
												<li><a href="#">Relaxed & Bootcut</a></li>
												<li><a href="#">Shorts</a></li>
												<li><a href="#">Shirts & Jackets</a></li>
												<li><a href="#">Moto & Destroyed</a></li>
											</ul>
											
                                        </div>
										 
                                        <div class="col-sm-4 text-center">
                                              <div class="banner">
                                                <a href="#"><img src="img/menu-denim.jpg" alt="GUESS Denim"></a><br><br>
												<a href="#">Shop Denim</a>
                                            </div>
                                        </div>
										 <div class="col-sm-5"></div>
										
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                </li>
				<!-- katapusan ng individual dropdown menu item -->
					
				<!-- umpisa ng individual dropdown menu item -->	
				<li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">HANDBAGS <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-3">
										
                                            <ul>
                                                <li><a href="#">View All</a></li>
                                                <li><a href="#">Wallets</a></li>
                                                <li><a href="#">Tote Bags</a></li>
                                                <li><a href="#">Satchels</a></li>
                                                <li><a href="#">Crossbody Bags</a></li>
                                                <li><a href="#">Crossbody Shoulder Bags</a></li>
                                                <li><a href="#">Backpacks</a></li>
                                                <li><a href="#">Mini Bags</a></li>
                                                <li><a href="#">Luggage & Travel Bags</a></li>
                                                <li><a href="#">Bag Accessories</a></li>
                                                <li><a href="#">Limited Pricing</a></li>
											</ul>
											
                                        </div>
										<div class="col-sm-3">
											<h5>FEATURES</h5>
                                            <ul>
												<li><a href="#">Best Sellers</a></li>
												<li><a href="#">Trending Now: Details</a></li>
												<li><a href="#">Night Out</a></li>
												<li><a href="#">Matching Sets</a></li>
												<li><a href="#">Logo</a></li>
												<li><a href="#">Online Exclusives</a></li>
												<li><a href="#">Gift Cards</a></li>
												<li><a href="#">Summer Whites</a></li>
											</ul>
										</div>
                                        <div class="col-sm-3 text-center">
                                            <div class="banner">
                                                <a href="#"><img src="img/menu-handbags.jpg" class="img img-responsive" alt="GUESS Handbags"></a><br>
												<a href="#">Shop Handbags</a>
                                            </div>
                                        </div>
										
										<div class="col-sm-3 text-center"></div>
										
										
										
                                    </div>
                                </div>
                               
                            </li>
                        </ul>
				<!-- katapusan ng individual dropdown menu item -->		
                 
				<!-- umpisa ng individual dropdown menu item -->	
				<li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">ACCESSORIES <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										<h5>WOMEN</h5>
                                            <ul>
                                                <li><a href="#">View All</a></li>
												<li><a href="#">Watches</a></li>
												<li><a href="#">Jewelry</a></li>
												<li><a href="#">Belts</a></li>
												<li><a href="#">Hats & Scarves</a></li>
												<li><a href="#">Sunglasses</a></li>
												<li><a href="#">Fragrance</a></li>
												<li><a href="#">Keychains</a></li>
												<li><a href="#">Phone Cases & Tech Accessories</a></li>
												<li><a href="#">Limited Pricing</a></li>
											</ul>
											
                                        </div>
										<div class="col-sm-2">
											<h5>MEN</h5>
                                            <ul>
												<li><a href="#">View All</a></li>
												<li><a href="#">Watches</a></li>
												<li><a href="#">Jewelry</a></li>
												<li><a href="#">Wallets & Bags</a></li>
												<li><a href="#">Belts</a></li>
												<li><a href="#">Hats</a></li>
												<li><a href="#">Sunglasses</a></li>
												<li><a href="#">Fragrance</a></li>
												<li><a href="#">Limited Pricing</a></li>
											</ul>
											
                                        </div>
										
                                        <div class="col-sm-4 text-center">
                                              <div class="banner">
                                                <a href="#"><img src="img/menu-accessories.jpg" alt="GUESS Accessories"></a><br><br>
												<a href="#">Shop Accessories</a>
                                            </div>
                                        </div>
										 <div class="col-sm-"></div>
										
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                    
					</li>
				<!-- katapusan ng individual dropdown menu item -->
				
				<!-- umpisa ng individual dropdown menu item -->	
				<li class="active dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">SALE <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										<h5>BABY</h5>
                                            <ul>
                                                <li><a href="#">View All</a></li>
                                                <li><a href="#">Baby Girl (0-24m)</a></li>
                                                <li><a href="#">Baby Boy (0-24M)</a></li>
											</ul>
                                        </div>
										<div class="col-sm-2">
										<h5>GIRL</h5>
                                            <ul>
                                                <li><a href="#">View All</a></li>
                                                <li><a href="#">Little Girl (2 -7)</a></li>
                                                <li><a href="#">Big Girl (7-16)</a></li>
											</ul>
                                        </div>
										<div class="col-sm-2">
                                        <h5>BOY</h5>
                                            <ul>
                                                <li><a href="#">View All</a></li>
                                                <li><a href="#">Little Boy (2 -7)</a></li>
                                                <li><a href="#">Big Boy (7-18)</a></li>
											</ul>
                                        </div>
										<div class="col-sm-2">
                                        <h5>FEATURES</h5>
                                            <ul>
                                                <li><a href="#">Best Sellers</a></li>
                                                <li><a href="#">Denim Shop</a></li>
                                                <li><a href="#">Back To School</a></li>
                                                <li><a href="#">Floral Shop</a></li>
                                                <li><a href="#">Gifts</a></li>
                                                <li><a href="#">Gift Cards</a></li>
											</ul>
                                        </div>
										<div class="col-sm-4 text-center">
                                              <div class="banner">
                                                <a href="#"><img src="img/menu-sale.jpg" alt="GUESS SALE"></a><br><br>
												<a href="#">Sale</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                    </li>
				<!-- katapusan ng individual dropdown menu item -->	
					
                </ul>

            </div>
<!-- katapusan para sa mga menu items -->			


            
			<div class="navbar-buttons">


                <div class="navbar-collapse collapse right"><!--id="search-not-mobile" -->
                  
					<a href="#" class="btn"   data-toggle="collapse" data-target="#search">
						 <span class="sr-only">Toggle search</span>
                        <i class="fa fa-search"></i>
					</a>
                       <a href="stores.html" class="btn"><i class="fa fa-map-marker"></i></a>
					   <a href="login.html" class="btn"><i class="fa fa-user"></i></a>
					   <a href="basket.html" class="btn"><i class="fa fa-shopping-bag"></i></a>
					   
				 
					
					
				</div>
				 
            </div>

           
			<!-- umpisa ng search toggle -->
		   <div class="collapse clearfix" id="search">

                <form class="navbar-form" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search">
                        <span class="input-group-btn">

			    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
				</span>
                    </div>
                </form>

            </div>
			<!-- katapusan ng search toggle --> 

			
        </div>
</div>  	
<!-- *** KATAPUSAN NG NAVBAR ***  -->





  
 <!-- *** UMPISA PARA SA OVERALL WEBSITE CONTENTS -->  
<section id="pinakalaman">
 <div id="content">
 
 
 <!-- UMPISA NG MAIN ONTENTS --> 
 <div class="container-fluid">
			
			
			 

                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="#">Home</a>
                        </li>
                        <li><a href="#">Women</a>
                        </li>
                        <li><a href="#">Tops</a>
                        </li>
                        <li>Longline Logo Tee</li>
                    </ul>

                </div>

				
				<!-- START LEFT COLUMN -->
                <div class="col-md-2 text-center">
                 	<!-- START OF COLORS -->
						 <ul id="colors" class="tabs"> 
						  
									<a href="#blue" data-toggle="tab"><li class="blue"></li></a>
									<a href="#red" data-toggle="tab"><li class="red"></li></a>
									<a href="#black" data-toggle="tab"><li class="black"></li></a>
									<a href="#gray" data-toggle="tab"><li class="gray"></li></a>
						
						</ul>
					<!-- END OF COLORS -->	
					
					 <br><br>
					
	 
  
					<!-- UMPISA NG TAB CONTENTS SA IBA'T IBANG KULAY -->
					<div class="tab-content">
 				 
						<section id="blue" class="tab-pane fade in active">
                            <div  id="thumbs">
                                 
                                    <table>
									<tr>
									<td>
									<a href="img/longline-logo-tee-white1.jpg" class="thumb">
                                        <img src="img/longline-logo-tee-white1.jpg" alt="" class="img-responsive">
                                    </a>
									</td>
									<td>
                                    <a href="img/longline-logo-tee-white2.jpg" class="thumb">
                                        <img src="img/longline-logo-tee-white2.jpg" alt="" class="img-responsive">
                                    </a>
									</td>
									</tr>
									
									<tr>
									<td>
                                    <a href="img/longline-logo-tee-white3.jpg" class="thumb">
                                        <img src="img/longline-logo-tee-white3.jpg" alt="" class="img-responsive">
                                    </a>
									</td>
									 
									</tr>
									
									</table>
                               
                            </div>
							
						</section>
						
						<section id="red" class="tab-pane fade in">
                            <div  id="thumbs">
                                 
                                    <table>
									<tr>
									<td>
									<a href="img/longline-logo-tee-white1.jpg" class="thumb">
                                        <img src="img/longline-logo-tee-white1.jpg" alt="" class="img-responsive">
                                    </a>
									</td>
									<td>
                                    <a href="img/longline-logo-tee-white2.jpg" class="thumb">
                                        <img src="img/longline-logo-tee-white2.jpg" alt="" class="img-responsive">
                                    </a>
									</td>
									</tr>
									
									<tr>
									<td>
                                    <a href="img/longline-logo-tee-white3.jpg" class="thumb">
                                        <img src="img/longline-logo-tee-white3.jpg" alt="" class="img-responsive">
                                    </a>
									</td>
									<td>
                                    <a href="img/longline-logo-tee-white4.jpg" class="thumb">
                                        <img src="img/longline-logo-tee-white4.jpg" alt="" class="img-responsive">
                                    </a>
									</td>
									</tr>
									
									</table>
                               
                            </div>
							
						</section>
							
						
					 
						<section id="black" class="tab-pane fade in">
                            <div  id="thumbs">
                                 
                                    <table>
									<tr>
									<td>
									<a href="img/longline-logo-tee-white1.jpg" class="thumb">
                                        <img src="img/longline-logo-tee-white1.jpg" alt="" class="img-responsive">
                                    </a>
									</td>
									<td>
                                    <a href="img/longline-logo-tee-white2.jpg" class="thumb">
                                        <img src="img/longline-logo-tee-white2.jpg" alt="" class="img-responsive">
                                    </a>
									</td>
									</tr>
									
									<tr>
									<td>
                                    <a href="img/longline-logo-tee-white3.jpg" class="thumb">
                                        <img src="img/longline-logo-tee-white3.jpg" alt="" class="img-responsive">
                                    </a>
									</td>
									<td>
                                    <a href="img/longline-logo-tee-white4.jpg" class="thumb">
                                        <img src="img/longline-logo-tee-white4.jpg" alt="" class="img-responsive">
                                    </a>
									</td>
									</tr>
									
									</table>
                               
                            </div>
							
						</section>
				   
				   
						<section id="gray" class="tab-pane fade in">
                            <div  id="thumbs">
                                 
                                    <table>
									 
									
									<tr>
									<td>
                                    <a href="img/longline-logo-tee-white3.jpg" class="thumb">
                                        <img src="img/longline-logo-tee-white3.jpg" alt="" class="img-responsive">
                                    </a>
									</td>
									<td>
                                    <a href="img/longline-logo-tee-white4.jpg" class="thumb">
                                        <img src="img/longline-logo-tee-white4.jpg" alt="" class="img-responsive">
                                    </a>
									</td>
									</tr>
									
									</table>
                               
                            </div>
							
						</section>
						
						
						
						
				   
				   </div> 
				<!-- KATAPUSAN NG TAB CONTENTS SA IBA'T IBANG KULAY -->


				   </div>
				<!-- END LEFT COLUMN -->

               
				<!-- START RIGHT COLUMN -->
			    <div class="col-md-10">

				<!-- umpisa ng main product display -->
                    <div class="row" id="productMain">
                        <div class="col-sm-8">
                            <div id="mainImage">
							<span class='zoom' id='ex1'>
                                <img src="img/detailbig1.jpg" alt="" class="img-responsive">
							</span>
                            </div>

                          <br><br>  

                    </div>
					<!-- katapusan ng main product display -->
                        
						<!-- umpisa ng col 4 box -->
						<div class="col-sm-4">
                            <div class="box">
                                <h1 class="text-center">
								Longline Logo Tee<br>
								₱ 6,000
								</h1>
								
								
                                
                                 

                                <p class="text-center buttons">
								<a href="#details" class="scroll-to goToDescription">View Product Details</a> 
                                </p>
								
								
						
					
					
								
								
														
<div class="text-center">
<hr>
<a href="basket.html" class="btn ghost-inverted"><i class="fa fa-shopping-bag"></i> ADD TO BAG</a> 
<!-- <a href="basket.html" class="btn ghost"><i class="fa fa-heart"></i> Add to wishlist</a>-->
<hr>
</div>
	
								
								
									
 
								<!-- start sizes -->
							
                                    <p class="text-center buttons">
                                    <a href="#" class="btn ghost">XS</a> 
									 <a href="#" class="btn ghost">S</a> 
									 <a href="#" class="btn ghost">M</a> 
									 <a href="#" class="btn ghost">L</a> 
									 
									 
									 <br>
									<a data-toggle="modal" href="sizechart.html" data-target="#SizeChart">VIEW SIZE CHART GUIDE</a>  <br>
</p>
 
<!-- umpisa ng size chart modal -->
<div class="modal fade" id="SizeChart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
             
		<div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h2>Size Chart</h2>
        </div>
            <div class="modal-body">
			
			<div class="embed-container">
			<iframe src="sizechart.html" width="auto" height="auto" style="border: 0; width: 100%; min-height: 450px;"></iframe>
			</div>

			
			</div>
            
        </div> 
		<!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> 
<!-- katapusan ng size chart modal -->



                              

                            </div>
							<!-- end sizes -->
							
                     
			 
						
						
					 
						
							<div class="social text-center">
                                <h4>SHARE YOUR FASHION</h4>
                                <p>
                                    <a href="#" class="external facebook" data-animate-hover="pulse"><i class="fa fa-facebook"></i></a>
                                    <a href="#" class="external gplus" data-animate-hover="pulse"><i class="fa fa-google-plus"></i></a>
                                    <a href="#" class="external twitter" data-animate-hover="pulse"><i class="fa fa-twitter"></i></a>
                                    <a href="#" class="email" data-animate-hover="pulse"><i class="fa fa-envelope"></i></a>
                                </p>
                            </div>			 
							
							<br><hr><br>
						
<!-- start product description -->
<div class="box" id="details">
                        <p>
                            <h4>PRODUCT  DETAILS</h4>
                            Style: #W73I42R1KP1<br> 
Super-soft <b>longline tee</b> with our iconic <b>triangle logo</b> at front and rolled cuffs.
<ul>
<li>60% Cotton, 40% Viscose</li>
<li>Machine wash</li>
</ul>
                        </p>
                            
</div>
<!-- end product description -->	
					
							
							
                        </div>

                    </div>
					<!-- katapusan ng col 4 box -->


                

					
					
					 
                                

                    
					
					

                </div>
                <!-- END RIGHT COLUMN -->
           

		   			




				
						

						
	


 
 
 <!-- start of MORE TO LOVE -->
<section id="moretolove"> 

 <div class="container">
 
       <h4 class="text-center">MORE TO LOVE</h4><br><br>               
							
                    <div class="row same-height-row">
                        
					 
 
						
						<div class="col-md-2 col-sm-6">
                            <div class="product same-height">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img src="img/product1.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img src="img/product1.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img src="img/product2.jpg" alt="" class="img-responsive">
                                </a>
                                <div class="text-center">
                                    <h5>Glow In The Dark Logo Tee</h5>
                                    <p class="price">₱ 6,000</p>
                                </div>
                            </div>
                            <!-- /.product -->
                        </div>
						
						

                        
						<div class="col-md-2 col-sm-6">
                            <div class="product same-height">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img src="img/product2.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img src="img/product2.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img src="img/product2.jpg" alt="" class="img-responsive">
                                </a>
                                <div class="text-center">
                                    <h5>Longline Logo Tee</h5>
                                    <p class="price">₱ 3,500</p>
                                </div>
                            </div>
                            <!-- /.product -->
                        </div>
						
						
						
						
						
						<div class="col-md-2 col-sm-6">
                            <div class="product same-height">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img src="img/product3.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img src="img/product3.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img src="img/product3.jpg" alt="" class="img-responsive">
                                </a>
                                <div class="text-center">
                                    <h5>Longline Logo Tee</h5>
                                    <p class="price">₱ 6,000</p>
                                </div>
                            </div>
                            <!-- /.product -->
                        </div>
						
						
						
						
						
						
						<div class="col-md-2 col-sm-6">
                            <div class="product same-height">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img src="img/product4.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img src="img/product4.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img src="img/product4.jpg" alt="" class="img-responsive">
                                </a>
                                <div class="text-center">
                                    <h5>Fitted Logo Tee</h5>
                                    <p class="price">₱ 4,500</p>
                                </div>
                            </div>
                            <!-- /.product -->
                        </div>
						
						
						
						
						
						
						<div class="col-md-2 col-sm-6">
                            <div class="product same-height">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img src="img/product5.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img src="img/product5.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img src="img/product5.jpg" alt="" class="img-responsive">
                                </a>
                                <div class="text-center">
                                    <h5>Zita Sleeveless Bodysuit</h5>
                                    <p class="price">₱ 5,000</p>
                                </div>
                            </div>
                            <!-- /.product -->
                        </div>
						
						
						
						
						
							
						
						<div class="col-md-2 col-sm-6">
                            <div class="product same-height">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front">
                                            <a href="product.html">
                                                <img src="img/product6.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                        <div class="back">
                                            <a href="product.html">
                                                <img src="img/product6.jpg" alt="" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <a href="product.html" class="invisible">
                                    <img src="img/product6.jpg" alt="" class="img-responsive">
                                </a>
                                <div class="text-center">
                                    <h5>Logo Tank</h5>
                                    <p class="price">₱ 4,000</p>
                                </div>
                            </div>
                            <!-- /.product -->
                        </div>
						
						
						
						
						
						
						
						
						
						

                    </div>
</section>
<!-- end of MORE TO LOVE -->
				


				
				
				
				
				
				
				
</div> 
</section>
 <!-- KATAPUSAN NG MAIN CONTENTS -->
			
			
			
	 
				
				
				
		
	
	
	</div>
</section>
<!-- *** KATAPUSAN PARA SA OVERALL WEBSITE CONTENTS -->  

    		


<!-- *** START FOOTER *** -->
<section id="footer" data-animate="fadeInUp">
	 
			<div class="container">
			<div class="col-sm-3"></div>
			
			<div class="col-sm-6 text-center">
			<p>Enjoy 15% OFF & Free Shipping on your first order</p>
			
						<form>
                            <div class="input-group">
							<input type="text" class="form-control">
								<span class="input-group-btn"><button class="btn btn-danger" type="button">SIGNUP NOW!</button></span>
							</div>
                            <!-- /input-group -->
						<p>
						<input type="checkbox"> By signing up I agree to receive emails about the latest trends and exclusive content.
						</p>
                        </form>
			</div>
			
			<div class="col-sm-3"></div>
			
			</div>
			
			<br><br>
		
			
            <div class="container">
			
			 <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <h4>MY ACCOUNT</h4>

                        <ul>
                            <li><a href="register.html">Register</a></li>
                            <li><a href="login.html">Login</a></li>
                            <li><a href="orders.html">Orders</a></li>
                            <li><a href="returns.html">Returns</a></li>
                         </ul>
 

                       

                        <hr class="hidden-md hidden-lg hidden-sm">

                    </div>
                    <!-- /.col-md-3 -->

                    <div class="col-md-3 col-sm-6">

                        <h4>ASSISTANCE</h4>
						<ul>
                            <li><a href="support.html">Customer Care</a></li>
                            <li><a href="stores.html">Store Locator</a></li>
                            <li><a href="contact.html">Contact Us</a></li>
                            <li><a href="gift.html">Gift Cards</a></li>
                        </ul>
 

                        <hr class="hidden-md hidden-lg">

                    </div>
                    <!-- /.col-md-3 -->

                    <div class="col-md-3 col-sm-6">

                        <h4>COMPANY</h4>
						<ul>
                            <li><a href="stores.html">Our Store</a></li>
                            <li><a href="careers.html">Corporate Careers</a></li>
                            <li><a href="careers-retail.html">Retail Careers</a></li>
                        </ul>
						
                        <p class="hidden">
						<strong>GUESS Philippines</strong>
                            <br>DII Bldg. 
                            <br>150 San Vicente Rd
                            <br>Brgy. San Vicente
                            <br>Laguna
                            <br>
                            <strong>Philippines</strong>
                        </p>
 
                        <hr class="hidden-md hidden-lg">

                    </div>
                    <!-- /.col-md-3 -->



                    <div class="col-md-3 col-sm-6">

                        <h4>FOLLOW US</h4>
 
                        <p>
                            <a href="https://www.facebook.com/GUESSPhil" class="facebook external" target="_blank" data-animate-hover="none"><i class="fa fa-facebook-square fa-2x"></i></a>
                            <a href="https://twitter.com/GUESS" class="twitter external" target="_blank" data-animate-hover="none"><i class="fa fa-twitter fa-2x"></i></a>
                            <a href="https://www.instagram.com/guess" class="instagram external" target="_blank" data-animate-hover="none"><i class="fa fa-instagram fa-2x"></i></a>
                            <a href="https://www.pinterest.com/guess" class="gplus external" target="_blank" data-animate-hover="none"><i class="fa fa-pinterest-p fa-2x"></i></a>
                            <a href="https://www.youtube.com/user/GUESSincTV" class="email external" target="_blank" data-animate-hover="none"><i class="fa fa-youtube-play fa-2x"></i></a>
                        </p> 

                    </div>
                    <!-- /.col-md-3 -->

					
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
			<br><br>
			<p class="small text-center">
			&copy; <script type="text/javascript">document.write(new Date().getFullYear());</script> GUESS Philippines. All rights reserved.
			<a href="terms.html">Terms & Conditions</a> <a href="privacy.html">Privacy Policy</a>
			</p>
			
			
			
			
			
        </section>
<!-- *** END FOOTER *** -->




<!-- *** START COPYRIGHT *** -->
<section id="copyright" class="hidden">
<div class="container ">
		 
			
		 
                <div class="col-md-6">
                    <p class="pull-left">© <script type="text/javascript">document.write(new Date().getFullYear());</script> GUESS Philippines. All rights reserved.</p>

                </div>
                <div class="col-md-6">
                    <p class="pull-right">
					<a href="terms.html">Terms & Conditions</a> <a href="privacy.html">Privacy Policy</a>
                        
                    </p>
                </div>
			
			
</div>
</section>
<!-- *** COPYRIGHT END *** -->


	


	
	
    

    <!-- *** SCRIPTS TO INCLUDE *** -->
    <script src="js/jquery-1.11.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.cookie.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/modernizr.js"></script>
    <script src="js/bootstrap-hover-dropdown.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/front.js"></script>

	<script src="js/jquery.lazy.js"></script>
	<script> 
	$(function() {
        $('.img-responsive').Lazy();
    });
	</script>
	

 
   
<!-- start of image zoom -->
 <script src='js/jquery.zoom.js'></script>
	<script>
		$(document).ready(function(){
			$('#ex1').zoom();
			$('#ex2').zoom({ on:'grab' });
			$('#ex3').zoom({ on:'click' });			 
			$('#ex4').zoom({ on:'toggle' });
		});
	</script>
<!-- end of image zoom -->

	
 

</body>

</html>