<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>    
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/main.css" />">
	<title>Stores</title>
</head>
<body>
	<h1>All Stores</h1>
	<div id="stores">
		<table>
			<tr>
				<td>Store Name</td>
				<td>Country</td>
				<td>Address</td>
				<td>Zipcode</td>
				<td>Contact</td>
				<td>Latitude</td>
				<td>Longitude</td>
				<td colspan=3></td>
			</tr>
			<c:choose>
				<c:when test="${empty stores}">
					<td colspan=9><p>No data found.</p></td>
				</c:when>
				<c:otherwise>
					<c:forEach var="store" items="${stores}">
						<tr>
							<td>
								<p>${store.name}</p>
							</td>
							<td>
								<p>${store.country.name}</p>
							</td>
							<td>
								<p>${store.address1}</p>
								<c:if test="${not empty store.address2}">
									<p>${store.address2}</p>
								</c:if>
							</td>
							<td>
								<p>${store.zipcode}</p>
							</td>
							<td>
								<p>${store.contact}</p>
							</td>
							<td>
								<p>${store.latitude}</p>
							</td>
							<td>
								<p>${store.longitude}</p>
							</td>
							<td>
								<input type="button" value="Update" onclick="location.href='/myApp/admin/store/update/${store.id}'"/>
							</td>
							<td>
								<input id="${store.id}" class="buttonDelete" type="button" value="Delete" />
								
							</td>
							<td>
								<input type="button" value="View Location" onclick="location.href='/myApp/admin/store/view/location/${store.id}'" /> 
							</td>
						</tr>
					</c:forEach>
				</c:otherwise>
			</c:choose>
			
		</table>
	</div>
	<br />
	<div><input type="button" value="Create new Store" onclick="location.href='/myApp/admin/store/create'"/></div>
	
	<script type="text/javascript" src="<c:url value="/resources/js/plugins/jquery-3.2.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/store.js" />"></script>
</body>
</html>