<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<style>
		.error{
			color:red;
			font-size:15px;
			display:none;
		}
		#map{
			height: 400px;
			width:100%;
		}
	</style>
	<title>Store - View Location</title>
</head>
<body>
	<h1>View Store</h1>
	<div class="error">
		<p>An error occured!</p>
	</div>
		<input type="hidden" id="storeId" value="${store.id}" />
		<div><p>Name: ${store.name}</p></div>
		<div><p>Address: ${store.address1} </p></div>
		<div><p>Zip code: ${store.zipcode}</p></div>
		<div><p>Location (Lat, Long): <div style="display:inline-block;" id="latitude">${store.latitude}</div>, <div style="display:inline-block;" id="longitude">${store.longitude}</div></p></div>
		<br/>
		<br/>
		<h2>Map:</h2>
		<div id="map">
			
		</div>	
		<br/>
		<div><input type="button" id="back" value="< Back" onclick="location.href='view'"/></div>
	
	<script type="text/javascript" src="<c:url value="/resources/js/plugins/jquery-3.2.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/storeLocation.js" />"></script>
	<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSdYnDeIooiAFfecg862qas1oU6QK8UGA&callback=initMap">
    </script>
	
</body>
</html>