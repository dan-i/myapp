<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<style>
		.error{
			color:red;
			font-size:15px;
			display:none;
		}
	</style>
	<title>Stores - Create</title>
</head>
<body>
	<h1>Create Store</h1>
	<div class="error">
		<p>An error occured!</p>
	</div>
	<form action="create">
		<div>Name</div>
		<div><input type="text" id="name"/></div>
		<div>Address (Line 1)</div>
		<div><input type="text" id="address1" /></div>
		<div>Address (Line 2)</div>
		<div><input type="text" id="address2" /></div>
		<div>Zipcode</div>
		<div><input type="text" id="zipcode" /></div>
		<div>Contact</div>
		<div><input type="text" id="contact" /></div>
		<div>Latitude</div>
		<div><input type="text" id="latitude" /></div>
		<div id="latitudeError" class="error">Latitude is not within the correct range.</div>
		<div>Longitude</div>
		<div><input type="text" id="longitude" /></div>
		<div id="longitudeError" class="error">Longtitude is not within the correct range.</div>
		<br/>
		<div id="loader"><p>Attempting to retrieve your location...</p></div>
		<div id="locationError" class="error">! Sorry, we were not able to retrieve your location.</div>
		<br/>
		<div><input type="button" id="createStore" value="Create Store"/></div>
	</form>
	
	<script type="text/javascript" src="<c:url value="/resources/js/plugins/jquery-3.2.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/store.js" />"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/retrieveLocation.js" />"></script>
</body>
</html>