<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<style>
		.error{
			color:red;
			font-size:15px;
			display:none;
		}
	</style>
	<title>Stores - Update</title>
</head>
<body>
	<h1>Update Store</h1>
	<div class="error">
		<p>An error occured!</p>
	</div>
	<form action="update">
		<input type="hidden" id="storeId" value="${store.id}" />
		<div>Name</div>
		<div><input type="text" id="name" value="${store.name}"/></div>
		<div>Address (Line 1)</div>
		<div><input type="text" id="address1" value="${store.address1}" /></div>
		<div>Address (Line 2)</div>
		<div><input type="text" id="address2" value="${store.address2}" /></div>
		<div>Zipcode</div>
		<div><input type="text" id="zipcode" value="${store.zipcode}"/></div>
		<div>Contact</div>
		<div><input type="text" id="contact" value="${store.contact}"/></div>
		<div>Latitude</div>
		<div><input type="text" id="latitude" value="${store.latitude}"/></div>
		<div id="latitudeError" class="error">Latitude is not within the correct range.</div>
		<div>Longitude</div>
		<div><input type="text" id="longitude" value="${store.longitude}" /></div>
		<div id="longtitudeError" class="error">Longtitude is not within the correct range.</div>
		<br/>
		<div><input type="button" id="updateStore" value="Update Store"/></div>
	</form>
	
	<script type="text/javascript" src="<c:url value="/resources/js/plugins/jquery-3.2.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/store.js" />"></script>
</body>
</html>