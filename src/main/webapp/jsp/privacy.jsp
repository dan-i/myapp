<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="For GUESS Philippines">
    <meta name="author" content="JETHER.NET">
    <meta name="keywords" content="">
	<title>
        GUESS Philippines
    </title>

    <meta name="keywords" content="">

 

    <!-- styles -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">

    <!-- theme stylesheet -->
    <link href="css/style.default.css" rel="stylesheet" id="theme-stylesheet">
	<!-- your stylesheet with modifications -->
    <link href="css/guess.css" rel="stylesheet">

    <script src="js/respond.min.js"></script>

    <link rel="shortcut icon" href="img/favicon.ico">



</head>

<body>


 
<!-- *** UMPISA NG NAVBAR ***  -->
	<div class="navbar navbar-default navbar-fixed-top yamm" role="navigation" id="navbar">
        <div class="row">
		
			<!-- umpisa ng navbar header -->
            <div class="navbar-header">

                <a class="navbar-brand home" href="index.html" data-animate-hover="none">
                    <img src="img/logo-JETHER.png" alt="GUESS logo" class="col-sm-11 hidden-xs">
                    <img src="img/logo-JETHER.png" alt="GUESS logo" class="visible-xs">
					<span class="sr-only">Go to homepage</span>
                </a>
                <div class="navbar-buttons">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-align-justify"></i>
                    </button>
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#search">
                        <span class="sr-only">Toggle search</span>
                        <i class="fa fa-search"></i>
                    </button>
                    <a class="btn btn-default navbar-toggle" href="basket.html">
                        <i class="fa fa-shopping-bag"></i>  <span class="hidden-xs">3 items in cart</span>
                    </a>
					 
					 <a class="btn btn-default navbar-toggle" href="#">
					 <i class="fa fa-user red"></i> 
					</a>
                </div>
            </div>
            <!-- katapusan ng navbar header -->
		
		
<!-- umpisa para sa mga menu items -->			
            <div class="navbar-collapse collapse" id="navigation">

               
				
				<ul class="nav navbar-nav navbar-left">
				
				<!-- umpisa ng individual dropdown menu item -->
				<li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">NEW ARRIVALS <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										<h5>WOMEN</h5>
                                            <ul>
                                                <li><a href="category.html">View All</a></li>
                                                <li><a href="category.html">Dresses</a></li>
                                                <li><a href="category.html">Rompers & Jumpsuits</a></li>
                                                <li><a href="category.html">Denim</a></li>
                                                <li><a href="category.html">Jackets & Outerwear</a></li>
                                                <li><a href="category.html">Tops</a></li>
                                                <li><a href="category.html">Pants</a></li>
                                                <li><a href="category.html">Shorts</a></li>
                                                <li><a href="category.html">Skirts</a></li>
                                                <li><a href="category.html">Sweaters</a></li>
                                                <li><a href="category.html">Lingerie</a></li>
                                                <li><a href="category.html">Swimwear</a></li>
                                                <li><a href="category.html">Activewear</a></li>
                                                <li><a href="category.html">Handbags</a></li>
                                                <li><a href="category.html">Shoes</a></li>
											</ul>
                                        </div>
										
										<div class="col-sm-2">
										<h5>MEN</h5>
                                            <ul>
                                                <li><a href="category.html">View All</a></li>
                                                <li><a href="category.html">Denim</a></li>
                                                <li><a href="category.html">Tanks & T Shirts</a></li>
                                                <li><a href="category.html">Shirts</a></li>
                                                <li><a href="category.html">Shorts</a></li>
                                                <li><a href="category.html">Pants</a></li>
                                                <li><a href="category.html">Jackets & Outerwear</a></li>
                                                <li><a href="category.html">Activewear</a></li>
                                                <li><a href="category.html">Underwear</a></li>
                                                <li><a href="category.html">Swimwear</a></li>
                                                <li><a href="category.html">Shoes</a></li>
											</ul>
                                        </div>
										
										<div class="col-sm-2">
										<h5>KIDS</h5>
                                            <ul>
                                                <li><a href="category.html">View All</a></li>
                                                <li><a href="category.html">Baby</a></li>
                                                <li><a href="category.html">Girl</a></li>
                                                <li><a href="category.html">Boy</a></li>
											</ul>
                                        </div>
										
										<div class="col-sm-2">
										<h5>HANDBAGS</h5>
                                            <ul>
                                                <li><a href="category.html">View All</a></li>
											</ul>
                                        </div>
										
										
                                        <div class="col-sm-2">
										<h5>ACCESSORIES</h5>
                                            <ul>
                                                <li><a href="category.html">View All</a></li>
                                                <li><a href="category.html">Women</a></li>
                                                <li><a href="category.html">Men</a></li>
											</ul>
                                        </div>
										
										
										<div class="col-sm-2">
										<h5>SHOES</h5>
                                             <ul>
                                                <li><a href="category.html">View All</a></li>
                                                <li><a href="category.html">Women</a></li>
                                                <li><a href="category.html">Men</a></li>
											</ul>
                                        </div>
										
										
										 
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                </li>
				<!-- katapusan ng individual dropdown menu item -->
				
				<!-- umpisa ng individual dropdown menu item -->
				<li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">WOMEN <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										
                                            <ul>
                                                <li><a href="#">View All</a></li>
												<li><a href="#">Dresses</a></li>
												<li><a href="#">Denim</a></li>
												<li><a href="#">Tops</a></li>
												<li><a href="#">Rompers & Jumpsuits</a></li>
												<li><a href="#">Bottoms</a></li>
												<li><a href="#">Shorts</a></li>
												<li><a href="#">Skirts</a></li>
												<li><a href="#">Pants</a></li>
												<li><a href="#">Pants</a></li>
												<li><a href="#">Jackets & Outerwear</a></li>
												<li><a href="#">Swimwear</a></li>
												<li><a href="#">Lingerie</a></li>
												<li><a href="#">Activewear</a></li>
												<li><a href="#">Limited Pricing</a></li>
											</ul>
											
                                        </div>
										<div class="col-sm-2">
											<h5>FEATURES</h5>
                                            <ul>
												<li><a href="#">Best Sellers</a></li>
												<li><a href="#">Online Exclusives</a></li>
												<li><a href="#">Online Exclusives</a></li>
												<li><a href="#">The Logo Shop</a></li>
												<li><a href="#">Camilas Picks</a></li>
												<li><a href="#">Fall Essentials</a></li>
												<li><a href="#">Night Out</a></li>
												<li><a href="#">The Wedding Shop</a></li>
												<li><a href="#">Embroidered</a></li>
												<li><a href="#">Marciano</a></li>
												<li><a href="#">Gift Cards</a></li>
											</ul>
                                        </div>
                                        <div class="col-sm-4 text-center">
                                              <div class="banner">
                                                <a href="#"><img src="img/menu-women.jpg" alt="GUESS Women"></a><br><br>
												<a href="#">Shop New</a>
                                            </div>
                                        </div>
										<div class="col-sm-6"></div>
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                    </li>
                <!-- katapusan ng individual dropdown menu item -->
				
				<!-- umpisa ng individual dropdown menu item -->
				<li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">MEN <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										
                                            <ul>
                                                <li><a href="#">View All</a></li>
												<li><a href="#">Denim</a></li>
												<li><a href="#">Tanks & T Shirts</a></li>
												<li><a href="#">Shirts</a></li>
												<li><a href="#">Shorts & Pants</a></li>
												<li><a href="#">Shorts</a></li>
												<li><a href="#">Pants</a></li>
												<li><a href="#">Jackets & Outerwear</a></li>
												<li><a href="#">Activewear</a></li>
												<li><a href="#">Swimwear</a></li>
												<li><a href="#">Underwear</a></li>
												<li><a href="#">Suits</a></li>
												<li><a href="#">Shoes</a></li>
												<li><a href="#">Accessories</a></li>
												<li><a href="#">Limited Pricing</a></li>
											</ul>
											
                                        </div>
										<div class="col-sm-2">
											<h5>FEATURES</h5>
                                            <ul>
												<li><a href="#">Best Sellers</a></li>
												<li><a href="#">Online Exclusives</a></li>
												<li><a href="#">Online Exclusives</a></li>
												<li><a href="#">The Logo Shop</a></li>
												<li><a href="#">Moto Trend</a></li>
												<li><a href="#">Fall Essentials</a></li>
												<li><a href="#">Night Out</a></li>
												<li><a href="#">Casual Shirts</a></li>
												<li><a href="#">Gifts For Him</a></li>
												<li><a href="#">Gift Cards</a></li>
											</ul>
                                        </div>
                                        <div class="col-sm-4 text-center">
                                              <div class="banner">
                                                <a href="#"><img src="img/menu-men.jpg" alt="GUESS Men"></a><br><br>
												<a href="#">Shop New</a>
                                            </div>
                                        </div>
										 <div class="col-sm-6"></div>
										
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                    </li>
				<!-- katapusan ng individual dropdown menu item -->
				
				<!-- umpisa ng individual dropdown menu item -->
                <li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">DENIM <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										<h5>WOMEN</h5>
                                            <ul>
                                                <li><a href="#">View All</a></li>
												<li><a href="#">Skinny</a></li>
												<li><a href="#">High Rise</a></li>
												<li><a href="#">Mid & Low Rise</a></li>
												<li><a href="#">Bootcut & Flare</a></li>
												<li><a href="#">Straight & Relaxed Fit</a></li>
												<li><a href="#">Shorts & Skirts</a></li>
												<li><a href="#">Tops & Jackets</a></li>
											</ul>
											
                                        </div>
										<div class="col-sm-3">
											<h5>MEN</h5>
                                            <ul>
												<li><a href="#">View All</a></li>
												<li><a href="#">Skinny</a></li>
												<li><a href="#">Slim Tapered</a></li>
												<li><a href="#">Slim Straight & Regular Straight</a></li>
												<li><a href="#">Relaxed & Bootcut</a></li>
												<li><a href="#">Shorts</a></li>
												<li><a href="#">Shirts & Jackets</a></li>
												<li><a href="#">Moto & Destroyed</a></li>
											</ul>
											
                                        </div>
										 
                                        <div class="col-sm-4 text-center">
                                              <div class="banner">
                                                <a href="#"><img src="img/menu-denim.jpg" alt="GUESS Denim"></a><br><br>
												<a href="#">Shop Denim</a>
                                            </div>
                                        </div>
										 <div class="col-sm-5"></div>
										
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                </li>
				<!-- katapusan ng individual dropdown menu item -->
					
				<!-- umpisa ng individual dropdown menu item -->	
				<li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">HANDBAGS <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-3">
										
                                            <ul>
                                                <li><a href="#">View All</a></li>
                                                <li><a href="#">Wallets</a></li>
                                                <li><a href="#">Tote Bags</a></li>
                                                <li><a href="#">Satchels</a></li>
                                                <li><a href="#">Crossbody Bags</a></li>
                                                <li><a href="#">Crossbody Shoulder Bags</a></li>
                                                <li><a href="#">Backpacks</a></li>
                                                <li><a href="#">Mini Bags</a></li>
                                                <li><a href="#">Luggage & Travel Bags</a></li>
                                                <li><a href="#">Bag Accessories</a></li>
                                                <li><a href="#">Limited Pricing</a></li>
											</ul>
											
                                        </div>
										<div class="col-sm-3">
											<h5>FEATURES</h5>
                                            <ul>
												<li><a href="#">Best Sellers</a></li>
												<li><a href="#">Trending Now: Details</a></li>
												<li><a href="#">Night Out</a></li>
												<li><a href="#">Matching Sets</a></li>
												<li><a href="#">Logo</a></li>
												<li><a href="#">Online Exclusives</a></li>
												<li><a href="#">Gift Cards</a></li>
												<li><a href="#">Summer Whites</a></li>
											</ul>
										</div>
                                        <div class="col-sm-3 text-center">
                                            <div class="banner">
                                                <a href="#"><img src="img/menu-handbags.jpg" class="img img-responsive" alt="GUESS Handbags"></a><br>
												<a href="#">Shop Handbags</a>
                                            </div>
                                        </div>
										
										<div class="col-sm-3 text-center"></div>
										
										
										
                                    </div>
                                </div>
                               
                            </li>
                        </ul>
				<!-- katapusan ng individual dropdown menu item -->		
                 
				<!-- umpisa ng individual dropdown menu item -->	
				<li class="dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">ACCESSORIES <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										<h5>WOMEN</h5>
                                            <ul>
                                                <li><a href="#">View All</a></li>
												<li><a href="#">Watches</a></li>
												<li><a href="#">Jewelry</a></li>
												<li><a href="#">Belts</a></li>
												<li><a href="#">Hats & Scarves</a></li>
												<li><a href="#">Sunglasses</a></li>
												<li><a href="#">Fragrance</a></li>
												<li><a href="#">Keychains</a></li>
												<li><a href="#">Phone Cases & Tech Accessories</a></li>
												<li><a href="#">Limited Pricing</a></li>
											</ul>
											
                                        </div>
										<div class="col-sm-2">
											<h5>MEN</h5>
                                            <ul>
												<li><a href="#">View All</a></li>
												<li><a href="#">Watches</a></li>
												<li><a href="#">Jewelry</a></li>
												<li><a href="#">Wallets & Bags</a></li>
												<li><a href="#">Belts</a></li>
												<li><a href="#">Hats</a></li>
												<li><a href="#">Sunglasses</a></li>
												<li><a href="#">Fragrance</a></li>
												<li><a href="#">Limited Pricing</a></li>
											</ul>
											
                                        </div>
										
                                        <div class="col-sm-4 text-center">
                                              <div class="banner">
                                                <a href="#"><img src="img/menu-accessories.jpg" alt="GUESS Accessories"></a><br><br>
												<a href="#">Shop Accessories</a>
                                            </div>
                                        </div>
										 <div class="col-sm-"></div>
										
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                    
					</li>
				<!-- katapusan ng individual dropdown menu item -->
				
				<!-- umpisa ng individual dropdown menu item -->	
				<li class="active dropdown yamm-fw">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">SALE <!--<b class="caret"></b>--></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-2">
										<h5>BABY</h5>
                                            <ul>
                                                <li><a href="#">View All</a></li>
                                                <li><a href="#">Baby Girl (0-24m)</a></li>
                                                <li><a href="#">Baby Boy (0-24M)</a></li>
											</ul>
                                        </div>
										<div class="col-sm-2">
										<h5>GIRL</h5>
                                            <ul>
                                                <li><a href="#">View All</a></li>
                                                <li><a href="#">Little Girl (2 -7)</a></li>
                                                <li><a href="#">Big Girl (7-16)</a></li>
											</ul>
                                        </div>
										<div class="col-sm-2">
                                        <h5>BOY</h5>
                                            <ul>
                                                <li><a href="#">View All</a></li>
                                                <li><a href="#">Little Boy (2 -7)</a></li>
                                                <li><a href="#">Big Boy (7-18)</a></li>
											</ul>
                                        </div>
										<div class="col-sm-2">
                                        <h5>FEATURES</h5>
                                            <ul>
                                                <li><a href="#">Best Sellers</a></li>
                                                <li><a href="#">Denim Shop</a></li>
                                                <li><a href="#">Back To School</a></li>
                                                <li><a href="#">Floral Shop</a></li>
                                                <li><a href="#">Gifts</a></li>
                                                <li><a href="#">Gift Cards</a></li>
											</ul>
                                        </div>
										<div class="col-sm-4 text-center">
                                              <div class="banner">
                                                <a href="#"><img src="img/menu-sale.jpg" alt="GUESS SALE"></a><br><br>
												<a href="#">Sale</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.yamm-content -->
                            </li>
                        </ul>
                    </li>
				<!-- katapusan ng individual dropdown menu item -->	
					
                </ul>

            </div>
<!-- katapusan para sa mga menu items -->			


            
			<div class="navbar-buttons">


                <div class="navbar-collapse collapse right"><!--id="search-not-mobile" -->
                  
					<a href="#" class="btn"   data-toggle="collapse" data-target="#search">
						 <span class="sr-only">Toggle search</span>
                        <i class="fa fa-search"></i>
					</a>
                       <a href="stores.html" class="btn"><i class="fa fa-map-marker"></i></a>
					   <a href="login.html" class="btn"><i class="fa fa-user"></i></a>
					   <a href="basket.html" class="btn"><i class="fa fa-shopping-bag"></i></a>
					   
				 
					
					
				</div>
				 
            </div>

           
			<!-- umpisa ng search toggle -->
		   <div class="collapse clearfix" id="search">

                <form class="navbar-form" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search">
                        <span class="input-group-btn">

			    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
				</span>
                    </div>
                </form>

            </div>
			<!-- katapusan ng search toggle --> 

			
        </div>
</div>  	
<!-- *** KATAPUSAN NG NAVBAR ***  -->





 <!-- umpisa ng maliit na promotional item 120px height. pwedeng hard coded or isang image lang -->
	<section id="ad-30">
	<div class="container">
	 
	<div class="col-sm-3 black text-center">ONLINE ONLY</div>
	<div class="col-sm-6 text-center">
	<span class="black-bold">50% OFF</span> <span class="black">SALE STYLES</span> + 
	<span class="red-bold">EXTRA 10%</span> <span class="red">AT CHECKOUT</span>
	</div>
	
	<div class="col-sm-3 text-center">
	<a href="#" class="black link">WOMEN</a><i class="fa fa-play"></i>
	<a href="#" class="black link">MEN</a> 
	<a href="#" class="black small">DETAILS</a>
	</div>
	
 
	</div>
	</section>
 <!-- katapusan ng maliit na promotional item 120px height. pwedeng hard coded or isang image lang -->

 

<!-- *** UMPISA PARA SA OVERALL WEBSITE CONTENTS -->  
<section id="pinakalaman">

 
 
 <!-- UMPISA NG MAIN ONTENTS --> 
 <div class="container">
			<h1>Privacy Policy</h1>
			
			 
	
<p style="text-decoration: underline;">Introduction</p>
<p>Welcome! You have arrived at a web site or mobile application that is provided by Guess?, Inc. ("Guess" or "we," "our" or "us"). This "Privacy Policy" governs your use of GUESS Philippines website and any other official Guess web site (including mobile sites), interactive features, widgets, and/or other online services that post a link to this Privacy Policy (collectively, referred to as the "Site"). This Privacy Policy also applies to Personal Information (defined below) we may collect from you in our retail stores ("Store" or "Stores") or Guess sponsored events in the United States and Canada, and through official surveys and sweepstakes entry forms, or in any other instance when you contact us.</p>
<p>This Privacy Policy describes what information Guess collects about you, why we collect it, how we use it, and under what circumstances we share it with third parties. This Privacy Policy also describes the choices you can make about how we use your information. To the extent we provide you notice on our Site or in our Stores of different or additional privacy policies or practices (e.g., at the point of our collection), those additional terms shall govern such data collection and use.</p>
<p>In addition, please review the Site's <a href="http://shop.guess.com/en/TermsAndConditions/">Terms &amp; Conditions</a>, which governs your use of the Site. By using our Site, you consent to our Privacy Policy and Terms &amp; Conditions.</p>
<h3>Linkable Table of Contents</h3>
<ol>
<li><a href="#1">Collection of Information</a></li>
<ol>
<li><a href="#1a">Information You Provide to Us</a></li>
<li><a href="#1b">Information We Collect When You Use the Site</a></li>
<li><a href="#1c">Location-Based Information</a></li>
<li><a href="#1d">Information We Collect From Third Parties</a></li>
<li><a href="#1e">Information We Collect When You Interact with Third-Party Sites</a></li>
<li><a href="#1f">Information You Provide About a Third Party</a></li>
</ol>
<li><a href="#2">How We Use the Information We Collect</a></li>
<li><a href="#3">Sharing of Information with Third Parties</a></li>
<ol>
<li><a href="#3a">When You Request We Share With Third Parties</a></li>
<li><a href="#3b">Third Parties Providing Services to Guess</a></li>
<li><a href="#3c">To Protect the Rights of Guess and Others</a></li>
<li><a href="#3d">Affiliates and Business Transfer</a></li>
<li><a href="#3e">Sweepstakes, Contests and Promotions</a></li>
<li><a href="#3f">Co-Branded Areas</a></li>
</ol>
<li><a href="#4">Information You Disclose Publicly</a></li>
<ol>
<li><a href="#4a">User Content and Public Information</a></li>
<li><a href="#4b">Name and Likeness</a></li>
</ol>
<li><a href="#5">Interest-Based Advertising</a></li>
<li><a href="#6">Third-Party Content and Links to Third-Party Sites</a></li>
<li><a href="#7">Your California Privacy Rights</a></li>
<li><a href="#8">Transfer of Information to and from the United States</a></li>
<li><a href="#9">What Should Parents Know About Children?</a></li>
<li><a href="#10">Security</a></li>
<li><a href="#11">Contact Us, Updating Your Information and Choices</a></li>
<li><a href="#12">Opt Outs</a></li>
<li><a href="#13">Changes to the Privacy Policy</a></li>
</ol>
<p style="text-decoration: underline;">Full Detailed Privacy Policy</p>
<ol>
<li class="topic"><a name="1"></a>Collection of Information<ol>
<li class="topic"><a name="1a"></a>Information You Provide to Us
<p>We may ask you to provide us: (i) your first and last name, email address, home address, phone number, credit and debit card numbers and date of birth or other information that could reasonably be used to identify you personally (hereinafter "Personal Information"); and/or (ii) demographic information, such as information like your gender ("Demographic Information"). We may collect this information from you at various times and places, such as when you:</p>
<ul>
<li>Submit account registration forms;</li>
<li>Sign up or request to be placed on our mailing, SMS text and/or email marketing lists;</li>
<li>Place an order through the Site;</li>
<li>Sign up for any of our loyalty programs;</li>
<li>Participate in a survey or Promotion (defined in Section 3(e) below);</li>
<li>Contact us regarding a question or concern;</li>
<li>Submit an application for employment; and</li>
<li>Otherwise interact with Guess, the Site or a Store.</li>
</ul>
<p>Your decision to provide this information is voluntary, but if you choose not to provide this requested information you may not be able to take advantage of all of the Site's features or Guess' services.</p>
</li>
<li><a name="1b"></a>Information We Collect When You Use the Site
<p>In addition to information that you choose to submit to us, we and/or our service providers may automatically collect and/or store certain information when you visit or interact with the Site ("Usage Information"). This Usage Information may be stored and/or accessed from your personal computer, laptop, tablet, mobile phone or other device (a "Device") whenever you visit or interact with our Site. Usage Information may include:</p>
<ul>
<li>your IP address or other unique identifier ("Device Identifier");</li>
<li>your Device browser, operating system and mobile network information;</li>
<li>the areas within our Site that you visit and your activities there, including remembering you and your preferences;</li>
<li>your general location; and</li>
<li>certain other Device data, including the time of day you visit our Site, among other information.</li>
</ul>
<p>We may use various methods and technologies to collect and/or store Usage Information, such as:</p>
<ul class="techList">
<li><em>Cookies</em>. A cookie is a file placed on a Device to uniquely identify your browser or to store information on your Device. Our Site may use HTTP cookies, HTML5 cookies, Flash cookies and other types of cookie technology to store information on local storage. Regular cookies may generally be disabled or removed by tools that are available as part of most commercial browsers, and in some (but not all) instances can be blocked by selecting certain settings. Each browser you use will need to be set separately and different browsers offer different functionality and options. Please note that these tools may not be effective with regard to Flash cookies or HTML5 cookies. For information on disabling Flash cookies, visit <a href="http://www.adobe.com" target="_blank">www.adobe.com</a>. Please understand that if you disable or remove any type of cookie, some parts of our Site may not function properly.</li>
<li><em>Browser Fingerprinting</em>. Collection and analysis of information from your Device, such as, without limitation, your operating system, plug-ins, system fonts and other data, for purposes of identification.</li>
<li><em>Web Beacons</em>. A Web Beacon is a small tag (which may be invisible to you) that may be placed on our Site's pages and messages. Web beacons may be used by us for a number of purposes, including to count visitors to the Site, to monitor how users navigate the Site, to count how many emails that were sent were actually opened or to count how many particular links were actually viewed.</li>
<li><em>Embedded Scripts</em>. An embedded script is programming code that is designed to collect information about your interactions with the Site, such as the links you click on. The code is temporarily downloaded onto your Device from our web server or a third party service provider, is active only while you are connected to the Site, and is deactivated or deleted thereafter. The embedded script does not collect your Personal Information.</li>
<li><em>ETag, or entity tag</em>. An Etag or entity tag is a feature of the cache in browsers. It is an opaque identifier assigned by a web server to a specific version of a resource found at a URL. If the resource content at that URL ever changes, a new and different ETag is assigned. Used in this manner ETags are a form of Device Identifier. ETag tracking may generate unique tracking values even where the consumer blocks HTTP, Flash and/or HTML5 cookies.</li>
<li><em>Recognition Technologies</em>. Technologies, including application of statistical probability to data sets, which attempt to recognize or make assumptions about users and devices (e.g., that a user of multiple devices is the same user).</li>
</ul>
<p>We may use the above technologies (or ones we incorporate in the future) for a variety of purposes, including:</p>
<ul>
<li>To allow you to use and access the Site, including for the prevention of fraudulent activity and improved security functionality or to allow you to make use of shopping cart functionality;</li>
<li>To assess the performance of the Site, including as part of our analytic practices or otherwise to improve the content, products or services offered through the Site;</li>
<li>To offer you enhanced functionality when accessing the Site, including identifying you when you sign in to our Site or keeping track of your specified preferences or to track your online activities over time and across third-party sites; and</li>
<li>To deliver content, including ads, relevant to your interests on our Site and third party sites based on how you interact with our advertisements and/or content.</li>
</ul>
</li>
<li><a name="1c"></a>Location-Based Information
<p>Our Site (including our mobile apps) and Stores may use location-based services to verify your location, deliver you relevant content and/or advertising based on your location, as well as to share your location with our service providers as part of the location-based services we offer. Further, we or our service providers may use your location information to display our nearest Store location to you. You have the ability to turn location-based services on and off by adjusting the settings of your Internet browser or mobile device or by following the standard uninstall process and removing all Guess applications from your mobile device.</p>
</li>
<li><a name="1d"></a>Information We Collect From Third Parties
<p>We may also receive information about you from your friends and other third parties that use the Site, and combine or link that information with information we have about you. Additionally, we may, from time to time, supplement the information we collect directly from you on our Site with outside records from third parties for various purposes, including to enhance our ability to serve you, to tailor our content to you and to offer you opportunities that may be of interest to you. To the extent we combine information we receive from third parties with your Personal Information, the combined information, unless disclosed otherwise, will be treated as Personal Information under this Privacy Policy. In no other circumstances will this Privacy Policy apply to information we receive about you from third parties.</p>
</li>
<li><a name="1e"></a>Information We Collect When You Interact with Third-Party Sites
<p>The Site may include functionality that allows certain kinds of interactions between the Site and your account on a third-party web site or application. The use of this functionality may involve the third-party site providing information, including Personal Information, to us. For example, we may provide links on the Site to facilitate sending a communication from the Site. For example, we may use third parties to facilitate emails, Tweets or Facebook, Instagram, Pinterest or Google+ postings. These third parties may retain any information used or provided in any such communications or activities and these third parties' practices are not subject to our Privacy Policy. Guess may not control or have access to your communications through these third parties. Further, when you use third-party sites or services, you are using their services and not our services and they, not we, are responsible for their practices. You should review the applicable third-party privacy policies before using such third-party tools on our Site.</p>
</li>
<li><a name="1f"></a>Information You Provide About a Third Party
<p>You may send someone else a communication from the Site, such as sending an invitation to a friend. If so, the information you provide (e.g., names, email addresses, mobile number, etc.) will be used to facilitate the communication and will not be used for marketing purposes unless we obtain consent from that person or we explicitly say otherwise. Please be aware that when you use any send-to-a-friend functionality on our Site, your email address, mobile number, name or username and message may be included in the communication sent to your addressee(s). Some of these tools may be third-party tools subject to third-party privacy policies.</p>
</li>
</ol></li>
<li><a name="2"></a>How We Use the Information We Collect
<p>We may use your Personal Information, Demographic Information or Usage Information for various purposes, including, but not limited to, the following:</p>
<ul>
<li>Improving the effectiveness of our Site, marketing endeavors and service offerings;</li>
<li>Processing loyalty program applications;</li>
<li>Site registration;</li>
<li>Processing and completing purchase, return and similar transactions;</li>
<li>Providing customer service;</li>
<li>Notifying you of product recalls or providing other information concerning products you have purchased;</li>
<li>Sending you marketing information about Guess and other third parties we work with, including through direct mail offerings;</li>
<li>Sending you email communications such as electronic newsletters about our products, services, events, loyalty programs and Promotions;</li>
<li>Sending you email about orders you have made on the Site;</li>
<li>Enabling you to participate in the Site's features such as surveys and Promotions;</li>
<li>Identifying your product and service preferences, providing personalized content and informing you of new or additional products, services and Promotions that may be of interest to you;</li>
<li>Providing mobile marketing messages and service messages;</li>
<li>For internal business purposes; and</li>
<li>For purposes disclosed at the time you provide your information or as otherwise set forth in this Privacy Policy.</li>
</ul>
<p>If you prefer not to receive marketing communications from Guess, you may opt out at any time by following the directions in <a href="#12">Section 12</a> below.</p>
</li>
<li><a name="3"></a>Sharing of Information with Third Parties
<p>We may share non-Personal Information, such as aggregated user statistics, with third parties. Further, we may share your Personal Information with third parties, such as our licensees, distributors, partners and others with whom we have marketing or other relationships, for the third parties' direct marketing purposes. If you are a California resident, you have the right to request additional information and so please see <a href="#7">Section 7</a> below about these rights. We may share your Device Identifiers with third parties along with data related to you and your activities. In addition, we may share the information we have collected about you, including Personal Information, as disclosed at the time you provide your information and as described below or otherwise in this Privacy Policy. Examples of when Guess may share your information include as follows:</p>
<ol>
<li>
<p><a name="3a"></a>When You Request We Share With Third Parties. You may be presented with an option to receive certain information and/or marketing offers directly from third parties or to have us send certain information to third parties or give them access to it. If you choose to do so, your information may be disclosed to such third parties and all information you disclose will be subject to the third-party privacy policies and practices of such third parties. In addition, third parties may store, collect or otherwise have access to your information when you interact with their content on our Site. This may include using third-party tools such as Facebook, Twitter, Pinterest or other third-party posting or content sharing tools and by so interacting you consent to such third party practices. We are not responsible for the privacy policies and practices of such third parties and, therefore, you should review such third-party privacy policies and practices of such third parties prior to requesting information from or otherwise interacting with them.</p>
</li>
<li>
<p><a name="3b"></a> Third Parties Providing Services to Guess. We may use third party service providers to perform certain services, such as: (a) to assist us in e-commerce operations; (b) to manage a database of customer information; (c) hosting the Site; (d) designing and/or operating the Site's features; (e) tracking the Site's activities and analytics; (f) enabling us to send you special offers or perform other administrative services; and (g) other services designed to assist us in maximizing our business potential. We may provide third parties with access to your information to carry out the services they are performing for us. Third-party analytics and other service providers such as Google Analytics and Adobe (Omniture) may set and access their own technologies on your Device and they may otherwise collect or have access to information about you. These service providers use the technology described above to help us analyze how users use the Site. We are not responsible for those third party technologies or activities arising out of them. However, some third parties may offer you certain choices regarding their practices. For example, to learn about opting out of Google Analytics, please click <a href="https://tools.google.com/dlpage/gaoptout" target="_blank">here</a>. To learn about opting out of the use of Adobe cookies for analytics and on-site personalization, please click <a href="http://www.adobe.com/privacy/opt-out.html#4" target="_blank">here</a>. We are not responsible for the effectiveness of or compliance with any third parties' opt-out options.</p>
</li>
<li>
<p><a name="3c"></a> To Protect the Rights of Guess and Others. To the fullest extent permitted by applicable law, we may also disclose your information if we believe in good faith that doing so is necessary or appropriate to: (i) protect or defend the rights, safety or property of Guess or third parties (including through the enforcement of this Privacy Policy, our <a href="http://shop.guess.com/en/TermsAndConditions/">Terms &amp; Conditions</a>, and other applicable agreements and policies); or (ii) comply with legal and regulatory obligations (e.g., pursuant to law enforcement inquiries, subpoenas or court orders). To the fullest extent permitted by applicable law, we have complete discretion in electing to make or not make such disclosures, and to contest or not contest requests for such disclosures, all without notice to you.</p>
</li>
<li>
<p><a name="3d"></a> Affiliates and Business Transfer. We may share your information with our parent, subsidiaries and affiliates. We also reserve the right to disclose and transfer all such information: (i) to a subsequent owner, co-owner or operator of the Site and/or our Stores; or (ii) in connection with a merger, consolidation, restructuring, the sale of substantially all of our interests and/or assets or other corporate change, including during the course of any due diligence process.</p>
</li>
<li>
<p><a name="3e"></a> Sweepstakes, Contests and Promotions. We may offer sweepstakes, contests, and other promotions (any, a "Promotion") through the Site, in the Stores or through one of our social media pages that may require registration. By participating in a Promotion, you are agreeing to official rules that govern that Promotion, which may contain specific requirements of you, including, allowing the sponsor of the Promotion to use your name, voice and likeness in advertising associated with the Promotion. If you choose to enter a Promotion, Personal Information may be disclosed to third parties or the public in connection with the administration of such Promotion, including in connection with winner selection, prize fulfillment, and as required by law or permitted by the Promotion's official rules, such as on a winner's list.</p>
</li>
<li>
<p><a name="3f"></a> Co-Branded Areas. Certain areas of the Site may be provided to you in association with third parties ("Co-Branded Areas") such as co-promotional partners and may require you to disclose Personal Information to them. Such Co-Branded Areas will identify the third party and indicate if they have a privacy policy that applies to their collection and use of your information. If you elect to register for products and/or services, communicate with such third parties or download their content or applications, you may be providing your information to both us and the third party. Further, if you sign-in to a Co-Branded Area with a username and password obtained on the Site, your Personal Information may be disclosed to the identified third parties for that Co-Branded Area. We are not responsible for such third party's data collection or practices and you should look to such third party privacy policies for more information.</p>
</li>
</ol></li>
<li><a name="4"></a>Information You Disclose Publicly<ol>
<li>
<p><a name="4a"></a> User Content and Public Information. The Site may permit you to submit ideas, photographs, videos, audio recordings, questions, comments, suggestions or other content, including Personal Information (collectively, "User Content"), on blogs and message boards, product reviews and other public forums. We or others may store, display, publish, distribute or otherwise use User Content online or offline in any media or format and may or may not attribute the User Content to you. Others may have access to this User Content and may have the ability to share it with third parties. Please note that Guess does not control who will have access to the information that you choose to make public, and cannot ensure that parties who have access to such publicly available information will respect your privacy or keep it secure. We are not responsible for the privacy or security of any information that you make publicly available on the Site or what others do with information you share with them on the Site.</p>
</li>
<li>
<p><a name="4b"></a> Name and Likeness. We may also publish your name, voice, likeness and other Personal Information that is part of your User Content, and we may use the content, or any portion of the content, for advertising, marketing, publicity and promotional activities. For full terms and conditions regarding User Content you submit, please review our <a href="http://shop.guess.com/en/TermsAndConditions/">Terms &amp; Conditions</a>. By connecting with the Guess brands through our social media pages or accounts, or by posting content using Guess-created hashtags or other identifiers, you are providing your consent for Guess to use any Personal Information or other content you submit or make available through such sites for marketing and promotional purposes, including, but not limited to, incorporation into our marketing materials, without further notice or compensation.</p>
</li>
</ol></li>
<li><a name="5"></a>Information You Disclose Publicly
<p>You may see certain ads on other websites because we participate in advertising networks administered by third parties. These networks track your online activities over time and across third party websites and online services by collecting information through automated means, including through the use of the technologies described in the "Information We Collect When You Use the Site" Section above, and they use this information to show you advertisements that are tailored to your individual interests. The information they collect includes information about your visits to our Site, such as the pages you have viewed. This collection and ad targeting takes place both on our Site and on third-party websites that participate in the ad network. This process also helps us track the effectiveness of our marketing efforts. We and our service providers use first-party cookies and third-party cookies together to report how our internet advertising methods are related to your visits to the Site.</p>
<p>To learn more about how these service providers use your information, cookies or other tracking information and explore your opt out options, please visit their respective websites below (NOTE: by clicking on the links below, you will be leaving the GUESS website):</p>
<ul>
<li>DoubleClick &ndash; click <a href="https://www.google.com/settings/ads?hl=en&amp;sig=ACi0TCi3R4mL7qb_WRaELD5Z_YPR5O5InCvFFgPTJ5jcWbHaE2tb_jOvoIkA5pP6F9oEvdNnVsMQuwj5HJV2Pg-6RADnzbP9mI_0dcxfwak_4v1_JeRylUrylqkCThZSDLaaI0iKKqsfaEE9pvltfXxOZH9ZarVDcsj6b7B5xIgS7XgzMgJjPq6m-L38v01JOJumwnkQh8Kt" target="_blank">here</a></li>
<li>Olapic &ndash; click <a href="https://www.olapic.com/privacy/" target="_blank">here</a></li>
<li>Chango/Rubicon &ndash; click <a href="http://www.networkadvertising.org/choices/" target="_blank">here</a></li>
<li>Curalate &ndash; click <a href="http://www.curalate.com/privacy_policy.html" target="_blank">here</a></li>
<li>MediaForge/Rakuten&ndash; click <a href="http://www.networkadvertising.org/choices/" target="_blank">here</a></li>
</ul>
<p>To learn more about ad networks, including how to opt-out, please click <a href="http://www.aboutads.info/choices" target="_blank">here</a></p>
<p>The list above may be modified from time to time and may not reflect all of our third-party service providers.</p>
<p>Also, some third parties, such as companies that offer Internet browsers, have developed signals (e.g., browser do not track signals) that give you a choice regarding the collection of information about your online activities over time and across third-party websites or online services. Currently, we do not monitor or take any action with respect to these signals or other mechanisms offered by Internet browsers. If you would like more information on self-regulatory programs that permit you to exercise choice, click on the ad networks link above.</p>
</li>
<li><a name="6"></a>Third-Party Content and Links to Third-Party Sites
<p>The Site may contain content that is supplied by a third party, and those third parties may collect Usage Information and your Device Identifier when pages from the Site are served to you. In addition, when you are on the Site, you may be directed to other services that are operated and controlled by third parties. We are not responsible for the data collection and privacy practices employed by any of these third parties. For example, if you "click" on a link, the &ldquo;click&rdquo; may take you off the Site onto a different site. These other sites may independently collect data about you and may or may not have their own published privacy policies.</p>
</li>
<li><a name="7"></a>Your California Privacy Rights
<p>We may from time to time elect to share certain information about you collected by us on the Site with third parties for those third parties' direct marketing purposes. California Civil Code Section 1798.83 permits California residents who have supplied personal information, as defined in the statute, to us, under certain circumstances, to request and obtain certin information regarding our disclosure, if any, of personal information to third parties for their direct marketing purposes. If this applies, you may obtain the categories of personal information shared and the names and addresses of all third parties that received personal information for their direct marketing purposes during the immediately prior calendar year (e.g., requests made in 2014 will receive information about 2013 sharing activities). To make such a request, please provide sufficient information for us to determine if this applies to you, attest to the fact that you are a California resident and provide a current California address for our response. To make such a request (limit one request per year), please submit a written request to the following address (limit one request per year): Guess, Attn: Customer Care &ndash; California Privacy Rights Notice, Guess?, Inc., 1444 Alameda Street, Los Angeles, CA 90021. You must include your full name, email address, and postal address in your request.</p>
</li>
<li><a name="8"></a>Transfer of Information to and from the United States
<p>Our Site is operated in the United States. If you are located outside of the United States, please be aware that information we collect, including Personal Information, will be transferred to, and processed, stored and used in the United States. Additionally, information, including Personal Information, may be transferred by us outside of the United States. The data protection laws in the United States may differ from those of the country in which you are located, and your information may be subject to access requests from governments, courts, or law enforcement in the United States according to laws of the United States and may be subject to access requests from governments, courts, or law enforcement in the other countries according to laws of these countries. By using the Site or providing us with any information, you consent to the transfer to, and processing, usage, sharing and storage of your information in the United States and in other countries, as set forth in this Privacy Policy.</p>
</li>
<li><a name="9"></a>What Should Parents Know About Children?
<p>We understand the importance of protecting children&rsquo;s privacy in the interactive world. We do not use the Site to knowingly collect Personal Information from children under the age of 13 without parental consent. If you are a child under 13 years of age, you are not permitted to use the Site and should not send any information about yourself to us through the Site.</p>
<p>In the event that we become aware that we have collected Personal Information from any child, we will dispose of that information in accordance with the Children&rsquo;s Online Privacy Protection Act. If you are a parent or guardian and you believe that your child under the age of 13 has provided us with information without your consent, please contact us by email at the address set forth in Section 11 below, and we will take reasonable steps to ensure that such information is deleted from our databases.</p>
</li>
<li><a name="10"></a>Security
<p>We incorporate commercially reasonable safeguards to help protect and secure your Personal Information. However, no data transmission over the Internet, mobile networks, wireless transmission or electronic storage of information can be guaranteed 100% secure. As a result, we cannot guarantee or warrant the security of any information you transmit to or from our Site, and you provide us with your information at your own risk.</p>
</li>
<li><a name="11"></a>Contact Us, Updating Your Information and Choices
<p>If you have any questions about this privacy statement, the practices of this site, or your dealings with Guess, you can contact us:</p>
<p style="text-decoration: underline;">Mail:</p>
<p style="line-height: 15px;">GUESS Customer Care<br/> Guess?, Inc.<br/> 1444 Alameda Street<br/> Los Angeles, CA 90021</p>
<p style="text-decoration: underline;">Phone:</p>
<p>Call Toll Free Monday through Friday 6:00 a.m. - 5:00 p.m., Saturday 7 a.m. - 4 p.m. (PT):</p>
<div class="table-responsive">
<table class="privacyTable table" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td valign="top">GUESS or GUESS KIDS:</td>
<td valign="top">1-877-44-GUESS (1-877-44-8377).</td>
</tr>
<tr>
<td valign="top">MARCIANO:</td>
<td valign="top">1-866-44-MARCIANO (1-866-446-2724).</td>
</tr>
<tr>
<td valign="top">G BY GUESS:</td>
<td valign="top">1-877-GbyGUESS (1-877-429-4837).</td>
</tr>
<tr>
<td align="left" valign="top">GUESS FACTORY:</td>
<td align="left" valign="top">1-877-GUESSFactory (1-877-483-7732)</td>
</tr>
</tbody>
</table>
</div>
<p style="text-decoration: underline;">E-mail:</p>
<p><a href="https://shop.guess.com/en/CustomerCare/ContactUs/">Customer Care Contact Form</a></p>
</li>
<li><a name="12"></a>Opt Outs
<p>If you would like to unsubscribe or cancel your memberships with Guess at any time, you can contact us using the following methods:</p>
<p>To unsubscribe from mobile alerts from:</p>
<div class="table-responsive">
<table class="privacyTable table" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td valign="top">GUESS:</td>
<td valign="top">text STOP to GUESS (48377)</td>
</tr>
<tr>
<td valign="top">MARCIANO:</td>
<td valign="top">text STOP to SHOPM (74676)</td>
</tr>
<tr>
<td valign="top">G BY GUESS:</td>
<td valign="top">text STOP to GPULSE (478573)</td>
</tr>
</tbody>
</table>
</div>
<p>To opt out of e-mail marketing communications for:</p>
<div class="table-responsive">
<table class="privacyTable table" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td valign="top">GUESS visit:</td>
<td valign="top"><a href="https://shop.guess.com/en/Subscription/Unsubscribe/">https://shop.guess.com/en/Subscription/Unsubscribe/</a></td>
</tr>
<tr>
<td valign="top">MARCIANO visit:</td>
<td valign="top"><a href="https://guessbymarciano.guess.com/en/Subscription/Unsubscribe/">https://guessbymarciano.guess.com/en/Subscription/Unsubscribe/</a></td>
</tr>
<tr>
<td valign="top">GUESS Kids visit:</td>
<td valign="top"><a href="https://guesskids.guess.com/en/Subscription/Unsubscribe/">https://guesskids.guess.com/en/Subscription/Unsubscribe/</a></td>
</tr>
<tr>
<td valign="top">G BY GUESS visit:</td>
<td valign="top"><a href="https://www.gbyguess.com/en/Subscription/Unsubscribe/">https://www.gbyguess.com/en/Subscription/Unsubscribe/</a></td>
</tr>
<tr>
<td valign="top">GUESS FACTORY visit:</td>
<td valign="top"><a href="https://guessfactory.com/en/Subscription/Unsubscribe">https://guessfactory.com/en/Subscription/Unsubscribe</a></td>
</tr>
</tbody>
</table>
</div>
<p>To opt out of direct mail marketing communications, please write to GUESS at the mailing address above, be sure to include your name and address as it appears on any marketing materials received from GUESS.</p>
<p>Please allow 10 days to process your email request and 6-8 weeks to process your postal mail request, during which period you may continue to receive communications from Guess. Please note that we will retain your information for business purposes or as required by law.</p>
<p>To cancel your membership in Guess List, M Loyalty or the Pulse Loyalty Programs, please follow the instructions in the applicable Program Terms &amp; Conditions available on the Site.</p>
<p>Please note that we may still send you certain communications relating to your account or use of our Site, such as administrative and service announcements and these transactional account messages may be unaffected if you choose to opt-out from receiving our marketing communications.</p>
<p>If you have any questions about the Privacy Policy, or practices described in it, you may also use the information above to contact us.</p>
</li>
<li><a name="13"></a>Changes to the Privacy Policy
<p>We reserve the right to change this Privacy Policy at any time. Any changes will be effective immediately upon the posting of the revised Privacy Policy and your use of our Site indicates your consent to the privacy policy posted at the time of use. However, we will not use your previously collected Personal Information in a manner materially different than represented at the time it was collected without your consent. To the extent any provision of this Privacy Policy is found by a competent tribunal to be invalid or unenforceable, such provision shall be severed to the extent necessary for the remainder to be valid and enforceable.</p>
</li>
</ol>
		
	 		 
			
</div>	
 
</section>
<!-- *** KATAPUSAN PARA SA OVERALL WEBSITE CONTENTS -->   


    

	
	
	
	

<!-- *** START FOOTER *** -->
<section id="footer" data-animate="fadeInUp">
	 
			<div class="container">
			<div class="col-sm-3"></div>
			
			<div class="col-sm-6 text-center">
			<p>Enjoy 15% OFF & Free Shipping on your first order</p>
			
						<form>
                            <div class="input-group">
							<input type="text" class="form-control">
								<span class="input-group-btn"><button class="btn btn-danger" type="button">SIGNUP NOW!</button></span>
							</div>
                            <!-- /input-group -->
						<p>
						<input type="checkbox"> By signing up I agree to receive emails about the latest trends and exclusive content.
						</p>
                        </form>
			</div>
			
			<div class="col-sm-3"></div>
			
			</div>
			
			<br><br>
		
			
            <div class="container">
			
			 <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <h4>MY ACCOUNT</h4>

                        <ul>
                            <li><a href="register.html">Register</a></li>
                            <li><a href="login.html">Login</a></li>
                            <li><a href="orders.html">Orders</a></li>
                            <li><a href="returns.html">Returns</a></li>
                         </ul>
 

                       

                        <hr class="hidden-md hidden-lg hidden-sm">

                    </div>
                    <!-- /.col-md-3 -->

                    <div class="col-md-3 col-sm-6">

                        <h4>ASSISTANCE</h4>
						<ul>
                            <li><a href="support.html">Customer Care</a></li>
                            <li><a href="stores.html">Store Locator</a></li>
                            <li><a href="contact.html">Contact Us</a></li>
                            <li><a href="gift.html">Gift Cards</a></li>
                        </ul>
 

                        <hr class="hidden-md hidden-lg">

                    </div>
                    <!-- /.col-md-3 -->

                    <div class="col-md-3 col-sm-6">

                        <h4>COMPANY</h4>
						<ul>
                            <li><a href="stores.html">Our Store</a></li>
                            <li><a href="careers.html">Corporate Careers</a></li>
                            <li><a href="careers-retail.html">Retail Careers</a></li>
                        </ul>
						
                        <p class="hidden">
						<strong>GUESS Philippines</strong>
                            <br>DII Bldg. 
                            <br>150 San Vicente Rd
                            <br>Brgy. San Vicente
                            <br>Laguna
                            <br>
                            <strong>Philippines</strong>
                        </p>
 
                        <hr class="hidden-md hidden-lg">

                    </div>
                    <!-- /.col-md-3 -->



                    <div class="col-md-3 col-sm-6">

                        <h4>FOLLOW US</h4>
 
                        <p>
                            <a href="https://www.facebook.com/GUESSPhil" class="facebook external" target="_blank" data-animate-hover="none"><i class="fa fa-facebook-square fa-2x"></i></a>
                            <a href="https://twitter.com/GUESS" class="twitter external" target="_blank" data-animate-hover="none"><i class="fa fa-twitter fa-2x"></i></a>
                            <a href="https://www.instagram.com/guess" class="instagram external" target="_blank" data-animate-hover="none"><i class="fa fa-instagram fa-2x"></i></a>
                            <a href="https://www.pinterest.com/guess" class="gplus external" target="_blank" data-animate-hover="none"><i class="fa fa-pinterest-p fa-2x"></i></a>
                            <a href="https://www.youtube.com/user/GUESSincTV" class="email external" target="_blank" data-animate-hover="none"><i class="fa fa-youtube-play fa-2x"></i></a>
                        </p> 

                    </div>
                    <!-- /.col-md-3 -->

					
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
			<br><br>
			<p class="small text-center">
			&copy; <script type="text/javascript">document.write(new Date().getFullYear());</script> GUESS Philippines. All rights reserved.
			<a href="terms.html">Terms & Conditions</a> <a href="privacy.html">Privacy Policy</a>
			</p>
			
			
			
			
			
        </section>
<!-- *** END FOOTER *** -->




<!-- *** START COPYRIGHT *** -->
<section id="copyright" class="hidden">
<div class="container ">
		 
			
		 
                <div class="col-md-6">
                    <p class="pull-left">© <script type="text/javascript">document.write(new Date().getFullYear());</script> GUESS Philippines. All rights reserved.</p>

                </div>
                <div class="col-md-6">
                    <p class="pull-right">
					<a href="terms.html" class="active">Terms & Conditions</a> <a href="privacy.html">Privacy Policy</a>
                        
                    </p>
                </div>
			
			
</div>
</section>
<!-- *** COPYRIGHT END *** -->


	
	
	 

    <!-- *** SCRIPTS TO INCLUDE ***
 _________________________________________________________ -->
    <script src="js/jquery-1.11.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.cookie.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/modernizr.js"></script>
    <script src="js/bootstrap-hover-dropdown.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/front.js"></script>
	 
</body>

</html>