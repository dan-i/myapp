$(document).ready(function(){
	
	//Methods
	function isGeoValid(){
		//Checks whether latitude and longtitude are in the correct range and not alpha 
		
		var regexLatitude = /^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?)$/;
		var regexLongitude = /^[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$/;
		
		var hasNoError = true;
		
		if(!regexLatitude.test($('#latitude').val())){
			$('#latitudeError').css('display', 'block');
			hasNoError = false;
		}
		
		if(!regexLongitude.test($('#longitude').val())){
			$('#longitudeError').css('display', 'block');
			hasNoError = false;
		}
		
		return hasNoError;
	}

	//Events
	$('#latitude').on('focus', function(){
		$('#latitudeError').css('display', 'none');
	});
	
	$('#longitude').on('focus', function(){
		$('#longitudeError').css('display', 'none');
	});
	
	//API Requests
	//Create Store
	$('#createStore').on('click', function(){		
		
		if(isGeoValid()){
			var data = {
					name: $('#name').val(),
					countryId: 169,
					code: 'n/a',
					address1: $('#address1').val(),
					address2: $('#address2').val(),
					email: 'n/a',
					contact: $('#contact').val(),
					ip: 'n/a',
					zipcode: $('#zipcode').val(),
					latitude: $('#latitude').val(),
					longitude: $('#longitude').val()
					
				}
			$.ajax({
				url: '/myApp/api/store/create',
				method: 'POST',
				contentType: "application/json",
				data: JSON.stringify(data),
				success: function(data){
					window.location.replace("view");
				},
				error: function(jqXHR, exception){
					
					alert("An error has occured. Please try again after a few minutes.");
				}
			});
		}
		
	});
	
	
	//Update Store
	$('#updateStore').on('click', function(){
		if(isGeoValid()){
			var data = {
					id: $('#storeId').val(), 
					name: $('#name').val(),
					countryId: 169,
					code: 'n/a',
					address1: $('#address1').val(),
					address2: $('#address2').val(),
					email: 'n/a',
					contact: $("#contact").val(),
					ip: 'n/a',
					zipcode: $('#zipcode').val(),
					latitude: $('#latitude').val(),
					longitude: $('#longitude').val()
					
				};
			$.ajax({
				url: '/myApp/api/store/update',
				method: 'POST',
				contentType: "application/json",
				data: JSON.stringify(data),
				success: function(data){
					window.location.replace("../view");
				},
				error: function(jqXHR, exception){
					alert("An error has occured. Please try again after a few minutes.");
				}
			});
		}
		
	});
	
	//Delete Store
	$('.buttonDelete').on('click', function(e){
		var storeIdDel = e.target.id;
		var willDelete = confirm('Are you sure you want to delete the store?');
		var data = {
			id: storeIdDel 
		}
		if(willDelete){
			$.ajax({
				url: '/myApp/api/store/delete',
				method: 'POST',
				contentType: "application/json",
				data: JSON.stringify(data),
				success: function(data){
					window.location.replace("view");
				},
				error: function(jqXHR, exception){
					alert(exception);
				}
			});
		}
	});
})

