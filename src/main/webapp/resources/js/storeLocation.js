//Obtain Longtitude and Latitude first, then the google API will generate the map location
var latitude = $("#latitude").html();
var longitude = $("#longitude").html();

function initMap() {
    var location = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 15,
        center: location
    });
    var marker = new google.maps.Marker({
        position: location,
        map: map
    });
}