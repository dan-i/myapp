//Try to obtain location (Longtitude and latitude) automatically

if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(
    	getPosition,
    	function(err){
    		$("#loader").css("display", "none");
    		$("#locationError").css("display", "block");
    		console.log(err.message);
    	},
    	{timeout: 10000}
    );
} else { 
	$("#loader").css("display", "none");
	$("#locationError").css("display", "block");
}

function getPosition(position){
	$("#loader").css("display", "none");
	if(position == null)
		$("#locationError").css("display", "block");
	else{	
		$("#latitude").val(position.coords.latitude);
		$("#longitude").val(position.coords.longitude);
	}
}