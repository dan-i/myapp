// var locations = [
//      ['<div class="info-window">' +
//                '<h4>GUESS Main Office</h4>' +
//                '<div class="info-content">' +
//                '<p>G/F Tii Building, San Vicente Road,<br> San Pedro City, Laguna<br>4023 PHILIPPINES</p>' +
//                '</div>' +
//                '</div>', 14.353115, 121.045837, 5],
//      ['<div class="info-window">' +
//                '<h4>GUESS Nueva Ecija</h4>' +
//                '<div class="info-content">' +
//                '<p>Pan-Philippine Hwy, Cabanatuan City, <br>3100 Nueva Ecija, Philippines<br>+63 44 464 1153</p>' +
//                '</div>' +
//                '</div>', 15.467422, 15.467422, 4],
//       ['<div class="info-window">' +
//                '<h4>GUESS Mall of Asia</h4>' +
//                '<div class="info-content">' +
//                '<p>SM Mall Of Asia Main Mall,<br> JW Diokno Blvd,<br> Pasay PHILIPPINES</p>' +
//                '</div>' +
//                '</div>', 14.555459, 121.056745, 3],
//       ['<div class="info-window">' +
//                '<h4>GUESS SM Fairview</h4>' +
//                '<div class="info-content">' +
//                '<p>Quirino Hi-Way Corner Regalado Streetfairview, Metro Manila PHILIPPINES</p>' +
//                '</div>' +
//                '</div>', 14.739631, 121.062027, 2],
//      ['<div class="info-window">' +
//                '<h4>GUESS Ayala</h4>' +
//                '<div class="info-content">' +
//                '<p>Quirino Hi-Way Corner Regalado Streetfairview, Metro Manila PHILIPPINES</p>' +
//                '</div>' +
//                '</div>', 14.558913, 121.025108, 1]
//    ];
 

 function initMap(){

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: new google.maps.LatLng(14.6091, 121.0223),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

	var markerImage = './resources/img/guess-marker.png';
 
    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map,
		icon: markerImage
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
 }